Ext.define('Dietofpoints.model.ListFriendRequest', {

    extend: "Ext.data.Model",
	
    config: {
        fields: [
            {name: "name_user", type: "string"},
            {name: "code_user",  type: "int"}
        ],
		
		remoteFilter: true,
 
		proxy: {
			type: 'ajax',
			reader: {
				type: 'json',
				rootProperty: 'root',
				successProperty: 'success',
				totalProperty: 'totalCount'
			}
		},
		
		autoLoad: true
    }
});