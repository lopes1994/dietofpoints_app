Ext.define("Dietofpoints.view.PanelUpdateUserFood", {

	extend: 'Ext.Panel',
	
	xtype: 'panelUpdateUserFood',

	config: {

        layout: 'vbox',
        scrollable: 'vertical',

        items: [
            {
                xtype: 'textfield',
                itemId: 'name',
                placeHolder: L('panel.template.panelUpdateUserFood.name'),
                cls: 'customInput'
            },
            {
                xtype: 'numberfield',
                placeHolder: L('panel.template.panelUpdateUserFood.portions'),
                itemId: 'portions',
                cls: 'customInput',
                maxLength: 2
            },
            {
                xtype: 'textfield',
                itemId: 'amount',
                placeHolder: L('panel.template.panelUpdateUserFood.amount'),
                cls: 'customInput'
            },
            {
                xtype: 'numberfield',
                placeHolder: L('panel.template.panelUpdateUserFood.points'),
                itemId: 'points',
                cls: 'customInput',
                maxLength: 3,
                listeners: [
                    {
                        fn: function(element, eOpts) {
                            element.down('input').dom.focus();
                        },
                        event: 'painted'
                    }
                ]
            },
            {
                xtype: 'numberfield',
                hidden: true,
                itemId: 'code'
            }
        ]
	}
});