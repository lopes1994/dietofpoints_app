Ext.define("Dietofpoints.store.ListMyFood", {

	extend: 'Ext.data.Store',

	config: {
		model: 'Dietofpoints.model.ListMyFood'
	}
});