Ext.define('Dietofpoints.view.Diary', {

    extend: 'Ext.Panel',
	
    xtype: 'diary',
	
    config: {

        scrollable: 'vertical',

        layout:{
            type  : 'vbox'
        },
		
        items: [
            {
                xtype: 'panelDatepicker'
            },
            {
                xtype: 'paneldatadiary'
            },
			{
                xtype: 'panelTitlePeriodBreakfast'
            },
            {
                xtype: 'listBreakfast'
            },
            {
                xtype: 'panelOptionsBreakfast'
            },
            {
                xtype: 'panelTitlePeriodLunch'
            },
            {
                xtype: 'listLunch'
            },
            {
                xtype: 'panelOptionsLunch'
            },
            {
                xtype: 'panelTitlePeriodDinner'
            },
            {
                xtype: 'listDinner'
            },
            {
                xtype: 'panelOptionsDinner'
            },
            {
                xtype: 'panelTitlePeriodSnacks'
            },
            {
                xtype: 'listSnacks'
            },
            {
                xtype: 'panelOptionsSnacks'
            },
            {
                xtype: 'panelTitleWater'
            }
        ]
    }
});