Ext.define("Dietofpoints.view.PanelAddMeal", {

	extend: 'Ext.Panel',
	
	xtype: 'panelAddMeal',

	config: {
	
        scrollable: false,

        layout:{
            type  : 'vbox'
        },

        items: [
            {
                xtype: 'panel',
                docked: 'top',
                cls: 'panelTitleMeal',
                itemId: 'nameMeal',
                html: null
            },
            {
                xtype: 'listAddMeal',
                flex: 1
            }
        ]
	}
});