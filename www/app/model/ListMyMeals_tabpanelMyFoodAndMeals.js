Ext.define('Dietofpoints.model.ListMyMeals_tabpanelMyFoodAndMeals', {

    extend: "Ext.data.Model",
	
    config: {
        fields: [
            {name: "name_meal", type: "string"},
            {name: "code_meal", type: "int"}
        ],
		
		remoteFilter: true,
 
		proxy: {
			type: 'ajax',
			reader: {
				type: 'json',
				rootProperty: 'root',
				successProperty: 'success',
				totalProperty: 'totalCount'
			}
		},
		
		autoLoad: true
    }
});