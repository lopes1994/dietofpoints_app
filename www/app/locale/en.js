/**
 *  Resource file for English language.
 */
Ext.define('Dietofpoints.locale.en', {
    singleton: true,
    requires: [
        'Ext.MessageBox',
        'Ext.picker.Picker',
        'Ext.picker.Date'
    ],
    translation: {
        language: {
            idiom: 'en'
        },
        buttons: {
            save: 'Save',
            next: 'Next',
            add: 'Add'
        },
        socialMessage: {
            text: 'Take control of your daily diet in the DietofPoints application. It is very easy, download for free now:'
        },
        notifications: {
            title: 'Record your daily meals',
            text: "It's time to register your meals. Come on?"
        },
        rate: {
            title: 'Rate app',
            buttons: {
                '1': 'Do not ask again',
                '2': 'After',
                '3': 'To evaluate'
            }
        },
        dateFormat: 'm/d/Y',
        emptyText: 'No results found',
        ERROR_TITLE: 'An error has occurred',
        messageBox_networkError: 'Network error. Try again!',
        menu: {
            diary: 'Diary',
            profile: 'Profile',
            myFoodAndMeals: 'My food and meals',
            reminders: 'Reminders',
            logout: 'Log out',
            help: 'Help',
            myWeight: 'My weight',
            about: 'About',
            share: 'Share',
            friend: 'Friends',
            friendRequest: 'Friend requests'
        },
        list: {
            template: {
                listWeight: {
                    last_weight: 'Initial weight'
                },
                listMyFood_tabpanelMyFoodAndMeals: {
                    title: 'My food'
                },
                listMyMeals_tabpanelMyFoodAndMeals: {
                    title: 'My meals'
                },
                listFriend: {
                    title: 'Friends',
                    won: 'Won',
                    lost: 'Lost'
                },
                listFriendRequest: {
                    title: 'Friend requests'
                }
            }
        },
        panel: {
            template: {
                panelTitlePeriodBreakfast: 'Breakfast',
                panelTitlePeriodLunch: 'Lunch',
                panelTitlePeriodDinner: 'Dinner',
                panelTitlePeriodSnacks: 'Snacks',
                panelTitleWater: {
                    title: 'Water',
                    cups: 'cups 250 ml'
                },
                diary: 'Diary',
                select_item: 'Select item',
                panelInitialWeight: {
                    title: 'Initial weight'
                },
                panelActualWeight: {
                    title: 'Actual weight'
                },
                panelGoalWeight: {
                    title: 'Goal weight'
                },
                panelActivityLevel: {
                    title: 'Activity level'
                },
                panelHeight: {
                    title: 'Height'
                },
                panelAge: {
                    title: 'Age'
                },
                panelProfile: {
                    title: 'Profile'
                },
                panelListWeight: {
                    title: 'Weight history'
                },
                panelTopListWeight: {
                    won: 'You won',
                    lost: 'You lost',
                    in: 'in',
                    days: 'days',
                    goal_lost: 'You need to lose',
                    goal_win: 'You need to win',
                    reach: 'to reach the goal of',
                    congratulations: 'Congratulations, you have managed to achieve the goal of'
                },
                panelMenu: {
                    consecutive_days: 'consecutive days',
                    text_balance_positive: 'Gained',
                    text_balance_negative: 'Lost'
                },
                panelAllFood: {
                    title: 'All food',
                    search: 'Search',
                    searchfield: ' Enter the name of the food',
                    createFood: 'Create a food',
                    quickAdd: 'Calories "Quick add"'
                },
                panelMyFood: 'My food',
                panelMyMeals: {
                    title: 'My meals',
                    searchfield: 'Enter the name of the meal'
                },
                panelOptionsDiary: {
                    addFood: 'Add food',
                    more: 'More'
                },
                paneldatadiary: {
                    goal: 'Goal',
                    food: 'Food',
                    pointsbalance: 'Points balance'
                },
                login: {
                    email: 'Email',
                    password: 'Password',
                    login: 'Log in'
                },
                main: {
                    register: 'Register',
                    login: 'Log in'
                },
                panelSaveMeal: {
                    title: 'Save meal',
                    textfield: 'Enter a name for this meal',
                    button: 'Save',
                    panel: 'Items contained in this meal'
                },
                panelSaveMealMultiplePeriods: {
                    textfield: 'Enter a name for this meal',
                    button: 'Save',
                    panel: 'Items contained in this meal'
                },
                panelCreateFood: {
                    title: 'Create food',
                    name: 'Name',
                    amount: 'Unit (tablespoon, ml, g)',
                    unit: 'Number of portions (1, 2, 350, 400)',
                    points: 'Calories'
                },
                panelUpdatePortion: {
                    title: 'Update portion',
                    portions: 'Number of portions'
                },
                panelUpdateCalories: {
                    title: 'Update calories',
                    calories: 'Calories'
                },
                panelActivityLevel_user: {
                    title: 'Activity level'
                },
                tabpanelMyFoodAndMeals: {
                    title: 'My food and meals'
                },
                panelUpdateUserFood: {
                    title: 'Update food',
                    name: 'Name',
                    amount: 'Unit (tablespoon, ml, g)',
                    portions: 'Number of portions (1, 2, 350, 400)',
                    points: 'Calories'
                },
                panelAddMeal: {
                    title: 'Add meal'
                },
                panelIdiom: {
                    title: 'Choose a language',
                    english: 'English',
                    spanish: 'Español',
                    portuguese: 'Português'
                },
                panelYou: {
                    title: 'You',
                    nameGender: 'Gender',
                    age: 'Age',
                    height: 'Height (cm)',
                    weight: 'Actual weight (kg)',
                    goalWeight: 'Goal weight',
                    name: 'Name'
                },
                panelGender_you: {
                    male: 'Male',
                    female: 'Female'
                },
                panelAccessData: {
                    title: 'Access data',
                    email: 'Email',
                    password: 'Password'
                },
                login: {
                    title: 'Log in',
                    email: 'Email',
                    password: 'Password',
                    login: 'Log in'
                },
                panelAbout: {
                    title: 'About',
                    text: 'In the Diet of points you need to follow the amount of points that can ingest a day. It is not allowed to eat foods that have trans fat. Any other food is allowed, but you need to use common sense when assembling your menu, choosing the healthiest foods to be able to maintain a balanced diet. Avoid the lack of nutrients to avoid future health problems.'
                },
                panelAddFriend: {
                    title: 'Add Friend',
                    email: "Friend's email"
                }
            }
        },
        modal: {
            template: {
                modalConfirmDeleteItems: {
                    title: 'Delete',
                    text: 'Are you sure you want to delete this item?',
                    cancel: 'Cancel',
                    delete: 'Delete'
                },
                modalCopyMeal: {
                    title: 'Copy to which date?',
                    today: 'Today',
                    yesterday: 'Yesterday',
                    tomorrow: 'Tomorrow'
                },
                modalErrorCreateMeal: {
                    title: 'Warning',
                    text: 'You have already created a meal with this name. Please choose another name.',
                    close: 'Close'
                },
                modalErrorDeleteItems: {
                    title: 'Warning',
                    text: 'There are no items selected.',
                    close: 'Close'
                },
                modalErrorQuicklyAdd: {
                    title: 'Warning',
                    text: 'Enter the number of calories you want to register.',
                    close: 'Close'
                },
                modalErrorAddWeight: {
                    text: 'Enter the number of weight you want to register.'
                },
                modalErrorWithoutNameCreateMeal: {
                    title: 'Warning',
                    text: 'You must enter a name for this meal.',
                    close: 'Close'
                },
                modalOptionsSelectItem: {
                    option1: 'Copy to date',
                    option2: 'Save meal',
                    option3: 'Select all'
                },
                modalQuicklyAdd: {
                    title: 'Calories to add',
                    text: 'Calories',
                    cancel: 'Cancel',
                    add: 'Add'
                },
                modalAddWeight: {
                    title: 'Weight to add',
                    text: 'kg'
                },
                modalSuccessCreateMeal: {
                    title: 'Warning',
                    text: 'Meal successfully created.',
                    close: 'Close'
                },
                modalWithMultipleOptions: {
                    option1: 'Save meal',
                    option2: 'Copy meal',
                    option3: 'Quick add'
                },
                modalWithSingleOption: {
                    option1: 'Quick add'
                },
                modalCheckFilledFields: {
                    title: 'Warning',
                    text: 'All fields must be filled.'
                },
                modalErrorCreateFood: {
                    title: 'Warning',
                    text: 'You have already created a food with this name. Please choose another name.'
                },
                modalHeight: {
                    title: 'Height',
                    text: 'cm'
                },
                modalAge: {
                    title: 'Age'
                },
                modalErrorHeight: {
                    text: 'Enter your height.'
                },
                modalErrorAge: {
                    text: 'Enter your age.'
                },
                modalWater: {
                    title: 'Cups to add',
                    text: 'cups of 250 ml'
                },
                modalErrorWater: {
                    text: 'Enter the number of glasses of 250 ml of water you want to register.'
                },
                modalAddMeal: {
                    title: 'Meals',
                    breakfast: 'Breakfast',
                    lunch: 'Lunch',
                    dinner: 'Dinner',
                    snacks: 'Snacks'
                },
                modalErrorLogin: {
                    text: 'Email or password is incorrect.'
                },
                modalErrorEmailRegister: {
                    text: 'The email address you entered is already being used.'
                },
                modalConfirmLogout: {
                    title: 'Log out',
                    text: 'Are you sure you want to log out?',
                    cancel: 'Cancel',
                    yes: 'Yes'
                },
                modalChangeLanguage: {
                    title: 'Warning',
                    text: 'The application will be restarted. It will take a few seconds.',
                    ok: 'Ok'
                },
                modalBadRate: {
                    title: 'Bad rate',
                    text: 'Could you tell us what needs to be improved?',
                    no: 'No',
                    yes: 'Yes'
                },
                modalErrorChooseRate: {
                    title: 'Warning',
                    text: 'You need to select a rate.'
                },
                modalDeleteFriend: {
                    title: 'Warning',
                    text: 'Are you sure you want to delete this friend?',
                    delete: 'Delete'
                },
                modalErrorSendFriendRequest: {
                    title: 'Warning',
                    text: 'We did not find any user with the given email. Would you like to send a message to your friend now?',
                    no: 'No',
                    yes: 'Yes',
                    message: 'Hello! I would like to add you as a friend in the DietofPoints. What is your email? If you have not downloaded the app, download it here:'
                },
                modalSuccessSendFriendRequest: {
                    text: 'Your friend request has been sent.'
                },
                modalAlreadyFriends: {
                    title: 'Warning',
                    text: 'You are already friends.'
                },
                modalOpenFriendRequestSent: {
                    text: 'There is already a friend request open between you and this user.'
                },
                modalOpenFriendRequestReceived: {
                    title: 'Warning',
                    text: 'There is already a friend request open between you and this user. Would you like to accept now?',
                    yes: 'Yes',
                    no: 'No'
                }
            }
        }
    },

    constructor: function() {
        if (this.$className.substr(-2) === lang) {
            this.applyLocale();
        }
    },
    applyLocale: function() {
        console.log('en');
        //
        // Default Sencha wordings
        //

        Ext.Date.monthNames = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        Ext.Date.monthNumbers = {
            'Jan.': 0,
            'Feb.': 1,
            'Mar.': 2,
            'Apr.': 3,
            'May':  4,
            'Jun.': 5,
            'Jul.': 6,
            'Aug.': 7,
            'Sep.': 8,
            'Oct.': 9,
            'Nov.': 10,
            'Dec.': 11
        };

        Ext.Date.shortMonthNames = [
            "Jan.",
            "Feb.",
            "Mar.",
            "Apr.",
            "May",
            "Jun.",
            "Jul.",
            "Aug.",
            "Sep.",
            "Oct.",
            "Nov.",
            "Dec."
        ];
        Ext.Date.getShortDayName = function(day) {
            return Ext.Date.dayNames[day].substring(0, 3);
        };

        Ext.Date.getMonthNumber = function(name) {
            return Ext.Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
        };
        Ext.Date.getShortMonthName = function(month) {
            return Ext.Date.shortMonthNames[month];
        };

        Ext.Date.parseCodes.S.s = '(?:?)';

        Ext.Date.getSuffix = function(date) {
            return '?';
        };

        if (Ext.picker && Ext.picker.Picker) {
            obj = Ext.picker.Picker.prototype.config;
            obj.doneButton = {
                text: 'Done'
            };
            obj.cancelButton = {
                text: 'Cancel'
            };
        }

        if (Ext.picker && Ext.picker.Date) {
            obj = Ext.picker.Date.prototype.config;
            obj.doneButton = {
                text: 'Done'
            };
            obj.cancelButton = {
                text: 'Cancel'
            };
            Ext.apply(obj, {
                'dayText': 'Day',
                'monthText': 'Month',
                'yearText': 'Year',
                'hourText': 'Hour',
                'minuteText': 'Minute',
                'slotOrder': ['month', 'day', 'year'],
                'yearFrom': new Date().getFullYear(),
                'yearTo': new Date().getFullYear()+1
            });
        }

        if (Ext.IndexBar) {
            obj = Ext.IndexBar.prototype.config;
            Ext.apply(obj, {
                letters: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
            });
        }

        if (Ext.LoadMask) {
            obj = Ext.LoadMask.prototype.config;
            Ext.apply(obj, {
                message: 'Loading...',
                indicator: true
            });
        }

        if (Ext.dataview && Ext.dataview.DataView) {
            obj = Ext.dataview.DataView.prototype.config;
            Ext.apply(obj, {
                loadingText: ''
            });
        }

        if (Ext.plugin.ListPaging) {
            obj = Ext.plugin.ListPaging.prototype.config;
            Ext.apply(obj, {
                autoPaging: true,
                loadMoreText: 'More records',
                noMoreRecordsText: 'No more records'
            });
        }

        if (Ext.Button) {
            obj = Ext.Button.prototype.config;
            obj.pressedCls = 'pressedButton'
        }

        if (Ext.MessageBox) {

            Ext.apply(Ext.MessageBox, {
                OK: {cls: 'button_messageBox'}
            });

            Ext.MessageBox.OK.text = 'OK';
            Ext.MessageBox.CANCEL.text = 'Cancel';
            Ext.MessageBox.YES.text = 'Yes';
            Ext.MessageBox.NO.text = 'No';

            Ext.MessageBox.OKCANCEL[0].text = 'Cancel';
            Ext.MessageBox.OKCANCEL[1].text = 'OK';

            Ext.MessageBox.YESNOCANCEL[0].text = 'Cancel';
            Ext.MessageBox.YESNOCANCEL[1].text = 'No';
            Ext.MessageBox.YESNOCANCEL[2].text = 'Yes';

            Ext.MessageBox.YESNO[0].text = 'No';
            Ext.MessageBox.YESNO[1].text = 'Yes';
        }
    }
});