Ext.define('Dietofpoints.model.ListWeight', {

    extend: "Ext.data.Model",
	
    config: {
        fields: [
            {name: "date_weighthistory", type: "string"},
            {name: "weight_weighthistory",  type: "string"},
            {name: "lostWeight", type: "int"},
            {name: "lastWeight", type: "int"},
            {name: "weight_balance", type: "string"}
        ],
		
		remoteFilter: true,
 
		proxy: {
			type: 'ajax',
			reader: {
				type: 'json',
				rootProperty: 'root',
				successProperty: 'success',
				totalProperty: 'totalCount'
			}
		},
		
		autoLoad: true
    }
});