Ext.define("Dietofpoints.view.ListSaveMealMultiplePeriods", {

	extend: 'Ext.List',
	
	xtype: 'listSaveMealMultiplePeriods',

	config: {

		scrollable: 'vertical',
		store: 'ListSaveMealMultiplePeriods',

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
					str += '<div class="titleWithoutCheckbox"><div class="name_food_withoutCheckbox">' + values.name_food + '</div></div>';
					str += '<div class="points_food">' + values.points_food + '</div>';
					if(values.unit_food != 0) {
						str += '<div class="textWithoutCheckbox"><div class="amount_food_withoutCheckbox">' + values.unit_food + ' ' + values.amount_food + '</div></div>';
					}
					return str;
				}
			}
		)
	}
});