Ext.define('Dietofpoints.view.TabpanelMyFoodAndMeals', {

	extend: 'Ext.TabPanel',
	
	xtype: 'tabpanelMyFoodAndMeals',

	config: {

		cls: 'tabpanelMyFoodAndMeals',

		layout: {
			type: 'card',
			animation: null
		}, 

		tabBar: {
            docked: 'top', 
			pack: 'center',
			align: 'center'
        },

		items: [
			{
				xtype: 'listMyFood_tabpanelMyFoodAndMeals'
			},
			{
				xtype: 'listMyMeals_tabpanelMyFoodAndMeals'
			}
		]
	}
});