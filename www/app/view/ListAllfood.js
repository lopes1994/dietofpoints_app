Ext.define("Dietofpoints.view.ListAllFood", {

	extend: 'Ext.List',
	
	xtype: 'listAllFood',

	config: {

		scrollable: 'vertical',
		store: 'ListAllFood',
		flex: 1,
		emptyText: L('emptyText'),

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            switch(L('language.idiom')) {
						case 'pt':
				    		str += '<div class="titleWithotCheckbox"><label><input class="checkbox" type="checkbox" value="' + values.code_food + '"><div class="name_food_withCheckbox">' + values.name_foodpt + '</div></label></div>';
							str += '<div class="points_food">' + values.points_food + '</div>';
							if(values.unit_food != 0) {
								str += '<div class="textWithoutCheckbox"><div class="amount_food_withCheckbox">' + values.unit_food + ' ' + values.amount_foodpt + '</div></div>';
							}
				    	break;
				    	case 'en':
				        	str += '<div class="titleWithoutCheckbox"><label><input class="checkbox" type="checkbox" value="' + values.code_food + '"><div class="name_food_withCheckbox">' + values.name_fooden + '</div></label></div>';
							str += '<div class="points_food">' + values.points_food + '</div>';
							if(values.unit_food != 0) {
								str += '<div class="textWithoutCheckbox"><div class="amount_food_withCheckbox">' + values.unit_food + ' ' + values.amount_fooden + '</div></div>';
							}
				        break;
				    	case 'es':
				        	str += '<div class="titleWithotCheckbox"><label><input class="checkbox" type="checkbox" value="' + values.code_food + '"><div class="name_food_withCheckbox">' + values.name_foodes + '</div></label></div>';
							str += '<div class="points_food">' + values.points_food + '</div>';
							if(values.unit_food != 0) {
								str += '<div class="textWithoutCheckbox"><div class="amount_food_withCheckbox">' + values.unit_food + ' ' + values.amount_foodes + '</div></div>';
							}
					}
					return str;
				}
			}
		)
	}
});