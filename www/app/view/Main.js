Ext.define('Dietofpoints.view.Main', {

    extend: 'Ext.Panel',
	
    xtype: 'main',
	
    config: {

        scrollable: 'vertical',
        cls: 'main',

        layout:{
            type  : 'vbox'
        },
		
        items: [
            {
                xtype: 'image',
                src: null,
                cls: 'logo'
            },
            {
                xtype: 'button',
                cls: 'button_main_register',
                text: L('panel.template.main.register'),
                itemId: 'register'
            },
			{
                xtype: 'button',
                cls: 'button_main_login',
                text: L('panel.template.main.login'),
				itemId: 'login'
            }
        ]
    },

    initialize: function() {
        var me = this;
        me.getItems().get(0).setSrc('./resources/img/logo.png');
    }
});