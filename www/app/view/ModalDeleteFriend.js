Ext.define("Dietofpoints.view.ModalDeleteFriend", {

	extend: 'Ext.Panel',
	
	xtype: 'modalDeleteFriend',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalDeleteFriend.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalDeleteFriend.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalDeleteFriend.delete'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'delete'
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalQuicklyAdd.cancel'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'cancel'
                    }
                ]
            }
        ]
    }
});