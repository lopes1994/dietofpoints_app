Ext.define("Dietofpoints.view.ModalBadRate", {

	extend: 'Ext.Panel',
	
	xtype: 'modalBadRate',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalBadRate.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalBadRate.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalBadRate.no'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'no'
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalBadRate.yes'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'yes'
                    }
                ]
            }
        ]
    }
});