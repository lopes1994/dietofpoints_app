Ext.define('Dietofpoints.model.ListFriend', {

    extend: "Ext.data.Model",
	
    config: {
        fields: [
            {name: "name", type: "string"},
            {name: "weight_balance", type: "string"},
            {name: "text_balance", type: "string"},
            {name: "code", type: "int"}
        ],
		
		remoteFilter: true,
 
		proxy: {
			type: 'ajax',
			reader: {
				type: 'json'
			}
		},
		
		autoLoad: true
    }
});