Ext.define("Dietofpoints.view.PanelTopListWeight", {

	extend: 'Ext.Panel',
	
	xtype: 'panelTopListWeight',

	config: {
	
		cls: 'panelTopListWeight',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    if(values.lostWeight === 0) {
                        str += '<div class="panelTopListWeight_balance1"><i class="fa fa-calculator fa-fw"></i> ' + L('panel.template.panelTopListWeight.won') + ' ' + values.weight_balance + ' ' + L('panel.template.panelTopListWeight.in') + ' ' + values.consecutive_days + ' ' + L('panel.template.panelTopListWeight.days') + '</div>';
                        if(values.haveToLostWeight === 0) {
                            str += '<div class="panelTopListWeight_balance2"><i class="fa fa-flag fa-fw"></i> ' + L('panel.template.panelTopListWeight.goal_win') + ' ' + values.idealWeight_balance + ' ' + L('panel.template.panelTopListWeight.reach') + ' ' + values.idealWeight + '</div>';
                        }
                        else if(values.haveToLostWeight === 2) {
                            str += '<div class="panelTopListWeight_balance2"><i class="fa fa-flag fa-fw"></i> ' + L('panel.template.panelTopListWeight.congratulations') + ' ' + values.idealWeight + '</div>';
                        }
                        else if(values.haveToLostWeight === 1){
                            str += '<div class="panelTopListWeight_balance2"><i class="fa fa-flag fa-fw"></i> ' + L('panel.template.panelTopListWeight.goal_lost') + ' ' + values.idealWeight_balance + ' ' + L('panel.template.panelTopListWeight.reach') + ' ' + values.idealWeight + '</div>';
                        }
                    } 
                    else {
                        str += '<div class="panelTopListWeight_balance1"><i class="fa fa-calculator fa-fw"></i> ' + L('panel.template.panelTopListWeight.lost') + ' ' + values.weight_balance + ' ' + L('panel.template.panelTopListWeight.in') + ' ' + values.consecutive_days + ' ' + L('panel.template.panelTopListWeight.days') + '</div>';
                        if(values.haveToLostWeight === 0) {
                            str += '<div class="panelTopListWeight_balance2"><i class="fa fa-flag fa-fw"></i> ' + L('panel.template.panelTopListWeight.goal_win') + ' ' + values.idealWeight_balance + ' ' + L('panel.template.panelTopListWeight.reach') + ' ' + values.idealWeight + '</div>';
                        }
                        else if(values.haveToLostWeight === 2) {
                            str += '<div class="panelTopListWeight_balance2"><i class="fa fa-flag fa-fw"></i> ' + L('panel.template.panelTopListWeight.congratulations') + ' ' + values.idealWeight + '</div>';
                        }
                        else if(values.haveToLostWeight === 1){
                            str += '<div class="panelTopListWeight_balance2"><i class="fa fa-flag fa-fw"></i> ' + L('panel.template.panelTopListWeight.goal_lost') + ' ' + values.idealWeight_balance + ' ' + L('panel.template.panelTopListWeight.reach') + ' ' + values.idealWeight + '</div>';
                        }
                    }
                    return str;
                }
            }
        )
	}
});