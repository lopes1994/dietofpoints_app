Ext.define("Dietofpoints.view.PanelBottomTabPanelAddFood", {

	extend: 'Ext.Panel',
	
	xtype: 'panelBottomTabPanelAddFood',

	config: {

        docked: 'bottom',

        layout: {
            type: 'vbox'
        },

        items: [
            {
                xtype: 'panel',
                cls: 'panelCreateFood',
                layout: 'hbox',
                style: 'border-bottom: 0px;',
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        text: L('panel.template.panelAllFood.createFood'),
                        cls: 'buttonCreateFood',
                        itemId: 'createFood',
                        pressedCls: 'pressedCreateFood'
                    },
                    {
                        xtype: 'spacer'
                    }
                ]
            },
            {
                xtype: 'panel',
                cls: 'panelCreateFood',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        text: L('panel.template.panelAllFood.quickAdd'),
                        cls: 'buttonCreateFood',
                        itemId: 'quickAdd',
                        pressedCls: 'pressedCreateFood'
                    },
                    {
                        xtype: 'spacer'
                    }
                ]
            }
        ]
	}
});