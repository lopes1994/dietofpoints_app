Ext.define("Dietofpoints.view.PanelMenu", {

	extend: 'Ext.Panel',
	
	xtype: 'panelMenu',

	config: {
	
		cls: 'panelMenu',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="username"><i class="fa fa-user fa-fw"></i> ' + values.username + '</div>';
                    str += '<div class="result_user"><div class="consecutive_days"><i class="fa fa-calendar fa-fw"></i> ' + values.consecutive_days + ' ' + L('panel.template.panelMenu.consecutive_days') + '</div>';
                    str += '<div class="balance"><i class="fa fa-flag fa-fw"></i> ' + values.text_balance + ' ' + values.weight_balance + '</div>';
                    return str;
                }
            }
        )
	}
});