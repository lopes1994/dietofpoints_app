/**
 *  Resource file for Spanish language.
 */
Ext.define('Dietofpoints.locale.es', {
    singleton: true,
    requires: [
        'Ext.MessageBox',
        'Ext.picker.Picker',
        'Ext.picker.Date'
    ],
    translation: {
        language: {
            idiom: 'es'
        },
        buttons: {
            save: 'Guardar',
            next: 'Siguiente',
            add: 'Añadir'
        },
        socialMessage: {
            text: 'Toma el control de su dieta diaria en la aplicación de la dieta de los puntos. Es muy fácil, descargar ahora de forma gratuita:'
        },
        notifications: {
            title: 'Registrar sus comidas diarias',
            text: 'Es el momento de registrarse sus comidas. ¿Vamos allá?'
        },
        rate: {
            title: 'Valore la aplicación en',
            buttons: {
                '1': 'No volver a preguntar',
                '2': 'Después',
                '3': 'Evaluar'
            }
        },
        dateFormat: 'd/m/Y',
        emptyText: 'No se encontraron resultados',
        ERROR_TITLE: 'Se produjo un error',
        messageBox_networkError: 'Error de red. Inténtelo de nuevo!',
        menu: {
            diary: 'Diario',
            profile: 'Perfil',
            myFoodAndMeals: 'Mi comida y comidas',
            reminders: 'Recordatorios',
            logout: 'Salir',
            help: 'Ayuda',
            myWeight: 'Mi peso',
            about: 'Acerca de',
            share: 'Compartir',
            friend: 'Amigos',
            friendRequest: 'Amigos solicitudes'
        },
        list: {
            template: {
                listWeight: {
                    last_weight: 'Peso inicial'
                },
                listMyFood_tabpanelMyFoodAndMeals: {
                    title: 'Mi comida'
                },
                listMyMeals_tabpanelMyFoodAndMeals: {
                    title: 'Mis comidas'
                },
                listFriend: {
                    title: 'Amigos',
                    won: 'Won',
                    lost: 'Perdido'
                },
                listFriendRequest: {
                    title: 'Amigos solicitudes'
                }
            }
        },
        panel: {
            template: {
                panelTitlePeriodBreakfast: 'Desayuno',
                panelTitlePeriodLunch: 'Almuerzo',
                panelTitlePeriodDinner: 'Cena',
                panelTitlePeriodSnacks: 'Aperitivos',
                panelTitleWater: {
                    title: 'Agua',
                    cups: 'tazas de 250 ml'
                },
                diary: 'Diario',
                select_item: 'Seleccione un elemento',
                panelInitialWeight: {
                    title: 'Peso inicial'
                },
                panelActualWeight: {
                    title: 'Peso actual'
                },
                panelGoalWeight: {
                    title: 'Objetivo de peso'
                },
                panelActivityLevel: {
                    title: 'Nivel de actividad'
                },
                panelHeight: {
                    title: 'Altura'
                },
                panelAge: {
                    title: 'Edad'
                },
                panelProfile: {
                    title: 'Perfil'
                },
                panelListWeight: {
                    title: 'Historia de peso'
                },
                panelTopListWeight: {
                    won: 'Ganaste',
                    lost: 'Tú perdiste',
                    in: 'en',
                    days: 'días',
                    goal_lost: 'Usted necesita perder',
                    goal_win: 'Usted necesita para ganar',
                    reach: 'para lograr el objetivo de',
                    congratulations: 'Felicidades, usted consiguió alcanzar el objetivo de'
                },
                panelMenu: {
                    consecutive_days: 'días consecutivos',
                    text_balance_positive: 'Won',
                    text_balance_negative: 'Perdido'
                },
                panelAllFood: {
                    title: 'Toda la comida',
                    search: 'Investigación',
                    searchfield: ' Introduzca el nombre del alimento',
                    createFood: 'Crear un alimento',
                    quickAdd: 'Calories "Quick add"'
                },
                panelMyFood: 'Mi comida',
                panelMyMeals: {
                    title: 'Mis comidas',
                    searchfield: 'Introduzca el nombre de la comida'
                },
                panelOptionsDiary: {
                    addFood: 'Añadir alimentos',
                    more: 'Más'
                },
                paneldatadiary: {
                    goal: 'Meta',
                    food: 'Comida',
                    pointsbalance: 'Saldo de puntos'
                },
                login: {
                    email: 'Correo electrónico',
                    password: 'Contraseña',
                    login: 'Entrar'
                },
                main: {
                    register: 'Registro',
                    login: 'Entrar'
                },
                panelSaveMeal: {
                    title: 'Guardar la comida',
                    textfield: 'Introduzca el nombre de la comida',
                    button: 'Guardar',
                    panel: 'Los productos contenidos en esta comida'
                },
                panelSaveMealMultiplePeriods: {
                    textfield: 'Introduzca el nombre de la comida',
                    button: 'Guardar',
                    panel: 'Los productos contenidos en esta comida'
                },
                panelCreateFood: {
                    title: 'Crea tu comida',
                    name: 'Nombre',
                    amount: 'Unidad (cuchara sopera, ml, g)',
                    unit: 'Número de porciones (1, 2, 350, 400)',
                    points: 'Calorías'
                },
                panelUpdatePortion: {
                    title: 'Cambiar la porción',
                    portions: 'Número de porciones'
                },
                panelUpdateCalories: {
                    title: 'Cambiar las calorías',
                    calories: 'Calorías'
                },
                panelActivityLevel_user: {
                    title: 'Nivel de actividad'
                },
                tabpanelMyFoodAndMeals: {
                    title: 'Mi comida y comidas'
                },
                panelUpdateUserFood: {
                    title: 'Cambiar comida',
                    name: 'Nombre',
                    amount: 'Unidad (cuchara sopera, ml, g)',
                    portions: 'Número de porciones (1, 2, 350, 400)',
                    points: 'Calorías'
                },
                panelAddMeal: {
                    title: 'Añadir comida'
                },
                panelIdiom: {
                    title: 'Seleccione su idioma',
                    english: 'English',
                    spanish: 'Español',
                    portuguese: 'Português'
                },
                panelYou: {
                    title: 'Usted',
                    nameGender: 'Género',
                    age: 'Edad',
                    height: 'Altura (cm)',
                    weight: 'Peso actual (kg)',
                    goalWeight: 'Objetivo de peso',
                    name: 'Nombre'
                },
                panelGender_you: {
                    male: 'Masculino',
                    female: 'Femenino'
                },
                panelAccessData: {
                    title: 'Datos de acceso',
                    email: 'Correo electrónico',
                    password: 'Contraseña'
                },
                login: {
                    title: 'Entrar',
                    email: 'Correo electrónico',
                    password: 'Contraseña',
                    login: 'Entrar'
                },
                panelAbout: {
                    title: 'Acerca de',
                    text: 'En la Dieta de puntos requerido seguir la cantidad de puntos que se puede ingerir al día. No se le permite comer alimentos que tienen grasas trans. Se permite cualquier otro alimento, pero hay que usar el sentido común al montaje de su menú, la elección de los alimentos más sanos que ser capaz de mantener una dieta equilibrada. Evitar la falta de nutrientes para evitar futuros problemas de salud.'
                },
                panelAddFriend: {
                    title: 'Añadir amigo',
                    email: 'Correo electrónico'
                }
            }
        },
        modal: {
            template: {
                modalConfirmDeleteItems: {
                    title: 'Borrar',
                    text: '¿Está seguro de que desea eliminar este artículo?',
                    cancel: 'Cancelar',
                    delete: 'Borrar'
                },
                modalCopyMeal: {
                    title: 'Copiar en qué fecha?',
                    today: 'Hoy',
                    yesterday: 'Ayer',
                    tomorrow: 'Mañana'
                },
                modalErrorCreateMeal: {
                    title: 'Advertencia',
                    text: '¿Ha creado una comida con este nombre. Por favor, elija otro nombre.',
                    close: 'Cerrar'
                },
                modalErrorDeleteItems: {
                    title: 'Advertencia',
                    text: 'No hay elementos seleccionados.',
                    close: 'Cerrar'
                },
                modalErrorQuicklyAdd: {
                    title: 'Advertencia',
                    text: 'Introduce el número de calorías que desea registrar.',
                    close: 'Cerrar'
                },
                modalErrorAddWeight: {
                    text: 'Introduce el número de peso que desea registrar.'
                },
                modalErrorWithoutNameCreateMeal: {
                    title: 'Advertencia',
                    text: 'Debe ingresar un nombre para esta comida.',
                    close: 'Cerrar'
                },
                modalOptionsSelectItem: {
                    option1: 'Copia hasta la fecha',
                    option2: 'Guardar la comida',
                    option3: 'Seleccionar todo'
                },
                modalQuicklyAdd: {
                    title: 'Calorías para añadir',
                    text: 'Calorías',
                    cancel: 'Cancelar',
                    add: 'Añadir'
                },
                modalAddWeight: {
                    title: 'Peso para agregar',
                    text: 'kg'
                },
                modalSuccessCreateMeal: {
                    title: 'Advertencia',
                    text: 'La comida se ha creado correctamente.',
                    close: 'Cerrar'
                },
                modalWithMultipleOptions: {
                    option1: 'Guardar la comida',
                    option2: 'Copiar la comida',
                    option3: 'Quick add'
                },
                modalWithSingleOption: {
                    option1: 'Quick add'
                },
                modalCheckFilledFields: {
                    title: 'Advertencia',
                    text: 'Todos los campos son obligatorios.'
                },
                modalErrorCreateFood: {
                    title: 'Advertencia',
                    text: '¿Ha creado un alimento con este nombre. Por favor, elija otro nombre.'
                },
                modalHeight: {
                    title: 'Altura',
                    text: 'cm'
                },
                modalAge: {
                    title: 'Edad'
                },
                modalErrorHeight: {
                    text: 'Introduzca su altura.'
                },
                modalErrorAge: {
                    text: 'Introduzca su edad.'
                },
                modalWater: {
                    title: 'Tazas para añadir',
                    text: 'tazas de 250 ml'
                },
                modalErrorWater: {
                    text: 'Introduce el número de tazas de 250 ml de agua que desea registrar.'
                },
                modalAddMeal: {
                    title: 'Comidas',
                    breakfast: 'Desayuno',
                    lunch: 'Almuerzo',
                    dinner: 'Cena',
                    snacks: 'Aperitivos'
                },
                modalErrorLogin: {
                    text: 'Correo electrónico o la contraseña es incorrecta.'
                },
                modalErrorEmailRegister: {
                    text: 'La dirección de correo electrónico que ha introducido ya está siendo utilizado.'
                },
                modalConfirmLogout: {
                    title: 'Salir',
                    text: '¿Seguro que quieres salir?',
                    cancel: 'Cancelar',
                    yes: 'Sí'
                },
                modalChangeLanguage: {
                    title: 'Advertencia',
                    text: 'La aplicación se reiniciará. Se llevará unos segundos.',
                    ok: 'Ok'
                },
                modalBadRate: {
                    title: 'Mala evaluación',
                    text: '¿Podría decirnos lo que necesita ser mejorado?',
                    no: 'No',
                    yes: 'Sí'
                },
                modalErrorChooseRate: {
                    title: 'Advertencia',
                    text: 'Es necesario seleccionar una evaluación.'
                },
                modalDeleteFriend: {
                    title: 'Advertencia',
                    text: '¿Está seguro de que quiere eliminar este amigo?',
                    delete: 'Borrar'
                },
                modalErrorSendFriendRequest: {
                    title: 'Advertencia',
                    text: 'No hemos encontrado ningún usuario con este correo electrónico. ¿Le gustaría enviar un mensaje a su amigo ahora?',
                    no: 'No',
                    yes: 'Sí',
                    message: 'Hola! Me gustaría añadir que como un amigo en la DietofPoints. ¿Cuál es tu email? Si no ha descargado la aplicación, puede descargarlo aquí:'
                },
                modalSuccessSendFriendRequest: {
                    text: 'Su solicitud de amigo ha sido enviado.'
                },
                modalAlreadyFriends: {
                    title: 'Advertencia',
                    text: 'Ya son amigos.'
                },
                modalOpenFriendRequestSent: {
                    text: 'Ya existe una solicitud de amistad abierta entre usted y este usuario.'
                },
                modalOpenFriendRequestReceived: {
                    title: 'Advertencia',
                    text: 'Ya existe una solicitud de amistad abierta entre usted y este usuario. ¿Le gustaría aceptar ahora?',
                    yes: 'Sí',
                    no: 'No'
                }
            }
        }
    },

    constructor: function() {
        if (this.$className.substr(-2) === lang) {
            this.applyLocale();
        }
    },
    applyLocale: function() {
        console.log('es');
        //
        // Default Sencha wordings
        //

        Ext.Date.monthNames = [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        ];

        Ext.Date.monthNumbers = {
            'Jan.': 0,
            'Feb.': 1,
            'Mar.': 2,
            'Apr.': 3,
            'May':  4,
            'Jun.': 5,
            'Jul.': 6,
            'Aug.': 7,
            'Sep.': 8,
            'Oct.': 9,
            'Nov.': 10,
            'Dec.': 11
        };

        Ext.Date.shortMonthNames = [
            "Ene.",
            "Feb.",
            "Mar.",
            "Abr.",
            "May",
            "Jun.",
            "Jul.",
            "Ago.",
            "Sep.",
            "Oct.",
            "Nov.",
            "Dic."
        ];
        Ext.Date.getShortDayName = function(day) {
            return Ext.Date.dayNames[day].substring(0, 3);
        };

        Ext.Date.getMonthNumber = function(name) {
            return Ext.Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
        };
        Ext.Date.getShortMonthName = function(month) {
            return Ext.Date.shortMonthNames[month];
        };

        Ext.Date.parseCodes.S.s = '(?:?)';

        Ext.Date.getSuffix = function(date) {
            return '?';
        };

        if (Ext.picker && Ext.picker.Picker) {
            obj = Ext.picker.Picker.prototype.config;
            obj.doneButton = {
                text: 'Ok'
            };
            obj.cancelButton = {
                text: 'Cancelar'
            };
        }

        if (Ext.picker && Ext.picker.Date) {
            obj = Ext.picker.Date.prototype.config;
            obj.doneButton = {
                text: 'OK'
            };
            obj.cancelButton = {
                text: 'Cancelar'
            };
            Ext.apply(obj, {
                'dayText': 'Día',
                'monthText': 'Mes',
                'yearText': 'Año',
                'hourText': 'Hora',
                'minuteText': 'Minuto',
                'slotOrder': ['day', 'month', 'year'],
                'yearFrom': new Date().getFullYear(),
                'yearTo': new Date().getFullYear()+1
            });
        }

        if (Ext.IndexBar) {
            obj = Ext.IndexBar.prototype.config;
            Ext.apply(obj, {
                letters: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
            });
        }

        if (Ext.LoadMask) {
            obj = Ext.LoadMask.prototype.config;
            Ext.apply(obj, {
                message: 'Cargando...',
                indicator: true
            });
        }

        if (Ext.dataview && Ext.dataview.DataView) {
            obj = Ext.dataview.DataView.prototype.config;
            Ext.apply(obj, {
                loadingText: ''
            });
        }

        if (Ext.plugin.ListPaging) {
            obj = Ext.plugin.ListPaging.prototype.config;
            Ext.apply(obj, {
                autoPaging: true,
                loadMoreText: 'Más registros',
                noMoreRecordsText: 'No hay más registros'
            });
        }

        if (Ext.Button) {
            obj = Ext.Button.prototype.config;
            obj.pressedCls = 'pressedButton'
        }

        if (Ext.MessageBox) {

            Ext.apply(Ext.MessageBox, {
                OK: {cls: 'button_messageBox'}
            });

            Ext.MessageBox.OK.text = 'Ok';
            Ext.MessageBox.CANCEL.text = 'Cancelar';
            Ext.MessageBox.YES.text = 'Sí';
            Ext.MessageBox.NO.text = 'No';

            Ext.MessageBox.OKCANCEL[0].text = 'Cancelar';
            Ext.MessageBox.OKCANCEL[1].text = 'Ok';

            Ext.MessageBox.YESNOCANCEL[0].text = 'Cancelar';
            Ext.MessageBox.YESNOCANCEL[1].text = 'No';
            Ext.MessageBox.YESNOCANCEL[2].text = 'Sí';

            Ext.MessageBox.YESNO[0].text = 'No';
            Ext.MessageBox.YESNO[1].text = 'Sí';
        }
    }
});