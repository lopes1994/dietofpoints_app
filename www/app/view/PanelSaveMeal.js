Ext.define("Dietofpoints.view.PanelSaveMeal", {

	extend: 'Ext.Panel',
	
	xtype: 'panelSaveMeal',

	config: {
	
        scrollable: false,

        layout:{
            type  : 'vbox'
        },

        items: [
            {
                xtype: 'panel',
                docked: 'top',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'textfield',
                        placeHolder: L('panel.template.panelSaveMeal.textfield'),
                        cls: 'textfield_panelSaveMeal',
                        itemId: 'meal',
                        maxLength: 100,
                        flex: 1,
                        listeners: [
                            {
                                fn: function(element, eOpts) {
                                    element.down('input').dom.focus();
                                },
                                event: 'painted'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        itemId: 'save',
                        cls: 'buttonSave_panelSaveMeal',
                        text: L('panel.template.panelSaveMeal.button')
                    }
                ]
            },
            {
                xtype: 'panel',
                docked: 'top',
                cls: 'panelTitleList_panelSaveMeal',
                html: L('panel.template.panelSaveMeal.panel')
            },
            {
                xtype: 'listSaveMeal',
                flex: 1
            }
        ]
	}
});