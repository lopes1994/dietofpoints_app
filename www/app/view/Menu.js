Ext.define('Dietofpoints.view.Menu', {

    extend: 'Ext.Menu',
    xtype: 'menu',
    
    config: {

		width: '85%',
		layout: 'vbox',
		animation: null,
		cls: 'menu',
        scrollable: {
	        direction: 'vertical',
	        directionLock: true
	    },
        items: [
        	{
        		xtype: 'panelMenu',
        		docked: 'top'
        	},
			{
                xtype: 'button',
				cls: 'menuButton',
				itemId: 'diary',
                text: L('menu.diary'),
                iconCls: 'fa fa-book fa-fw'
            },
            {
                xtype: 'button',
				cls: 'menuButton',
				itemId: 'profile',
                text: L('menu.profile'),
                iconCls: 'fa fa-user fa-fw'
            },
            {
				xtype: 'button',
				cls: 'menuButton',
				itemId: 'myFoodAndMeals',
                text: L('menu.myFoodAndMeals'),
                iconCls: 'fa fa-cutlery fa-fw'
			},
			{
				xtype: 'button',
				cls: 'menuButton',
				itemId: 'myWeight',
                text: L('menu.myWeight'),
                iconCls: 'fa fa-calculator fa-fw'
			},
			{
				xtype: 'button',
				cls: 'menuButton',
				itemId: 'help',
				text: L('menu.help'),
                iconCls: 'fa fa-question fa-fw'
			},
			{
				xtype: 'button',
				cls: 'menuButton',
				itemId: 'about',
				text: L('menu.about'),
                iconCls: 'fa fa-info-circle fa-fw'
			},
			{
				xtype: 'button',
				cls: 'menuButton',
				itemId: 'friend',
				text: L('menu.friend'),
                iconCls: 'fa fa-users fa-fw'
			},
			{
				xtype: 'button',
				cls: 'menuButton',
				itemId: 'share',
				text: L('menu.share'),
                iconCls: 'fa fa-share-alt fa-fw'
			},
            {					
				xtype: 'button',
				cls: 'menuButton',
				itemId: 'logout',
                text: L('menu.logout'),
                iconCls: 'fa fa-sign-out fa-fw'
			}
        ]
    }
});