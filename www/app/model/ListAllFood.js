Ext.define('Dietofpoints.model.ListAllFood', {

    extend: "Ext.data.Model",
	
    config: {
        fields: [
            {name: "name_food" + L('language.idiom'), type: "string"},
            {name: "amount_food" + L('language.idiom'),  type: "string"},
            {name: "points_food", type: "int"},
            {name: "code_food", type: "int"},
            {name: "unit_food", type: "int"}
        ],
		
		remoteFilter: true,
 
		proxy: {
			type: 'ajax',
			reader: {
				type: 'json',
				rootProperty: 'root',
				successProperty: 'success',
				totalProperty: 'totalCount'
			}
		},
		
		autoLoad: true
    }
});