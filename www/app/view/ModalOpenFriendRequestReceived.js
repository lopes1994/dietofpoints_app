Ext.define("Dietofpoints.view.ModalOpenFriendRequestReceived", {

	extend: 'Ext.Panel',
	
	xtype: 'modalOpenFriendRequestReceived',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalOpenFriendRequestReceived.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalOpenFriendRequestReceived.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalOpenFriendRequestReceived.no'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'no'
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalOpenFriendRequestReceived.yes'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'yes'
                    }
                ]
            }
        ]
    }
});