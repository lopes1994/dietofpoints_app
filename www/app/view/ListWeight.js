Ext.define("Dietofpoints.view.ListWeight", {

	extend: 'Ext.List',
	
	xtype: 'listWeight',

	config: {

		scrollable: 'vertical',
		store: 'ListWeight',
		flex: 1,

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            if(L('dateFormat') === 'd/m/Y') {
		            	str += '<div class="date_listWeight">' + values.date_weighthistory + '</div>';
		            }
		            else {
		            	str += '<div class="date_listWeight">' + values.date_weighthistory.substr(3, 2) + '/' + values.date_weighthistory.substr(0, 2) + '/' + values.date_weighthistory.substr(6) + '</div>';
		            }
					str += '<div class="weight_listWeight">' + values.weight_weighthistory + ' kg' + '</div>';
					if(values.lostWeight != null) {
						if(values.lostWeight == 0) {
							str += '<div class="notLostWeight_listWeight">' + L('panel.template.panelMenu.text_balance_positive') + ' ' + values.weight_balance + ' kg' + '</div>';
						}
						else {
							str += '<div class="lostWeight_listWeight">' + L('panel.template.panelMenu.text_balance_negative') + ' ' + values.weight_balance + ' kg' + '</div>';
						}
					}
					if(values.lastWeight == 1) {
						str += '<div class="lastWeight_listWeight">' + L('list.template.listWeight.last_weight') + '</div>';
					}
					return str;
				}
			}
		)
	}
});