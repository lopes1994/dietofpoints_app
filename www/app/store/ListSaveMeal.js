Ext.define("Dietofpoints.store.ListSaveMeal", {

	extend: 'Ext.data.Store',

	config: {
		model: 'Dietofpoints.model.ListSaveMeal'
	}
});