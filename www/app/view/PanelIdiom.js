Ext.define("Dietofpoints.view.PanelIdiom", {

	extend: 'Ext.Panel',
	
	xtype: 'panelIdiom',

	config: {

        scrollable: 'vertical',

        layout:{
            type  : 'vbox'
        },

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) { 
                    var str = '';
                    str += '<label><div class="textWithRadioOrCheckbox"><input name="item" class="radio" type="radio" value=' + values.code1 + '>' + values.name1 + '</div></label>';
                    str += '<label><div class="textWithRadioOrCheckbox"><input name="item" class="radio" type="radio" value=' + values.code2 + '>' + values.name2 + '</div></label>';
                    str += '<label><div class="textWithRadioOrCheckbox"><input name="item" class="radio" type="radio" value=' + values.code3 + '>' + values.name3 + '</div></label>';
                    return str;
                }
            }
        )
	}
});