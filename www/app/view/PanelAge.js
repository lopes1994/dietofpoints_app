Ext.define("Dietofpoints.view.PanelAge", {

	extend: 'Ext.Panel',
	
	xtype: 'panelAge',

	config: {

        cls: 'panelProfile',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="panelProfile_textLeft">' + L('panel.template.panelAge.title') + '</div>';
                    str += '<div class="panelProfile_textRight">' + values.text + '</div>';
                    return str;
                }
            }
        )
	},

    initialize: function() {

        var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showModalAge,
            delegate: ['.panelProfile_textRight'],
            scope: me
        });
    },

    showModalAge: function() {
        Dietofpoints.app.getController('Diary').showModalAge();
    }
});