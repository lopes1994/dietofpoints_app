Ext.define('Dietofpoints.controller.GoogleAnalytics', {

	extend: 'Ext.app.Controller', 

	startTrackerWithId: function(id) {
		if(window.cordova) {
			window.analytics.startTrackerWithId(id);
		}
		else {
			console.log('startTrackerWithId: ' + id);
		}
	},

	trackView: function(viewTitle) {
		if(window.cordova) {
			window.analytics.trackView(viewTitle);
		}
		else {
			console.log('trackView: ' + viewTitle);
		}
	},

	trackEvent: function(category, action, label, value) {
		if(window.cordova) {
			window.analytics.trackEvent(category, action, label, value);
		}
		else {
			console.log('trackEvent: ' + category + ' ' + action + ' ' + label + ' ' + value);
		}
	},

	trackException: function(description, fatal) {
		if(window.cordova) {
			window.analytics.trackException(description, fatal);
		}
		else {
			console.log('trackException: ' + description + ' ' + fatal);
		}
	},

	trackTiming: function(category, intervalInMilliseconds, variable, label) {
		if(window.cordova) {
			window.analytics.trackTiming(category, intervalInMilliseconds, variable, label);
		}
		else {
			console.log('trackTiming: ' + category + ' ' + intervalInMilliseconds + ' ' + variable + ' ' + label);
		}
	}
});