Ext.define("Dietofpoints.view.ListMyFood", {

	extend: 'Ext.List',
	
	xtype: 'listMyFood',

	config: {

		scrollable: 'vertical',
		store: 'ListMyFood',
		flex: 1,
		emptyText: L('emptyText'),

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            str += '<div class="titleWithoutCheckbox"><label><input class="checkbox" type="checkbox" value="' + values.code_userfood + '"><div class="name_food_withCheckbox">' + values.name_food + '</div></label></div>';
					str += '<div class="points_food">' + values.points_food + '</div>';
					if(values.unit_food != 0) {
						str += '<div class="textWithoutCheckbox"><div class="amount_food_withCheckbox">' + values.unit_food + ' ' + values.amount_food + '</div></div>';
					}
					return str;
				}
			}
		)
	}
});