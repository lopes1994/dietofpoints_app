Ext.define("Dietofpoints.view.ModalChangeLanguage", {

	extend: 'Ext.Panel',
	
	xtype: 'modalChangeLanguage',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalChangeLanguage.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalChangeLanguage.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalChangeLanguage.ok'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'ok'
                    }
                ]
            }
        ]
    }
});