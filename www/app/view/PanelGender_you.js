Ext.define("Dietofpoints.view.PanelGender_you", {

	extend: 'Ext.Panel',
	
	xtype: 'panelGender_you',

	config: {

        layout:{
            type  : 'vbox'
        },
        height: 'auto',
        cls: 'panelGender_you',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) { 
                    var str = '';
                    str += '<label><div class="textWithRadioOrCheckbox"><input name="item" class="radio" type="radio" value=' + values.code1 + '>' + values.name1 + '</div></label>';
                    str += '<label><div class="textWithRadioOrCheckbox"><input name="item" class="radio" type="radio" value=' + values.code2 + '>' + values.name2 + '</div></label>';
                    return str;
                }
            }
        )
	}
});