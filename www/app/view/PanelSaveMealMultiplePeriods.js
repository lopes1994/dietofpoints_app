Ext.define("Dietofpoints.view.PanelSaveMealMultiplePeriods", {

	extend: 'Ext.Panel',
	
	xtype: 'panelSaveMealMultiplePeriods',

	config: {
	
        scrollable: false,

        layout:{
            type  : 'vbox'
        },

        items: [
            {
                xtype: 'panel',
                docked: 'top',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'textfield',
                        placeHolder: L('panel.template.panelSaveMealMultiplePeriods.textfield'),
                        cls: 'textfield_panelSaveMeal',
                        itemId: 'meal',
                        maxLength: 100,
                        flex: 1,
                        listeners: [
                            {
                                fn: function(element, eOpts) {
                                    element.down('input').dom.focus();
                                },
                                event: 'painted'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        itemId: 'save',
                        cls: 'buttonSave_panelSaveMeal',
                        text: L('panel.template.panelSaveMealMultiplePeriods.button')
                    }
                ]
            },
            {
                xtype: 'panel',
                docked: 'top',
                cls: 'panelTitleList_panelSaveMeal',
                html: L('panel.template.panelSaveMealMultiplePeriods.panel')
            },
            {
                xtype: 'listSaveMealMultiplePeriods',
                flex: 1
            }
        ]
	}
});