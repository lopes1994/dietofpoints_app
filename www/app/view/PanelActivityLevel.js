Ext.define("Dietofpoints.view.PanelActivityLevel", {

	extend: 'Ext.Panel',
	
	xtype: 'panelActivityLevel',

	config: {

        cls: 'panelProfile',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="panelProfile_textLeft">' + L('panel.template.panelActivityLevel.title') + '</div>';
                    str += '<div class="panelProfile_textRight">' + values.text + '</div>';
                    return str;
                }
            }
        )
	},

    initialize: function() {

        var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showPanelActivityLevel_user,
            delegate: ['.panelProfile_textRight'],
            scope: me
        });
    },

    showPanelActivityLevel_user: function() {
        Dietofpoints.app.getController('Diary').showPanelActivityLevel_user();
    }
});