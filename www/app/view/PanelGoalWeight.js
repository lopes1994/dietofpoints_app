Ext.define("Dietofpoints.view.PanelGoalWeight", {

	extend: 'Ext.Panel',
	
	xtype: 'panelGoalWeight',

	config: {

        cls: 'panelProfile',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="panelProfile_textLeft">' + L('panel.template.panelGoalWeight.title') + '</div>';
                    str += '<div class="panelProfile_textRight">' + values.text + ' kg' + '</div>';
                    return str;
                }
            }
        )
	},

    initialize: function() {

        var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showModalGoalWeight,
            delegate: ['.panelProfile_textRight'],
            scope: me
        });
    },

    showModalGoalWeight: function() {
        Dietofpoints.app.getController('Diary').showModalGoalWeight();
    }
});