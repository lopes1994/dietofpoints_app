Ext.define("Dietofpoints.view.PanelDatepicker", {

	extend: 'Ext.Panel',
	
	xtype: 'panelDatepicker',

	config: {

        layout: 'hbox',
        docked: 'top',
        cls: 'panelDatepicker',

        items: [
            {
                xtype: 'button',
                iconCls: 'fa fa-chevron-left fa-fw',
                ui: 'plain',
                itemId: 'buttonBack',
                cls: 'buttonBack_panelDatepicker'
            },
            {
                xtype: 'datepicker_diary',
                flex: 1,
            },
            {
                xtype: 'button',
                iconCls: 'fa fa-chevron-right fa-fw',
                ui: 'plain',
                itemId: 'buttonGo',
                cls: 'buttonGo_panelDatepicker'
            }
        ]
	}
});