Ext.define('Dietofpoints.view.Navigationview', {

    extend: 'Ext.navigation.View',
    
    xtype: 'navigationview',
                
    config: {
    
        flex: 1,

        defaultBackButtonText: '',
        
        scrollable: null,

        layout: {
            type: 'card',
            animation: null
        },

        navigationBar: {

            backButton : {
                iconCls : 'fa fa-chevron-left fa-fw',
                ui: 'plain',
                pressedCls: 'pressedButton_Navigationview'
            },

            items: [
                {
                    xtype: 'button',
                    iconCls: 'fa fa-bars fa-fw',
                    hidden: true,
                    ui: 'plain',
                    itemId: 'menu_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-times fa-fw',
                    hidden: true,
                    ui: 'plain',
                    itemId: 'close_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-pencil fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'edit_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-trash fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'trash_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-ellipsis-v fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'options_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-check fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'add_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.save'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'saveCreateFood_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.save'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'saveUpdatePortion_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.save'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'saveUpdateCalories_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-pencil fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'registerWeight_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.save'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'saveActivityLevel_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.next'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'saveActivityLevelRegister_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-plus fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'addTabpanelMyFoodAndMeals_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-pencil fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'editTabpanelMyFoodAndMeals_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-trash fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'trashTabpanelMyFoodAndMeals_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-times fa-fw',
                    hidden: true,
                    ui: 'plain',
                    itemId: 'closeTabpanelMyFoodAndMeals_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.save'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'updateUserFood_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.save'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'saveCreateFoodFromTabpanelMyFoodAndMeals_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.add'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'addMeal_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.save'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'saveIdiom_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.next'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'savePanelYou_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.next'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'savePanelAccessData_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-plus fa-fw',
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    itemId: 'addFriend_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                },
                {
                    xtype: 'button',
                    text: L('buttons.add'),
                    hidden: true,
                    ui: 'plain',
                    align: 'right',
                    cls: 'buttonSave_navigationbar',
                    itemId: 'sendFriendRequest_navigationview',
                    pressedCls: 'pressedButton_Navigationview'
                }
            ]
        }
    }
});