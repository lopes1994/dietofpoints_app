Ext.define("Dietofpoints.view.ListMyFood_tabpanelMyFoodAndMeals", {

	extend: 'Ext.List',
	
	xtype: 'listMyFood_tabpanelMyFoodAndMeals',

	config: {

		title: L('list.template.listMyFood_tabpanelMyFoodAndMeals.title'),
		scrollable: 'vertical',
		store: 'ListMyFood_tabpanelMyFoodAndMeals',
		flex: 1,
		emptyText: L('emptyText'),

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            str += '<div class="titleWithoutCheckbox"><div class="name_food_withoutCheckbox">' + values.name_food + '</div></div>';
		            str += '<div class="titleWithCheckbox"><label><input class="checkbox" type="checkbox" value="' + values.code_userfood + '"><div class="name_food_withCheckbox">' + values.name_food + '</div></label></div>';
					str += '<div class="points_food">' + values.points_food + '</div>';
					if(values.unit_food != 0) {
						str += '<div class="textWithoutCheckbox"><div class="amount_food_withoutCheckbox">' + values.unit_food + ' ' + values.amount_food + '</div></div>';
						str += '<div class="textWithCheckbox"><div class="amount_food_withCheckbox">' + values.unit_food + ' ' + values.amount_food + '</div></div>';
					}
					str += '<div class="code">' + values.code_userfood + '</div>';
					return str;
				}
			}
		)
	},

	initialize: function() {

		var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showPanelUpdateUserFood,
            delegate: ['.titleWithoutCheckbox .name_food_withoutCheckbox'],
            scope: me
        });

        listElement.on({
            tap: me.showPanelUpdateUserFood,
            delegate: ['.textWithoutCheckbox .amount_food_withoutCheckbox'],
            scope: me
        });
	},

	showPanelUpdateUserFood: function(event, element) {
		var me = this;
        var record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord();
		Dietofpoints.app.getController('Diary').showPanelUpdateUserFood(record.get('code_userfood'));
	}
});