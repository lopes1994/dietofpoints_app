Ext.define("Dietofpoints.view.PanelActivityLevel_user", {

	extend: 'Ext.Panel',
	
	xtype: 'panelActivityLevel_user',

	config: {

        scrollable: 'vertical',

        layout:{
            type  : 'vbox'
        },

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) { 
                    var str = '';
                    str += '<label><div class="name_sedentarylevel"><input name="activitylevel" class="radio" type="radio" value=' + values.code1 + '>' + values.name1 + '</div></label>';
                    str += '<label><div class="name_sedentarylevel"><input name="activitylevel" class="radio" type="radio" value=' + values.code2 + '>' + values.name2 + '</div></label>';
                    str += '<label><div class="name_sedentarylevel"><input name="activitylevel" class="radio" type="radio" value=' + values.code3 + '>' + values.name3 + '</div></label>';
                    str += '<label><div class="name_sedentarylevel"><input name="activitylevel" class="radio" type="radio" value=' + values.code4 + '>' + values.name4 + '</div></label>';
                    str += '<label><div class="name_sedentarylevel"><input name="activitylevel" class="radio" type="radio" value=' + values.code5 + '>' + values.name5 + '</div></label>';
                    str += '<label><div class="name_sedentarylevel"><input name="activitylevel" class="radio" type="radio" value=' + values.code6 + '>' + values.name6 + '</div></label>';
                    return str;
                }
            }
        )
	}
});