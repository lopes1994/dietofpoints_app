Ext.define("Dietofpoints.view.ListFriendRequest", {

	extend: 'Ext.List',
	
	xtype: 'listFriendRequest',

	config: {

		scrollable: 'vertical',
		store: 'ListFriendRequest',
		flex: 1,
		emptyText: L('emptyText'),

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            str += '<div class="name_listFriend">' + values.name_user + '</div>';
		            str += '<div class="icons_listFriendRequest">';
                    str += '<i class="icon_listFriendRequest_delete icon_listFriendRequest fa fa-times fa-fw"></i>';
                    str += '<i class="icon_listFriendRequest_add fa fa-check fa-fw"></i>';
                    str += '</div>';
                    str += '<div class="code">' + values.code_user + '</div>';
					return str;
				}
			}
		)
	},

	initialize: function() {

		var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.deleteFriendRequest,
            delegate: ['.fa-times'],
            scope: me
        });

        listElement.on({
            tap: me.acceptFriendRequest,
            delegate: ['.fa-check'],
            scope: me
        });
	},

	deleteFriendRequest: function(event, element) {
		var me = this;
        var record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord();
		Dietofpoints.app.getController('Diary').deleteFriendRequest(record.get('code_user'));
	},

	acceptFriendRequest: function(event, element) {
		var me = this;
        var record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord();
		Dietofpoints.app.getController('Diary').acceptFriendRequest(record.get('code_user'));
	}
});