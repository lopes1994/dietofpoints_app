Ext.define("Dietofpoints.view.PanelAddFriend", {

	extend: 'Ext.Panel',
	
	xtype: 'panelAddFriend',

	config: {

        layout: 'vbox',
        scrollable: 'vertical',

        items: [
            {
                xtype: 'emailfield',
                itemId: 'email',
                placeHolder: L('panel.template.panelAddFriend.email'),
                cls: 'customInput',
                listeners: [
                    {
                        fn: function(element, eOpts) {
                            element.down('input').dom.focus();
                        },
                        event: 'painted'
                    }
                ]
            }
        ]
	}
});