Ext.define("Dietofpoints.view.PanelOptionsSnacks", {

	extend: 'Ext.Panel',
	
	xtype: 'panelOptionsSnacks',

	config: {
	
		cls: 'panelOptions_diary',
        height: null,

        items: [
            {
                xtype: 'button',
                itemId: 'addFood_period4',
                text: L('panel.template.panelOptionsDiary.addFood'),
                cls: 'addItem_panelOptions_diary',
                iconCls: 'fa fa-plus fa-fw'
            },
            {
                xtype: 'button',
                itemId: 'moreOptions_period4',
                text: L('panel.template.panelOptionsDiary.more'),
                cls: 'moreOptions_panelOptions_diary',
                iconCls: 'fa fa-ellipsis-h fa-fw'
            }
        ]
	}
});