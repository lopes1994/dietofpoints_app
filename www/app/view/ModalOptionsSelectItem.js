Ext.define("Dietofpoints.view.ModalOptionsSelectItem", {

	extend: 'Ext.Panel',
	
	xtype: 'modalOptionsSelectItem',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'button',
                cls: 'buttonModal',
                text: L('modal.template.modalOptionsSelectItem.option1'),
                itemId: 'option1'
            },
            {
                xtype: 'button',
                cls: 'buttonModal',
                text: L('modal.template.modalOptionsSelectItem.option2'),
                itemId: 'option2'
            },
            {
                xtype: 'button',
                cls: 'buttonModal',
                text: L('modal.template.modalOptionsSelectItem.option3'),
                itemId: 'option3'
            }
        ]
    }
});