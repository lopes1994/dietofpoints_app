Ext.define("Dietofpoints.view.ListMyMeals_tabpanelMyFoodAndMeals", {

	extend: 'Ext.List',
	
	xtype: 'listMyMeals_tabpanelMyFoodAndMeals',

	config: {

		title: L('list.template.listMyMeals_tabpanelMyFoodAndMeals.title'),
		scrollable: 'vertical',
		store: 'ListMyMeals_tabpanelMyFoodAndMeals',
		flex: 1,
		emptyText: L('emptyText'),

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            str += '<div class="titleWithoutCheckbox"><div class="name_food_withoutCheckbox">' + values.name_meal + '</div></div>';
		            str += '<div class="titleWithCheckbox"><label><input class="checkbox" type="checkbox" value="' + values.code_meal + '"><div class="name_food_withCheckbox">' + values.name_meal + '</div></label></div>';
		            str += '<div class="code">' + values.code_meal + '</div>';
					return str;
				}
			}
		)
	},

	initialize: function() {

		var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showPanelAddMeal,
            delegate: ['.titleWithoutCheckbox'],
            scope: me
        });
	},

	showPanelAddMeal: function(event, element) {
		var me = this;
        var record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord();
		Dietofpoints.app.getController('Diary').showPanelAddMeal(record.get('code_meal'));
	}
});