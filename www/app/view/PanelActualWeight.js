Ext.define("Dietofpoints.view.PanelActualWeight", {

	extend: 'Ext.Panel',
	
	xtype: 'panelActualWeight',

	config: {

        cls: 'panelProfile',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="panelProfile_textLeft">' + L('panel.template.panelActualWeight.title') + '</div>';
                    str += '<div class="panelProfile_textRight">' + values.text + ' kg' + '</div>';
                    return str;
                }
            }
        )
	},

    initialize: function() {

        var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showModalActualWeight,
            delegate: ['.panelProfile_textRight'],
            scope: me
        });
    },

    showModalActualWeight: function() {
        Dietofpoints.app.getController('Diary').showModalActualWeight();
    }
});