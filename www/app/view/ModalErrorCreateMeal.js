Ext.define("Dietofpoints.view.ModalErrorCreateMeal", {

	extend: 'Ext.Panel',
	
	xtype: 'modalErrorCreateMeal',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalErrorCreateMeal.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalErrorCreateMeal.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalErrorCreateMeal.close'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'close'
                    }
                ]
            }
        ]
    }
});