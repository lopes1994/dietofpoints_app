Ext.define('Dietofpoints.model.ListLunch', {

    extend: "Ext.data.Model",
	
    config: {
        fields: [
            {name: "name_food", type: "string"},
            {name: "amount_food",  type: "string"},
            {name: "points_food", type: "int"},
            {name: "code_feedhistory", type: "int"},
            {name: "unit_food", type: "int"},
            {name: "repeat_portion", type: "int"}
        ],
		
		remoteFilter: true,
 
		proxy: {
			type: 'ajax',
			reader: {
				type: 'json',
				rootProperty: 'root',
				successProperty: 'success',
				totalProperty: 'totalCount'
			}
		},
		
		autoLoad: true
    }
});