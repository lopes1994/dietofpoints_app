Ext.define("Dietofpoints.view.ListLunch", {

	extend: 'Ext.List',
	
	xtype: 'listLunch',

	config: {
	
		store: 'ListLunch',
		scrollable: false,

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            str += '<div class="titleWithCheckbox"><label><input class="checkbox" type="checkbox" value="' + values.code_feedhistory + '"><div class="name_food_withCheckbox">' + values.name_food + '</div></label></div>';
					str += '<div class="titleWithoutCheckbox"><div class="name_food_withoutCheckbox">' + values.name_food + '</div></div>';
					str += '<div class="points_food">' + values.points_food + '</div>';
					if(values.unit_food != 0) {
						str += '<div class="textWithCheckbox"><div class="amount_food_withCheckbox">' + values.unit_food + ' ' + values.amount_food + '</div></div>';
						str += '<div class="textWithoutCheckbox"><div class="amount_food_withoutCheckbox">' + values.unit_food + ' ' + values.amount_food + '</div></div>';
					}
					str += '<div class="code">' + values.repeat_portion + '</div>';
					return str;
				}
			}
		)
	},

	initialize: function() {

		var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showPanelUpdatePortion,
            delegate: ['.titleWithoutCheckbox .name_food_withoutCheckbox'],
            scope: me
        });

        listElement.on({
            tap: me.showPanelUpdatePortion,
            delegate: ['.textWithoutCheckbox .amount_food_withoutCheckbox'],
            scope: me
        });
	},

	showPanelUpdatePortion: function(event, element) {
		var me = this;
        var record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord();
		Dietofpoints.app.getController('Diary').showPanelUpdatePortion(record.get('code_feedhistory'));
	}
});