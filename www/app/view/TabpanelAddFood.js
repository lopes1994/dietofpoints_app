Ext.define('Dietofpoints.view.TabpanelAddFood', {

	extend: 'Ext.TabPanel',
	
	xtype: 'tabpanelAddFood',

	config: {

		layout: {
			type: 'card',
			animation: null
		}, 

		tabBar: {
            docked: 'top', 
			pack: 'center',
			align: 'center'
        },

		items: [
			{
				xtype: 'panelAllFood'
			},
			{
				xtype: 'panelMyFood'
			},
			{
				xtype: 'panelMyMeals'
			},
            {
                xtype: 'panelBottomTabPanelAddFood'
            }
		]
	}
});