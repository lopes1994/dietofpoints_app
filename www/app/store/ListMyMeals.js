Ext.define("Dietofpoints.store.ListMyMeals", {

	extend: 'Ext.data.Store',

	config: {
		model: 'Dietofpoints.model.ListMyMeals'
	}
});