Ext.define("Dietofpoints.view.ModalCopyMeal", {

	extend: 'Ext.Panel',
	
	xtype: 'modalCopyMeal',

	config: {
	
        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,

        items: [
            {
                xtype: 'panel',
                cls: 'title_modalCopyMeal',
                html: L('modal.template.modalCopyMeal.title')
            },
            {
                xtype: 'button',
                cls: 'option_modalCopyMeal',
                text: null,
                itemId: 'option1'
            },
            {
                xtype: 'button',
                cls: 'option_modalCopyMeal',
                text: null,
                itemId: 'option2'
            },
            {
                xtype: 'button',
                cls: 'option_modalCopyMeal',
                text: null,
                itemId: 'option3'
            },
            {
                xtype: 'button',
                cls: 'option_modalCopyMeal',
                text: null,
                itemId: 'option4'
            },
            {
                xtype: 'button',
                cls: 'option_modalCopyMeal',
                text: null,
                itemId: 'option5'
            }
        ] 
	}
});