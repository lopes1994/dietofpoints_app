Ext.define("Dietofpoints.view.PanelYou", {

	extend: 'Ext.Panel',
	
	xtype: 'panelYou',

	config: {

        layout: 'vbox',
        scrollable: 'vertical',

        items: [
            {
                xtype: 'panel',
                docked: 'top',
                cls: 'panelNameGender',
                html: L('panel.template.panelYou.nameGender')
            },
            {
                xtype: 'panelGender_you'
            },
            {
                xtype: 'textfield',
                itemId: 'name',
                placeHolder: L('panel.template.panelYou.name'),
                cls: 'customInput'
            },
            {
                xtype: 'numberfield',
                itemId: 'age',
                placeHolder: L('panel.template.panelYou.age'),
                cls: 'customInput',
                maxLength: 3
            },
            {
                xtype: 'numberfield',
                itemId: 'height',
                placeHolder: L('panel.template.panelYou.height'),
                cls: 'customInput',
                maxLength: 4
            },
            {
                xtype: 'numberfield',
                itemId: 'weight',
                placeHolder: L('panel.template.panelYou.weight'),
                cls: 'customInput',
                maxLength: 5
            },
            {
                xtype: 'numberfield',
                itemId: 'goalWeight',
                placeHolder: L('panel.template.panelYou.goalWeight'),
                cls: 'customInput',
                maxLength: 5
            }
        ]
	}
});