Ext.define("Dietofpoints.view.PanelCreateFood", {

	extend: 'Ext.Panel',
	
	xtype: 'panelCreateFood',

	config: {

        layout: 'vbox',
        scrollable: 'vertical',

        items: [
            {
                xtype: 'textfield',
                placeHolder: L('panel.template.panelCreateFood.name'),
                itemId: 'name',
                cls: 'customInput',
                listeners: [
                    {
                        fn: function(element, eOpts) {
                            element.down('input').dom.focus();
                        },
                        event: 'painted'
                    }
                ]
            },
            {
                xtype: 'numberfield',
                placeHolder: L('panel.template.panelCreateFood.unit'),
                itemId: 'unit',
                cls: 'customInput',
                maxLength: 2
            },
            {
                xtype: 'textfield',
                placeHolder: L('panel.template.panelCreateFood.amount'),
                itemId: 'amount',
                cls: 'customInput'
            },
            {
                xtype: 'numberfield',
                placeHolder: L('panel.template.panelCreateFood.points'),
                itemId: 'points',
                cls: 'customInput',
                maxLength: 3
            }
        ]
	}
});