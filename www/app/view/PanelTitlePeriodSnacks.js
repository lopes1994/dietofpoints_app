Ext.define("Dietofpoints.view.PanelTitlePeriodSnacks", {

	extend: 'Ext.Panel',

	xtype: 'panelTitlePeriodSnacks',

	config: {

		cls: 'panelTitlePeriod',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="titlePeriod_diary">' + L('panel.template.panelTitlePeriodSnacks') + '</div>';
                    str += '<div class="amountPeriod_diary">' + values.amount + '</div>';
                    return str;
                }
            }
        )
	}
});
