Ext.define("Dietofpoints.view.PanelAbout", {

	extend: 'Ext.Panel',
	
	xtype: 'panelAbout',

	config: {

        layout:{
            type  : 'vbox'
        },
        scrollable: 'vertical',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) { 
                    var str = '';
                    str += '<div class="textAbout">' + values.text + '</div>';
                    return str;
                }
            }
        )
	}
});