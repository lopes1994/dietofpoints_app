Ext.define('Dietofpoints.controller.Diary', {

	extend: 'Ext.app.Controller',

	config: { 

		refs: {  
			diary: 'diary',
			navigationview: 'navigationview',
			datepicker_diary: 'datepicker_diary',
			paneldatadiary: 'paneldatadiary',
			panelTitlePeriodBreakfast: 'panelTitlePeriodBreakfast',
			panelTitlePeriodLunch: 'panelTitlePeriodLunch',
			panelTitlePeriodDinner: 'panelTitlePeriodDinner',
			panelTitlePeriodSnacks: 'panelTitlePeriodSnacks',
			listBreakfast: 'listBreakfast',
			listLunch: 'listLunch',
			listDinner: 'listDinner',
			listSnacks: 'listSnacks',
			panelOptionsBreakfast: 'panelOptionsBreakfast',
			panelOptionsLunch: 'panelOptionsLunch',
			panelOptionsDinner: 'panelOptionsDinner',
			panelOptionsSnacks: 'panelOptionsSnacks',
			panelDatepicker: 'panelDatepicker',
			modalQuicklyAdd: 'modalQuicklyAdd',
			modalErrorQuicklyAdd: 'modalErrorQuicklyAdd',
			modalWithSingleOption: 'modalWithSingleOption',
			modalWithMultipleOptions: 'modalWithMultipleOptions',
			modalCopyMeal: 'modalCopyMeal',
			panelSaveMeal: 'panelSaveMeal',
			modalErrorWithoutNameCreateMeal: 'modalErrorWithoutNameCreateMeal',
			modalErrorCreateMeal: 'modalErrorCreateMeal',
			modalSuccessCreateMeal: 'modalSuccessCreateMeal',
			modalOptionsSelectItem: 'modalOptionsSelectItem',
			modalErrorDeleteItems: 'modalErrorDeleteItems',
			modalConfirmDeleteItems: 'modalConfirmDeleteItems',
			modalCopyMealMultiplePeriods: 'modalCopyMealMultiplePeriods',
			panelSaveMealMultiplePeriods: 'panelSaveMealMultiplePeriods',
			panelAllFood: 'panelAllFood',
			panelMyFood: 'panelMyFood',
			panelMyMeals: 'panelMyMeals',
			tabpanelAddFood: 'tabpanelAddFood',
			listAllFood: 'listAllFood',
			listMyFood: 'listMyFood',
			listMyMeals: 'listMyMeals',
			modalQuicklyAddCalledFromTabpanelAddFood: 'modalQuicklyAddCalledFromTabpanelAddFood',
			panelCreateFood: 'panelCreateFood',
			modalCheckFilledFields: 'modalCheckFilledFields',
			modalErrorCreateFood: 'modalErrorCreateFood',
			modalErrorQuicklyAddCalledFromTabpanelAddFood: 'modalErrorQuicklyAddCalledFromTabpanelAddFood',
			panelUpdatePortion: 'panelUpdatePortion',
			panelUpdateCalories: 'panelUpdateCalories',
			menu: 'menu',
			panelMenu: 'panelMenu',
			listWeight: 'listWeight',
			modalAddWeight: 'modalAddWeight',
			modalErrorAddWeight: 'modalErrorAddWeight',
			panelListWeight: 'panelListWeight',
			panelTopListWeight: 'panelTopListWeight',
			panelInitialWeight: 'panelInitialWeight',
			panelActualWeight: 'panelActualWeight',
			panelGoalWeight: 'panelGoalWeight',
			panelHeight: 'panelHeight',
			panelAge: 'panelAge',
			panelProfile: 'panelProfile',
			modalActualWeight: 'modalActualWeight',
			modalGoalWeight: 'modalGoalWeight',
			modalHeight: 'modalHeight',
			modalAge: 'modalAge',
			modalErrorActualWeight: 'modalErrorActualWeight',
			modalErrorGoalWeight: 'modalErrorGoalWeight',
			modalErrorHeight: 'modalErrorHeight',
			modalErrorAge: 'modalErrorAge',
			panelActivityLevel_user: 'panelActivityLevel_user',
			panelActivityLevel: 'panelActivityLevel',
			panelTitleWater: 'panelTitleWater',
			modalWater: 'modalWater',
			modalErrorWater: 'modalErrorWater',
			tabpanelMyFoodAndMeals: 'tabpanelMyFoodAndMeals',
			listMyFood_tabpanelMyFoodAndMeals: 'listMyFood_tabpanelMyFoodAndMeals',
			listMyMeals_tabpanelMyFoodAndMeals: 'listMyMeals_tabpanelMyFoodAndMeals',
			panelUpdateUserFood: 'panelUpdateUserFood',
			modalConfirmDeleteItemsTabpanelMyFoodAndMeals: 'modalConfirmDeleteItemsTabpanelMyFoodAndMeals',
			panelAddMeal: 'panelAddMeal',
			listAddMeal: 'listAddMeal',
			modalAddMeal: 'modalAddMeal',
			main: 'main',
			panelIdiom: 'panelIdiom',
			panelGender_you: 'panelGender_you',
			panelYou: 'panelYou',
			panelAccessData: 'panelAccessData',
			login: 'login',
			modalErrorLogin: 'modalErrorLogin',
			modalErrorEmailRegister: 'modalErrorEmailRegister',
			modalConfirmLogout: 'modalConfirmLogout',
			modalChangeLanguage: 'modalChangeLanguage',
			panelAbout: 'panelAbout',
			listFriend: 'listFriend',
			modalBadRate: 'modalBadRate',
			modalErrorChooseRate: 'modalErrorChooseRate',
			rateApp: 'rateApp',
			modalDeleteFriend: 'modalDeleteFriend',
			panelAddFriend: 'panelAddFriend',
			modalErrorSendFriendRequest: 'modalErrorSendFriendRequest',
			modalSuccessSendFriendRequest: 'modalSuccessSendFriendRequest',
			modalAlreadyFriends: 'modalAlreadyFriends',
			modalOpenFriendRequestSent: 'modalOpenFriendRequestSent',
			listFriendRequest: 'listFriendRequest',
			modalOpenFriendRequestReceived: 'modalOpenFriendRequestReceived',
			panelBottomTabPanelAddFood: 'panelBottomTabPanelAddFood'
		},
		
		control: {
			navigationview: {
				initialize: 'starts_navigationview',
				back: 'backButton_navigationview'
			},
			'navigationview #edit_navigationview': {
				tap: 'editItems'
			},
			'navigationview #addMeal_navigationview': {
				tap: 'showModalAddMeal'
			},
			'navigationview #registerWeight_navigationview': {
				tap: 'showModalAddWeight'
			},
			'navigationview #saveIdiom_navigationview': {
				tap: 'saveIdiom'
			},
			'navigationview #savePanelAccessData_navigationview': {
				tap: 'savePanelAccessData'
			},
			'navigationview #editTabpanelMyFoodAndMeals_navigationview': {
				tap: 'editTabpanelMyFoodAndMeals'
			},
			'navigationview #trashTabpanelMyFoodAndMeals_navigationview': {
				tap: 'confirmDeleteItemsTabpanelMyFoodAndMeals'
			},
			'navigationview #addTabpanelMyFoodAndMeals_navigationview': {
				tap: 'showPanelCreateFoodFromTabpanelMyFoodAndMeals'
			},
			'navigationview #saveCreateFoodFromTabpanelMyFoodAndMeals_navigationview': {
				tap: 'createFoodFromTabpanelMyFoodAndMeals'
			},
			'navigationview #updateUserFood_navigationview': {
				tap: 'updateUserFood'
			},
			'navigationview #closeTabpanelMyFoodAndMeals_navigationview': {
				tap: 'closeTabpanelMyFoodAndMeals'
			},
			'navigationview #addFriend_navigationview': {
				tap: 'showPanelAddFriend'
			},
			'navigationview #sendFriendRequest_navigationview': {
				tap: 'sendFriendRequest'
			},
			'navigationview #saveActivityLevel_navigationview': {
				tap: 'activityLevel_user'
			},
			'navigationview #saveActivityLevelRegister_navigationview': {
				tap: 'activityLevelRegister'
			},
			'navigationview #close_navigationview': {
				tap: 'closeEditItems'
			},
			'navigationview #menu_navigationview': {
				tap: 'showMenu'
			},
			'navigationview #trash_navigationview': {
				tap: 'confirmDeleteItems'
			},
			'navigationview #options_navigationview': {
				tap: 'showModalOptionsSelectItem'
			},
			'navigationview #add_navigationview': {
				tap: 'add_food'
			},
			'navigationview #savePanelYou_navigationview': {
				tap: 'savePanelYou'
			},
			'navigationview #saveCreateFood_navigationview': {
				tap: 'createFood'
			},
			'navigationview #saveUpdatePortion_navigationview': {
				tap: 'updatePortion'
			},
			'navigationview #saveUpdateCalories_navigationview': {
				tap: 'updateCalories'
			},
			diary: {
				initialize: 'starts_diary'
			},
			panelSaveMeal: {
				initialize: 'starts_panelSaveMeal'
			},
			'panelSaveMeal #save': {
				tap: 'saveMeal'
			},
			'panelSaveMealMultiplePeriods #save': {
				tap: 'saveMealMultiplePeriods'
			},
			'panelDatepicker #buttonBack': {
				tap: 'decreaseDate'
			},
			'panelDatepicker #buttonGo': {
				tap: 'increaseDate'
			},
			'modalQuicklyAdd #add': {
				tap: 'quicklyAdd'
			},
			'modalQuicklyAdd #cancel': {
				tap: 'destroyModalQuicklyAdd'
			},
			'modalAddWeight #cancel': {
				tap: 'destroyModalAddWeight'
			},
			'modalAddWeight #add': {
				tap: 'addWeight'
			},
			'modalQuicklyAddCalledFromTabpanelAddFood #add': {
				tap: 'quicklyAddCalledFromTabpanelAddFood'
			},
			'modalQuicklyAddCalledFromTabpanelAddFood #cancel': {
				tap: 'destroyModalQuicklyAddCalledFromTabpanelAddFood'
			},
			'modalErrorQuicklyAdd #close': {
				tap: 'showModalQuicklyAdd'
			},
			'modalErrorAddWeight #close': {
				tap: 'showModalAddWeight'
			},
			'modalErrorQuicklyAddCalledFromTabpanelAddFood #close': {
				tap: 'showModalQuicklyAddCalledFromTabpanelAddFood'
			},
			'modalErrorCreateFood #close': {
				tap: 'destroyModalErrorCreateFood'
			},
			'modalWithSingleOption #option1': {
				tap: 'showModalQuicklyAdd'
			},
			'modalWithMultipleOptions #option1': {
				tap: 'showPanelSaveMeal'
			},
			'modalWithMultipleOptions #option2': {
				tap: 'showModalCopyMeal'
			},
			'modalWithMultipleOptions #option3': {
				tap: 'showModalQuicklyAdd'
			},
			'modalErrorWithoutNameCreateMeal #close': {
				tap: 'destroyModalErrorWithoutNameCreateMeal'
			},
			'modalErrorCreateMeal #close': {
				tap: 'destroyModalErrorCreateMeal'
			},
			'modalSuccessCreateMeal #close': {
				tap: 'destroyModalSuccessCreateMeal'
			},
			'modalCheckFilledFields #close': {
				tap: 'destroyModalCheckFilledFields'
			},
			'modalOptionsSelectItem #option3': {
				tap: 'selectAllItems'
			},
			'modalOptionsSelectItem #option1': {
				tap: 'showModalCopyMealMultiplePeriods'
			},
			'modalOptionsSelectItem #option2': {
				tap: 'showPanelSaveMealMultiplePeriods'
			},
			'modalErrorDeleteItems #close': {
				tap: 'destroyModalErrorDeleteItems'
			},
			'modalConfirmDeleteItems #cancel': {
				tap: 'destroyModalConfirmDeleteItems'
			},
			'modalConfirmDeleteItems #delete': {
				tap: 'deleteItems'
			},
			'modalConfirmDeleteItemsTabpanelMyFoodAndMeals #cancel': {
				tap: 'destroyModalConfirmDeleteItemsTabpanelMyFoodAndMeals'
			},
			'modalConfirmDeleteItemsTabpanelMyFoodAndMeals #delete': {
				tap: 'deleteItemsTabpanelMyFoodAndMeals'
			},
			'panelAllFood #searchfield': {
				clearicontap: 'cleanStoreListAllFood'
			},
			'panelAllFood #search': {
				tap: 'searchFood_panelAllFood'
			},
			'panelBottomTabPanelAddFood #createFood': {
				tap: 'showPanelCreateFood'
			},
			'panelBottomTabPanelAddFood #quickAdd': {
				tap: 'showModalQuicklyAddCalledFromTabpanelAddFood'
			},
			'panelMyFood #searchfield': {
				clearicontap: 'cleanStoreListMyFood'
			},
			'panelMyFood #search': {
				tap: 'searchFood_panelMyFood'
			},
			'panelMyMeals #searchfield': {
				clearicontap: 'cleanStoreListMyMeals'
			},
			'panelMyMeals #search': {
				tap: 'searchFood_panelMyMeals'
			},
			tabpanelAddFood: {
				hide: 'cleanStores'
			},
			panelAllFood: {
				hide: 'unckeckCheckboxes'
			},
			panelMyFood: {
				hide: 'unckeckCheckboxes'
			},
			panelMyMeals: {
				hide: 'unckeckCheckboxes'
			},
			'menu #diary': {
				tap: 'showPanelDiary'
			},
			'menu #myWeight': {
				tap: 'showPanelListWeight'
			},
			'menu #profile': {
				tap: 'showPanelProfile'
			},
			'menu #friendRequest': {
				tap: 'showListFriendRequest'
			},
			'menu #help': {
				tap: 'openHelpdesk'
			},
			'menu #myFoodAndMeals': {
				tap: 'showTabPanelMyFoodAndMeals'
			},
			'menu #logout': {
				tap: 'confirmLogout'
			},
			'menu #about': {
				tap: 'showPanelAbout'
			},
			'menu #friend': {
				tap: 'showListFriend'
			},
			'menu #reminders': {
				tap: 'showPanelReminders'
			},
			'menu #share': {
				tap: 'share'
			},
			listWeight: {
				initialize: 'starts_listWeight'
			},
			panelTopListWeight: {
				initialize: 'set_panelTopListWeight'
			},
			panelListWeight: {
				initialize: 'starts_panelListWeight'
			},
			panelProfile: {
				initialize: 'starts_panelProfile'
			},
			'modalActualWeight #cancel': {
				tap: 'destroyModalActualWeight'
			},
			'modalActualWeight #add': {
				tap: 'actualWeight_user'
			},
			'modalErrorActualWeight #close': {
				tap: 'showModalActualWeight'
			},
			'modalGoalWeight #cancel': {
				tap: 'destroyModalGoalWeight'
			},
			'modalGoalWeight #add': {
				tap: 'goalWeight_user'
			},
			'modalErrorGoalWeight #close': {
				tap: 'showModalGoalWeight'
			},
			'modalHeight #cancel': {
				tap: 'destroyModalHeight'
			},
			'modalHeight #add': {
				tap: 'height_user'
			},
			'modalErrorHeight #close': {
				tap: 'showModalHeight'
			},
			'modalAge #cancel': {
				tap: 'destroyModalAge'
			},
			'modalAge #add': {
				tap: 'age_user'
			},
			'modalErrorAge #close': {
				tap: 'showModalAge'
			},
			panelActivityLevel_user: {
				initialize: 'set_modalActivityLevel',
				hide: 'cleanRadioButtons'
			},
			'modalWater #cancel': {
				tap: 'destroyModalWater'
			},
			'modalWater #add': {
				tap: 'addWater'
			},
			'modalErrorWater #close': {
				tap: 'showModalWater'
			},
			listMyFood_tabpanelMyFoodAndMeals: {
				initialize: 'starts_listMyFood_tabpanelMyFoodAndMeals',
				show: 'show_listMyFood_tabpanelMyFoodAndMeals'
			},
			listMyMeals_tabpanelMyFoodAndMeals: {
				show: 'show_listMyMeals_tabpanelMyFoodAndMeals'
			},
			tabpanelMyFoodAndMeals: {
				activeitemchange: 'itemChange_tabPanelMyFoodAndMeals',
				initialize: 'starts_tabpanelMyFoodAndMeals'
			},
			listAddMeal: {
				initialize: 'load_listAddMeal'
			},
			modalAddMeal: {
				initialize: 'starts_modalAddMeal'
			},
			datepicker_diary: {
				change : 'newDateSelected'
			},
			'main #register': {
				tap: 'register'
			},
			'main #login': {
				tap: 'showPanelLogin'
			},
			panelIdiom: {
				initialize: 'starts_panelIdiom',
				hide: 'cleanRadioButtons'
			},
			panelGender_you: {
				initialize: 'starts_panelGender_you'
			},
			panelYou: {
				hide: 'cleanRadioButtons'
			},
			'login #login': {
				tap: 'login'
			},
			'modalErrorLogin #close': {
				tap: 'destroyModalErrorLogin'
			},
			'modalErrorEmailRegister #close': {
				tap: 'destroyModalErrorEmailRegister'
			},
			'modalConfirmLogout #cancel': {
				tap: 'destroyModalConfirmLogout'
			},
			'modalConfirmLogout #yes': {
				tap: 'logout'
			},
			'modalChangeLanguage #ok': {
				tap: 'destroyModalChangeLanguage'
			},
			panelAbout: {
				initialize: 'starts_panelAbout'
			},
			listFriend: {
				initialize: 'starts_listFriend'
			},
			'modalBadRate #yes': {
				tap: 'showHelpdesk'
			},
			'modalBadRate #no': {
				tap: 'destroyModalBadRate'
			},
			'modalErrorChooseRate #close': {
				tap: 'destroyModalErrorChooseRate'
			},
			'modalDeleteFriend #cancel': {
				tap: 'destroyModalDeleteFriend'
			},
			'modalDeleteFriend #delete': {
				tap: 'deleteFriend'
			},
			'modalErrorSendFriendRequest #no': {
				tap: 'destroyModalErrorSendFriendRequest'
			},
			'modalErrorSendFriendRequest #yes': {
				tap: 'sendMessageErrorSendRequest'
			},
			'modalSuccessSendFriendRequest #close': {
				tap: 'destroyModalSuccessSendFriendRequest'
			},
			'modalAlreadyFriends #close': {
				tap: 'destroyModalAlreadyFriends'
			},
			'modalOpenFriendRequestSent #close': {
				tap: 'destroyModalOpenFriendRequestSent'
			},
			menu: {
				initialize: 'starts_menu'
			},
			listFriendRequest: {
				initialize: 'starts_listFriendRequest'
			},
			'modalOpenFriendRequestReceived #yes': {
				tap: 'showListFriendRequestFromModalOpenFriendRequestReceived'
			},
			'modalOpenFriendRequestReceived #no': {
				tap: 'destroyModalOpenFriendRequestReceived'
			},
			'modalCopyMeal #option1': {
				tap: 'clickModalCopyMeal'
			},
			'modalCopyMeal #option2': {
				tap: 'clickModalCopyMeal'
			},
			'modalCopyMeal #option3': {
				tap: 'clickModalCopyMeal'
			},
			'modalCopyMeal #option4': {
				tap: 'clickModalCopyMeal'
			},
			'modalCopyMeal #option5': {
				tap: 'clickModalCopyMeal'
			},
			'modalCopyMealMultiplePeriods #option1': {
				tap: 'clickModalCopyMealMultiplePeriods'
			},
			'modalCopyMealMultiplePeriods #option2': {
				tap: 'clickModalCopyMealMultiplePeriods'
			},
			'modalCopyMealMultiplePeriods #option3': {
				tap: 'clickModalCopyMealMultiplePeriods'
			},
			'modalCopyMealMultiplePeriods #option4': {
				tap: 'clickModalCopyMealMultiplePeriods'
			},
			'modalCopyMealMultiplePeriods #option5': {
				tap: 'clickModalCopyMealMultiplePeriods'
			},
			'panelOptionsBreakfast #addFood_period1': {
				tap: 'clickAddFood'
			},
			'panelOptionsBreakfast #moreOptions_period1': {
				tap: 'clickMoreOptions'
			},
			'panelOptionsLunch #addFood_period2': {
				tap: 'clickAddFood'
			},
			'panelOptionsLunch #moreOptions_period2': {
				tap: 'clickMoreOptions'
			},
			'panelOptionsDinner #addFood_period3': {
				tap: 'clickAddFood'
			},
			'panelOptionsDinner #moreOptions_period3': {
				tap: 'clickMoreOptions'
			},
			'panelOptionsSnacks #addFood_period4': {
				tap: 'clickAddFood'
			},
			'panelOptionsSnacks #moreOptions_period4': {
				tap: 'clickMoreOptions'
			}
		}
	},

	period: '',
	code_meal: 0,
	activityLevel: '',
	gender: 0,
	age: 0,
	height: 0,
	weight: '',
	goalWeight: '',
	email: '',
	password: '',
	name: '',
	friend: 0,

	clickAddFood: function(button) {
		var me = this;
		var button = button.getItemId();
		switch(button) {
			case ('addFood_period1'):
				Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: add food (Breakfast)');
	    		me.showPanelAddFood(1);
		    break;
	    	case ('addFood_period2'):
	    		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: add food (Lunch)');
	        	me.showPanelAddFood(2);
	        break;
	    	case ('addFood_period3'):
	    		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: add food (Dinner)');
	        	me.showPanelAddFood(3);
	        break;
	        case ('addFood_period4'):
	        	Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: add food (Snacks)');
	        	me.showPanelAddFood(4);
		}
	},

	clickMoreOptions: function(button) {
		var me = this;
		var button = button.getItemId();
		switch(button) {
			case ('moreOptions_period1'):
				Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: more (Breakfast)');
	    		me.showModalOption_panelOptions_diary(1);
		    break;
	    	case ('moreOptions_period2'):
	    		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: more (Lunch)');
	        	me.showModalOption_panelOptions_diary(2);
	        break;
	    	case ('moreOptions_period3'):
	    		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: more (Dinner)');
	        	me.showModalOption_panelOptions_diary(3);
	        break;
	        case ('moreOptions_period4'):
	        	Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: more (Snacks)');
	        	me.showModalOption_panelOptions_diary(4);
		}
	},

	clickModalCopyMeal: function(button) {
		var me = this;
		var button = button.getItemId();
		switch(button) {
			case ('option1'):
	    		me.copyMeal(1);
		    break;
	    	case ('option2'):
	        	me.copyMeal(2);
	        break;
	    	case ('option3'):
	        	me.copyMeal(3);
	        break;
	        case ('option4'):
	        	me.copyMeal(4);
	        break;
	        case ('option5'):
	        	me.copyMeal(5);
		}
	},

	clickModalCopyMealMultiplePeriods: function(button) {
		var me = this;
		var button = button.getItemId();
		switch(button) {
			case ('option1'):
	    		me.copyMealMultiplePeriods(1);
		    break;
	    	case ('option2'):
	        	me.copyMealMultiplePeriods(2);
	        break;
	    	case ('option3'):
	        	me.copyMealMultiplePeriods(3);
	        break;
	        case ('option4'):
	        	me.copyMealMultiplePeriods(4);
	        break;
	        case ('option5'):
	        	me.copyMealMultiplePeriods(5);
		}
	},

	showListFriendRequestFromModalOpenFriendRequestReceived: function() {
		var me = this;
		me.destroyModalOpenFriendRequestReceived();
		me.showListFriendRequest();
	},

	loadStoreListFriendRequest: function() {
		var me = this;
		var store = Ext.getStore('ListFriendRequest');
		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user')}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listFriendRequest');
		store.removeAll();
		store.load(function() {
			var count = Ext.getStore('ListFriendRequest').getCount();
			if(count > 0) {
				me.getMenu().down('#friendRequest').setBadgeText(count);
			}
			else {
				me.getMenu().down('#friendRequest').destroy();
			}
		}, this);
	},

	deleteFriendRequest: function(user) {
		var me = this;
		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'deleteFriendRequest',
			async: false,
			params: {
				friend: user,
				user: localStorage.getItem('dietofpoints_code_user')
			},
			success: function (conn, response, options, eOpts) {
				me.loadStoreListFriendRequest();
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});
	},

	acceptFriendRequest: function(user) {
		var me = this;
		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'acceptFriendRequest',
			async: false,
			params: {
				friend: user,
				user: localStorage.getItem('dietofpoints_code_user')
			},
			success: function (conn, response, options, eOpts) {
				me.loadStoreListFriendRequest();
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});
	},

	starts_listFriendRequest: function() {
		var me = this;
		me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		me.updateMenuButtonColor('#friendRequest');
	},

	showListFriendRequest: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: menu', 'bt: friendRequest');
		var currentXtype = me.getXtypeByView(me.getNavigationview().getActiveItem());
        if (currentXtype != 'listFriendRequest') {
			me.destroyComponents();
			var component = Ext.widget('listFriendRequest',{
				title: L('list.template.listFriendRequest.title')
			});
			me.getNavigationview().push(component);
		}
		me.hideMenu();
	},

	starts_menu: function() {
		var me = this;
		var store = Ext.getStore('ListFriendRequest');
		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user')}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listFriendRequest');
		store.removeAll();
		store.load(function() {
			var count = Ext.getStore('ListFriendRequest').getCount();
			if(count > 0) {
				me.getMenu().add([{
					xtype: 'button',
					cls: 'menuButton',
					itemId: 'friendRequest',
					badgeText: count,
	                text: L('menu.friendRequest'),
	                iconCls: 'fa fa-plus fa-fw'
				}]);
			}
		}, this);		
	},

	sendMessageErrorSendRequest: function() {
		var me = this;
		me.destroyModalErrorSendFriendRequest();
    	Dietofpoints.app.getController('GoogleAnalytics').trackEvent('modalErrorSendFriendRequest', 'bt: yes');
    	//https://www.npmjs.com/package/cordova-plugin-social-message
        var message = {
            text: L('modal.template.modalErrorSendFriendRequest.message') + ' ' + Dietofpoints.app.market,
            url: Dietofpoints.app.market
        };
        window.socialmessage.send(message);
	},

	showPanelAddFriend: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackView('view: panelAddFriend');
		me.hideNavButtons();
		me.getNavigationview().down('#sendFriendRequest_navigationview').setHidden(false);		
		var component = Ext.widget('panelAddFriend',{
			title: L('panel.template.panelAddFriend.title')
		});
		me.getNavigationview().push(component);
	},

	sendFriendRequest: function() {
		var me = this;
		var email = me.getPanelAddFriend().down('#email').getValue().trim();
		if(email === '') {
			me.showModalCheckFilledFields();
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'sendFriendRequest',
				async: false,
				params: {
					email: email,
					user: localStorage.getItem('dietofpoints_code_user')
				},
				success: function (conn, response, options, eOpts) {
					var result = Ext.decode(conn.responseText);
					if(result.success == false && result.error === 'nonExistentUser') {
						me.showModalErrorSendFriendRequest();
					}
					else if(result.success == false && result.error === 'alreadyFriends') {
						me.showModalAlreadyFriends();
					}
					else if(result.success == false && result.error === 'openFriendRequestSent') {
						me.showModalOpenFriendRequestSent();
					}
					else if(result.success == false && result.error === 'openFriendRequestReceived') {
						me.showModalOpenFriendRequestReceived();
					}
					else {
						me.hideNavButtons();
						me.getNavigationview().down('#menu_navigationview').setHidden(false);
						me.getNavigationview().down('#addFriend_navigationview').setHidden(false);
						me.getNavigationview().pop(1);
						me.showModalSuccessSendFriendRequest();
					}
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
	},

	deleteFriend: function() {
		var me = this;
		me.destroyModalDeleteFriend();
		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'deleteFriend',
			async: false,
			params: {
				friend: me.friend,
				user: localStorage.getItem('dietofpoints_code_user')
			},
			success: function (conn, response, options, eOpts) {
				me.load_listFriend();
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});		
	},

	showModalDeleteFriend: function(friend) {
		var me = this;
		me.friend = friend;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalDeleteFriend');
		Ext.Viewport.add(panel);
		panel.show();
	},

	destroyModalDeleteFriend: function() {
		var me = this;
		me.getModalDeleteFriend().destroy();
	},

	showModalErrorSendFriendRequest: function(friend) {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalErrorSendFriendRequest');
		Ext.Viewport.add(panel);
		panel.show();
	},

	destroyModalSuccessSendFriendRequest: function() {
		var me = this;
		me.getModalSuccessSendFriendRequest().destroy();
	},

	showModalSuccessSendFriendRequest: function(friend) {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalSuccessSendFriendRequest');
		Ext.Viewport.add(panel);
		panel.show();
	},

	destroyModalOpenFriendRequestSent: function() {
		var me = this;
		me.getModalOpenFriendRequestSent().destroy();
	},

	showModalOpenFriendRequestSent: function(friend) {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalOpenFriendRequestSent');
		Ext.Viewport.add(panel);
		panel.show();
	},

	destroyModalOpenFriendRequestReceived: function() {
		var me = this;
		me.getModalOpenFriendRequestReceived().destroy();
	},

	showModalOpenFriendRequestReceived: function(friend) {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalOpenFriendRequestReceived');
		Ext.Viewport.add(panel);
		panel.show();
	},

	destroyModalAlreadyFriends: function() {
		var me = this;
		me.getModalAlreadyFriends().destroy();
	},

	showModalAlreadyFriends: function(friend) {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalAlreadyFriends');
		Ext.Viewport.add(panel);
		panel.show();
	},

	destroyModalErrorSendFriendRequest: function() {
		var me = this;
		me.getModalErrorSendFriendRequest().destroy();
	},

	destroyModalErrorChooseRate: function() {
		var me = this;
		me.getModalErrorChooseRate().destroy();
	},

	showModalErrorChooseRate: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalErrorChooseRate');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showHelpdesk: function() {
		var me = this;
		me.destroyModalBadRate();
		me.openHelpdesk();
	},

	destroyModalBadRate: function() {
		var me = this;
		me.getModalBadRate().destroy();
	},

	showModalBadRate: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalBadRate');
		Ext.Viewport.add(panel);
		panel.show();
	},

	share: function() {
    	var me = this;
    	Dietofpoints.app.getController('GoogleAnalytics').trackEvent('menu', 'bt: share');
    	//https://www.npmjs.com/package/cordova-plugin-social-message
        var message = {
            text: L('socialMessage.text') + ' ' + Dietofpoints.app.market,
            url: Dietofpoints.app.market
        };
        window.socialmessage.send(message);
		me.hideMenu();
    },

	starts_panelAbout: function() {
		var me = this;
		me.set_panelAbout();
		me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		me.updateMenuButtonColor('#about');
	},

	starts_listFriend: function() {
		var me = this;
		me.load_listFriend();
		me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		me.getNavigationview().down('#addFriend_navigationview').setHidden(false);
		me.updateMenuButtonColor('#friend');
	},

	set_panelAbout: function() {
		var me = this;
		me.getPanelAbout().setData(
			{
				text: L('panel.template.panelAbout.text')
			}
		);
	},

	openHelpdesk: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('menu', 'bt: Help');
		//https://www.npmjs.com/package/cordova-plugin-inappbrowser
		cordova.InAppBrowser.open(Dietofpoints.app.helpdesk, '_blank', 'location=yes');
		me.hideMenu();
	},

	showPanelLogin: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: main', 'bt: login');
		me.hideNavButtons();
		me.getNavigationview().getNavigationBar().setHidden(false);
		var component = Ext.widget('login',{
			title: L('panel.template.login.title')
		});
		me.getNavigationview().push(component);
	},

	login: function() {
		var me = this;
		var email = me.getLogin().down('#email').getValue().trim();
		var password = me.getLogin().down('#password').getValue();
		if(email === '' || password === '') {
			me.showModalCheckFilledFields();
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'login',
				params: {
					email: email,
					password: password
				},
				success: function (conn, response, options, eOpts) {
					var result = Ext.decode(conn.responseText);
					if(result.length === 0) {
						me.showModalErrorLogin();
					}
					else {
						localStorage.setItem('dietofpoints_code_user', result[0]['code_user']);
						localStorage.setItem('dietofpoints_goal_user', result[0]['points_user']);
						localStorage.setItem('dietofpoints_name_user', result[0]['name_user']);

						me.startsMenu();
						me.set_panelMenu();
			        	me.updateMenuButtonColor('#diary');
						me.destroyComponents();
						var component = Ext.widget('diary',{
							title: L('panel.template.diary')
						});
						me.getNavigationview().push(component);
					}
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
	},

	finishRegister: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: panelAccessData', 'bt: next');
		me.registerUser();
		me.recalculateDiaryPoints();
		me.hideNavButtons();
		me.startsMenu();
		me.set_panelMenu();
    	me.updateMenuButtonColor('#diary');
		me.destroyComponents();
		var component = Ext.widget('diary',{
			title: L('panel.template.diary')
		});
		me.getNavigationview().push(component);
	},

	registerUser: function() {
		var me = this;
		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'registerUser',
			async: false,
			params: {
				language: localStorage.getItem('dietofpoints_language'),
				activityLevel: me.activityLevel,
				name: me.name,
				gender: me.gender,
				age: me.age,
				height: me.height,
				currentWeight: me.weight,
				goalWeight: me.goalWeight,
				email: me.email,
				password: me.password,
				registrationDate: me.formatDateToPortugueseFormat(new Date())
			},
			success: function (conn, response, options, eOpts) {
				var result = Ext.decode(conn.responseText);
				localStorage.setItem('dietofpoints_code_user', result.code);
				localStorage.setItem('dietofpoints_goal_user', result.points);
				localStorage.setItem('dietofpoints_name_user', result.name);
			},
			//Caso não tenha conseguido fazer a requisição
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});		
	},

	savePanelAccessData: function() {
		var me = this;
		var email = me.getPanelAccessData().down('#email').getValue().trim();
		var password = me.getPanelAccessData().down('#password').getValue();
		if(email === '' || password === '') {
			me.showModalCheckFilledFields();
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'checkEmail',
				async: false,
				params: {
					email: email
				},
				success: function (conn, response, options, eOpts) {
					var result = Ext.decode(conn.responseText);
					if(result.length === 0) {
						me.email = email;
						me.password = password;
						me.finishRegister();
					}
					else {
						me.showModalErrorEmailRegister();
					}
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});	
		}
	},

	cleanRadioButtons: function() {
		var me = this;
		var elements = document.getElementsByClassName('radio');
		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].checked = 0;
		}
	},

	activityLevelRegister: function() {
		var me = this;
		if(!me.findRadioChecked()) {
			me.showModalCheckFilledFields();
		}
		else {
			var elements = document.getElementsByClassName('radio');
			var option = 0;
			for(cont = 0; cont < elements.length; cont++) {
				if(elements[cont].checked == 1) {
					option = elements[cont].value;
				}
			}
			me.activityLevel = option;
			me.showPanelYou();
		}
	},

	showPanelYou: function() {
		var me = this;
		me.hideNavButtons();
		me.getNavigationview().down('#savePanelYou_navigationview').setHidden(false);
		var component = Ext.widget('panelYou',{
			title: L('panel.template.panelYou.title')
		});
		me.getNavigationview().push(component);
	},

	showPanelAccessData: function() {
		var me = this;
		me.hideNavButtons();
		me.getNavigationview().down('#savePanelAccessData_navigationview').setHidden(false);
		var component = Ext.widget('panelAccessData',{
			title: L('panel.template.panelAccessData.title')
		});
		me.getNavigationview().push(component);
	},

	savePanelYou: function() {
		var me = this;
		var name = me.getPanelYou().down('#name').getValue();
		var age = me.getPanelYou().down('#age').getValue();
		var height = me.getPanelYou().down('#height').getValue();
		var weight = me.getPanelYou().down('#weight').getValue();
		var goalWeight = me.getPanelYou().down('#goalWeight').getValue();
		if(!me.findRadioChecked()) {
			me.showModalCheckFilledFields();
		}
		else {
			if(name === '' || age === null || height === null || weight === null || goalWeight === null) {
				me.showModalCheckFilledFields();
			}
			else {
				var elements = document.getElementsByClassName('radio');
				var option = 0;
				for(cont = 0; cont < elements.length; cont++) {
					if(elements[cont].checked == 1) {
						option = elements[cont].value;
					}
				}
				me.name = name;
				me.gender = option;
				me.age = age;
				me.height = height;
				me.weight = weight;
				me.goalWeight = goalWeight;
				me.showPanelAccessData();
			}
		}
	},

	starts_panelGender_you: function() {
		var me = this;
		me.getPanelGender_you().setData(
            {
                name1: L('panel.template.panelGender_you.female'),
                code1: 1,
                name2: L('panel.template.panelGender_you.male'),
                code2: 2
            }
        );
	},

	saveIdiom: function() {
		var me = this;
		if(!me.findRadioChecked()) {
			me.showModalCheckFilledFields();
		}
		else {
			var elements = document.getElementsByClassName('radio');
			var option = 0;
			for(cont = 0; cont < elements.length; cont++) {
				if(elements[cont].checked == 1) {
					option = elements[cont].value;
				}
			}
			localStorage.setItem('dietofpoints_language', option);
			me.showModalChangeLanguage();
		}
	},

	showPanelActivityLevelRegister: function() {
		var me = this;
		me.hideNavButtons();
		me.getNavigationview().down('#saveActivityLevelRegister_navigationview').setHidden(false);
		var component = Ext.widget('panelActivityLevel_user',{
			title: L('panel.template.panelActivityLevel_user.title')
		});
		me.getNavigationview().push(component);
	},

	register: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: main', 'bt: register');
		me.getNavigationview().getNavigationBar().setHidden(false);
		me.showPanelActivityLevelRegister();
	},

	starts_panelIdiom: function() {
		var me = this;
		me.getPanelIdiom().setData(
            {
                name1: L('panel.template.panelIdiom.english'),
                code1: 'en',
                name2: L('panel.template.panelIdiom.spanish'),
                code2: 'es',
                name3: L('panel.template.panelIdiom.portuguese'),
                code3: 'pt'
            }
        );		
	},

	checkIdiom: function() {
        if(localStorage.getItem('dietofpoints_language')) {
        	return true;
        }
        else {
        	return false;
        }
	},

	starts_modalAddMeal: function() {
		var me = this;
		me.getModalAddMeal().setData(
            {
                title: L('modal.template.modalAddMeal.title'),
                breakfast: L('modal.template.modalAddMeal.breakfast'),
                lunch: L('modal.template.modalAddMeal.lunch'),
                dinner: L('modal.template.modalAddMeal.dinner'),
                snacks: L('modal.template.modalAddMeal.snacks')
            }
        );
	},

	addMeal: function(period) {
		var me = this;
		me.destroyModalAddMeal();
		var store = Ext.getStore('ListAddMeal');
		store.each(function (item, index, length) {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'addMeal',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					period: period,
					date: me.formatDateToPortugueseFormat(new Date()),
					name_food: item.get('name_food'),
					amount_food: item.get('amount_food'),
					points_food: item.get('points_food'),
					unit_food: item.get('unit_food')
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		});
		me.showPanelDiary();
	},

	showModalAddMeal: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalAddMeal');
		Ext.Viewport.add(panel);
		panel.show();
	},

	load_listAddMeal: function() {
		var me = this;
		var store = Ext.getStore('ListAddMeal');
		store.setParams(
			{code_meal: me.code_meal}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listAddMeal');
		store.removeAll();
		store.load();
	},

	showPanelAddMeal: function(code) {
		var me = this;
		me.code_meal = code;
		me.hideNavButtons();
		me.getNavigationview().down('#addMeal_navigationview').setHidden(false);
		var component = Ext.widget('panelAddMeal',{
			title: L('panel.template.panelAddMeal.title')
		});

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'getNameMeal',
			async: false,
			params: {
				code_meal: code
			},
			success: function (conn, response, options, eOpts) {
				var result = Ext.decode(conn.responseText);
				component.down('#nameMeal').setHtml(result.name);
				me.getNavigationview().push(component);
			},
			//Caso não tenha conseguido fazer a requisição
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});
	},

	createFoodFromTabpanelMyFoodAndMeals: function() {
		var me = this;
		var name = me.getPanelCreateFood().down('#name').getValue();
		var unit = me.getPanelCreateFood().down('#unit').getValue();
		var amount = me.getPanelCreateFood().down('#amount').getValue();
		var points = me.getPanelCreateFood().down('#points').getValue();
		if(name === '' || amount === '' || points === null || unit === null || unit == 0) {
			me.showModalCheckFilledFields();
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'createFoodFromTabpanelMyFoodAndMeals',
				async: false,
				params: {
					name: name,
					unit: unit,
					amount: amount,
					points: points/3.6,
					user: localStorage.getItem('dietofpoints_code_user')
				},
				success: function (conn, response, options, eOpts) {
					var result = Ext.decode(conn.responseText);
					if(result.success === true) {
						me.hideNavButtons();
						me.getNavigationview().down('#addTabpanelMyFoodAndMeals_navigationview').setHidden(false); 
						me.getNavigationview().down('#menu_navigationview').setHidden(false);
						me.starts_listMyFood_tabpanelMyFoodAndMeals();
						me.getNavigationview().pop(1);
					}
					else {
						me.showModalErrorCreateFood();
					}
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
	},

	showPanelCreateFoodFromTabpanelMyFoodAndMeals: function() {
		var me = this;
		me.hideNavButtons();
		me.getNavigationview().down('#saveCreateFoodFromTabpanelMyFoodAndMeals_navigationview').setHidden(false);
		var component = Ext.widget('panelCreateFood',{
			title: L('panel.template.panelCreateFood.title')
		});
		me.getNavigationview().push(component);	
	},

	updateUserFood: function() {

		var me = this;
		var name = me.getPanelUpdateUserFood().down('#name').getValue();
		var amount = me.getPanelUpdateUserFood().down('#amount').getValue();
		var portions = me.getPanelUpdateUserFood().down('#portions').getValue();
		var points = me.getPanelUpdateUserFood().down('#points').getValue();
		var code = me.getPanelUpdateUserFood().down('#code').getValue();

		if(name === '' || amount === '' || portions === null || points === null) {
			me.showModalCheckFilledFields();
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'updateUserFood',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					code_userfood: code,
					name: name,
					amount: amount,
					portion: portions,
					point: points
				},
				success: function (conn, response, options, eOpts) {
					me.hideNavButtons();
					me.getNavigationview().down('#addTabpanelMyFoodAndMeals_navigationview').setHidden(false); 
					me.getNavigationview().down('#menu_navigationview').setHidden(false);
					me.starts_listMyFood_tabpanelMyFoodAndMeals();
					me.getNavigationview().pop(1);
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}		
	},

	editTabpanelMyFoodAndMeals: function() {
		var me = this;
		me.showCheckbox();
		me.hideNavButtons();
		me.getNavigationview().down('#closeTabpanelMyFoodAndMeals_navigationview').setHidden(false);
		me.getNavigationview().down('#trashTabpanelMyFoodAndMeals_navigationview').setHidden(false); 
	},

	closeTabpanelMyFoodAndMeals: function() {
		var me = this;
		var elements = document.getElementsByClassName('checkbox');
		//Clean all checkbox
		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].checked = 0;
		}
		me.hideCheckbox();
		me.hideNavButtons();
		var currentXtype = me.getXtypeByView(me.getTabpanelMyFoodAndMeals().getActiveItem());
		if(currentXtype === 'listMyFood_tabpanelMyFoodAndMeals') {
			me.getNavigationview().down('#menu_navigationview').setHidden(false); 
			me.getNavigationview().down('#addTabpanelMyFoodAndMeals_navigationview').setHidden(false);
			me.starts_listMyFood_tabpanelMyFoodAndMeals();
		}
		else {
			me.getNavigationview().down('#menu_navigationview').setHidden(false);
			me.starts_listMyMeals_tabpanelMyFoodAndMeals();
		}
	},

	starts_listMyFood_tabpanelMyFoodAndMeals: function() {
		var me = this;
		var store = Ext.getStore('ListMyFood_tabpanelMyFoodAndMeals');
		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user')}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listMyFood_tabpanelMyFoodAndMeals');
		store.removeAll();
		store.load(function() {
			if(Ext.getStore('ListMyFood_tabpanelMyFoodAndMeals').getCount() > 0) {
				me.getNavigationview().down('#editTabpanelMyFoodAndMeals_navigationview').setHidden(false);	
			}
		}, this);
	},

	starts_listMyMeals_tabpanelMyFoodAndMeals: function() {
		var me = this;
		var store = Ext.getStore('ListMyMeals_tabpanelMyFoodAndMeals');
		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user')}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listMyMeals_tabpanelMyFoodAndMeals');
		store.removeAll();
		store.load(function() {
			if(Ext.getStore('ListMyMeals_tabpanelMyFoodAndMeals').getCount() > 0) {
				me.getNavigationview().down('#editTabpanelMyFoodAndMeals_navigationview').setHidden(false);	
			}
		}, this);
	},

	showTabPanelMyFoodAndMeals: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: menu', 'bt: myFoodAndMeals');
		var currentXtype = me.getXtypeByView(me.getNavigationview().getActiveItem());
        if (currentXtype != 'tabpanelMyFoodAndMeals') {
			me.destroyComponents();
			var component = Ext.widget('tabpanelMyFoodAndMeals', {
				title: L('panel.template.tabpanelMyFoodAndMeals.title')
			});
			me.getNavigationview().push(component);
		}
		me.hideMenu();
	},

	starts_tabpanelMyFoodAndMeals: function() {
		var me = this;
		me.updateMenuButtonColor('#myFoodAndMeals');
		me.hideNavButtons();
		me.getNavigationview().down('#addTabpanelMyFoodAndMeals_navigationview').setHidden(false); 
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
	},

	itemChange_tabPanelMyFoodAndMeals: function() {
		var me = this;
		var elements = document.getElementsByClassName('checkbox');
		//Clean all checkbox
		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].checked = 0;
		}
		me.hideCheckbox();
	},

	show_listMyMeals_tabpanelMyFoodAndMeals: function() {
		var me = this;
		me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		me.starts_listMyMeals_tabpanelMyFoodAndMeals();
	},

	show_listMyFood_tabpanelMyFoodAndMeals: function() {
		var me = this;
		me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		me.getNavigationview().down('#addTabpanelMyFoodAndMeals_navigationview').setHidden(false); 
		me.starts_listMyFood_tabpanelMyFoodAndMeals();
	},

	starts_panelProfile: function() {
		var me = this;
		me.set_panelsProfile();
		//Set buttons in Navigationbar
		me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		me.updateMenuButtonColor('#profile');
	},

	showModalErrorActualWeight: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorActualWeight');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorGoalWeight: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorGoalWeight');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorHeight: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorHeight');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorAge: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorAge');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalActualWeight: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalActualWeight');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalGoalWeight: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalGoalWeight');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalHeight: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalHeight');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalAge: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalAge');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorLogin: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalErrorLogin');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorEmailRegister: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalErrorEmailRegister');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalConfirmLogout: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: menu', 'bt: logout');		
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalConfirmLogout');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalChangeLanguage: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalChangeLanguage');
		Ext.Viewport.add(panel);
		panel.show();
	},

	destroyModalActualWeight: function() {
		var me = this;
		me.getModalActualWeight().destroy();
	},

	destroyModalChangeLanguage: function() {
		var me = this;
		me.getModalChangeLanguage().destroy();
		if(window.cordova) {
			navigator.splashscreen.show();
		}
		window.location.reload(true);
	},

	destroyModalConfirmLogout: function() {
		var me = this;
		me.getModalConfirmLogout().destroy();
	},

	destroyModalErrorEmailRegister: function() {
		var me = this;
		me.getModalErrorEmailRegister().destroy();
	},

	destroyModalErrorLogin: function() {
		var me = this;
		me.getModalErrorLogin().destroy();
	},

	destroyModalAddMeal: function() {
		var me = this;
		me.getModalAddMeal().destroy();
	},

	destroyModalWater: function() {
		var me = this;
		me.getModalWater().destroy();
	},

	destroyModalGoalWeight: function() {
		var me = this;
		me.getModalGoalWeight().destroy();
	},

	destroyModalHeight: function() {
		var me = this;
		me.getModalHeight().destroy();
	},

	destroyModalAge: function() {
		var me = this;
		me.getModalAge().destroy();
	},

	showPanelActivityLevel_user: function() {
		var me = this;
		me.hideNavButtons();
		me.getNavigationview().down('#saveActivityLevel_navigationview').setHidden(false);
		var component = Ext.widget('panelActivityLevel_user',{
			title: L('panel.template.panelActivityLevel_user.title')
		});
		me.getNavigationview().push(component);	
	},

	set_modalActivityLevel: function() {
		var me = this;		
		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'get_activityLevels',
			async: false,
			params: {
				idiom: L('language.idiom')
			},
			success: function (conn, response, options, eOpts) {
				var result = Ext.decode(conn.responseText);
				me.getPanelActivityLevel_user().setData(
		            {
		                code1: result.code0,
		                name1: result.name0,
		                code2: result.code1,
		                name2: result.name1,
		                code3: result.code2,
		                name3: result.name2,
		                code4: result.code3,
		                name4: result.name3,
		                code5: result.code4,
		                name5: result.name4,
		                code6: result.code5,
		                name6: result.name5
		            }
		        );
			},
			//Caso não tenha conseguido fazer a requisição
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});		
	},

	actualWeight_user: function() {
		
		var me = this;

		var weight = me.getModalActualWeight().down('#weight').getValue();
		me.destroyModalActualWeight();

		if(weight != null) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'addWeight',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					date: me.formatDateToPortugueseFormat(new Date()),
					weight: weight
				},
				success: function (conn, response, options, eOpts) {
					me.recalculateDiaryPoints();
					me.set_panelsProfile();
					me.set_panelMenu();
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorActualWeight();
		}
	},

	goalWeight_user: function() {

		var me = this;

		var weight = me.getModalGoalWeight().down('#weight').getValue();
		me.destroyModalGoalWeight();

		if(weight != null) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'goalWeight_user',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					weight: weight
				},
				success: function (conn, response, options, eOpts) {
					me.recalculateDiaryPoints();
					me.set_panelsProfile();
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorGoalWeight();
		}
	},

	activityLevel_user: function() {
		
		var me = this;

		if(!me.findRadioChecked()) {
			me.showModalCheckFilledFields();
		}
		else {
			var elements = document.getElementsByClassName('radio');
			var option = 0;

			for(cont = 0; cont < elements.length; cont++) {
				if(elements[cont].checked == 1) {
					option = elements[cont].value;
				}
			}

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'activityLevel_user',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					option: option
				},
				success: function (conn, response, options, eOpts) {
					me.recalculateDiaryPoints();
					me.set_panelsProfile();
					me.hideNavButtons();
					me.getNavigationview().down('#menu_navigationview').setHidden(false);
					me.getNavigationview().pop(1);
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
	},

	height_user: function() {
		
		var me = this;

		var height = me.getModalHeight().down('#height').getValue();
		me.destroyModalHeight();

		if(height != null) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'height_user',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					height: height
				},
				success: function (conn, response, options, eOpts) {
					me.recalculateDiaryPoints();
					me.set_panelsProfile();
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorHeight();
		}
	},

	age_user: function() {
		
		var me = this;

		var age = me.getModalAge().down('#age').getValue();
		me.destroyModalAge();

		if(age != null) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'age_user',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					age: age
				},
				success: function (conn, response, options, eOpts) {
					me.recalculateDiaryPoints();
					me.set_panelsProfile();
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorAge();
		}
	},

	set_panelsProfile: function() {
		var me = this;

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'showPanelProfile',
			async: false,
			params: {
				user: localStorage.getItem('dietofpoints_code_user'),
				idiom: L('language.idiom')
			},
			success: function (conn, response, options, eOpts) {
				var result = Ext.decode(conn.responseText);
				me.getPanelInitialWeight().setData(
		            {
		                text: result.initialweight
		            }
		        );
		        me.getPanelActualWeight().setData(
		            {
		                text: result.actualweight
		            }
		        );
		        me.getPanelGoalWeight().setData(
		            {
		                text: result.goalweight
		            }
		        );
		        me.getPanelActivityLevel().setData(
		            {
		                text: result.activitylevel
		            }
		        );
		        me.getPanelHeight().setData(
		            {
		                text: result.height
		            }
		        );
		        me.getPanelAge().setData(
		            {
		                text: result.age
		            }
		        );				
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});
	},

	starts_panelListWeight: function() {
		var me = this;
		//Set buttons in Navigationbar
		me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		me.getNavigationview().down('#registerWeight_navigationview').setHidden(false);
		me.updateMenuButtonColor('#myWeight');
	},

	//Calculate difference in days between the received date and actual date
	diff_days: function(date) {
		today = new Date();
        day = today.getDate();
        month = today.getMonth();
        year = today.getFullYear();
        year_received = date.substr(6);
        month_received = date.substr(3, 2);
        day_received = date.substr(0, 2);
        var z = new Date(Date.parse(year_received + "," + month_received + "/" + day_received));
        var x = new Date(Date.parse(today.getFullYear() + "," + (today.getMonth() + 1) + "/" + today.getDate()));
        var oneDay = 1000 * 60 * 60 * 24;
        var c = parseInt(x.getTime() - z.getTime());
        var d = (c/oneDay);
        return d;
	},

	set_panelMenu: function() {

		var me = this;

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'panelMenu',
			async: false,
			params: {
				user: localStorage.getItem('dietofpoints_code_user')
			},
			success: function (conn, response, options, eOpts) {
				var result = Ext.decode(conn.responseText);
				var text_balance = '';
				if(result.text_balance === 'positive') {
					text_balance = L('panel.template.panelMenu.text_balance_positive');
				}
				else {
					text_balance = L('panel.template.panelMenu.text_balance_negative');
				}

				me.getPanelMenu().setData(
		            {
		                username: localStorage.getItem('dietofpoints_name_user'),
		                consecutive_days: me.diff_days(result.registrationDate),
		                weight_balance: result.weight_balance + ' kg',
		                text_balance: text_balance
		            }
		        );
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});
	},

	set_panelTopListWeight: function() {
		var me = this;

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'panelTopListWeight',
			async: false,
			params: {
				user: localStorage.getItem('dietofpoints_code_user')
			},
			success: function (conn, response, options, eOpts) {
				var result = Ext.decode(conn.responseText);
				var lostWeight = 0;
				if(result.text_balance === 'positive') {
					lostWeight = 0;
				}
				else {
					lostWeight = 1;
				}

				me.getPanelTopListWeight().setData(
		            {
		                consecutive_days: me.diff_days(result.registrationDate),
		                weight_balance: result.weight_balance + ' kg',
		                lostWeight: lostWeight,
		                idealWeight_balance: result.idealWeight_balance + ' kg',
		                idealWeight: result.idealWeight + ' kg',
		                haveToLostWeight: result.haveToLostWeight
		            }
		        );
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});
	},

	startsMenu: function() {
		Dietofpoints.app.addMenu();
	},

	updateMenuButtonColor: function(button) {
		var me = this;
		if(me.getMenu()) {
			var menu = me.getMenu();
			menu.down('#diary').setCls('menuButton');
			menu.down('#profile').setCls('menuButton');
			menu.down('#myFoodAndMeals').setCls('menuButton');
			menu.down('#help').setCls('menuButton');
			menu.down('#logout').setCls('menuButton');
			menu.down('#myWeight').setCls('menuButton');
			menu.down('#about').setCls('menuButton');
			menu.down('#friend').setCls('menuButton');
			if(menu.down('#friendRequest')) {
				menu.down('#friendRequest').setCls('menuButton');
			}
			menu.down(button).setCls('selectedButtonMenu');
		}
	},

	recalculateDiaryPoints: function() {

		var me = this;

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'recalculateDiaryPoints',
			async: false,
			params: {
				user: localStorage.getItem('dietofpoints_code_user')
			},
			success: function (conn, response, options, eOpts) {

				var result = Ext.decode(conn.responseText);

				var age = result.age;
				var weight = result.weight;
				var gender = result.gender;
				var sedentarylevel = result.sedentarylevel;
				var height = result.height;

				//First step
				var tbm = 0;
				var calculatedSedentaryLevel = 0;
				//If female
				if(gender === 1) {

					if(age <= 18) {
						tbm = 12.2 * weight + 746;
					}
					else if(age >= 19 && age <= 30) {
						tbm = 14.7 * weight + 496;
					}
					else if(age >= 31 && age <= 60) {
						tbm = 8.7 * weight + 829;
					}
					else {
						tbm = 10.5 * weight + 596;
					}

					switch(sedentarylevel) {
						case ('1'):
				    		calculatedSedentaryLevel = 1.2;
 				    	break;
				    	case ('2'):
				        	calculatedSedentaryLevel = 1.3;
				        break;
				    	case ('3'):
				        	calculatedSedentaryLevel = 1.35;
				        break;
				        case ('4'):
				        	calculatedSedentaryLevel = 1.45;
				        break;
				        case ('5'):
				        	calculatedSedentaryLevel = 1.5;
				        break;
				        case ('6'):
				        	calculatedSedentaryLevel = 1.7;
					}
				}
				//Male
				else {

					if(age <= 18) {
						tbm = 17.5 * weight + 651;
					}
					else if(age >= 19 && age <= 30) {
						tbm = 15.3 * weight + 679;
					}
					else if(age >= 31 && age <= 60) {
						tbm = 8.7 * weight + 487;
					}
					else {
						tbm = 13.5 * weight + 487;
					}

					switch(sedentarylevel) {
						case ('1'):
				    		calculatedSedentaryLevel = 1.2;
 				    	break;
				    	case ('2'):
				        	calculatedSedentaryLevel = 1.3;
				        break;
				    	case ('3'):
				        	calculatedSedentaryLevel = 1.4;
				        break;
				        case ('4'):
				        	calculatedSedentaryLevel = 1.5;
				        break;
				        case ('5'):
				        	calculatedSedentaryLevel = 1.6;
				        break;
				        case ('6'):
				        	calculatedSedentaryLevel = 1.8;
					}
				}

				//Second step
				var calories = tbm * calculatedSedentaryLevel;
				//Third step
				var points = calories / 3.6;
				//Fourth step
				var imc = weight / (height * height);
				var calculatedImc = 0;
				//If female
				if(gender === 1) {
					if(imc <= 29) {
						calculatedImc = 175;
					}
					else if(imc >= 30 && imc <= 34) {
						calculatedImc = 200;
					}
					else if(imc >= 35 && imc <= 39) {
						calculatedImc = 250;
					}
					else {
						calculatedImc = 300;
					}
				}
				//Male
				else {
					if(imc <= 29) {
						calculatedImc = 200;
					}
					else if(imc >= 30 && imc <= 34) {
						calculatedImc = 225;
					}
					else if(imc >= 35 && imc <= 39) {
						calculatedImc = 275;
					}
					else {
						calculatedImc = 325;
					}
				}

				//Fifth step
				var calculatedPoints = points - calculatedImc;

				Ext.Ajax.request({
					url: Dietofpoints.app.data_server + 'updatePoints',
					async: false,
					params: {
						user: localStorage.getItem('dietofpoints_code_user'),
						points: calculatedPoints
					},
					success: function (conn, response, options, eOpts) {
						var result = Ext.decode(conn.responseText);
						localStorage.setItem('dietofpoints_goal_user', result.points);
					},
					failure: function (conn, response, options, eOpts) {
						me.messageBox_networkError();
					}
				});
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});		
	},

	showPanelListWeight: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: menu', 'bt: myWeight');
		var currentXtype = me.getXtypeByView(me.getNavigationview().getActiveItem());
        if (currentXtype != 'panelListWeight') {
			me.destroyComponents();
			var component = Ext.widget('panelListWeight',{
				title: L('panel.template.panelListWeight.title')
			});
			me.getNavigationview().push(component);
		}
		me.hideMenu();
	},

	showPanelProfile: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: menu', 'bt: profile');
		var currentXtype = me.getXtypeByView(me.getNavigationview().getActiveItem());
        if (currentXtype != 'panelProfile') {
			me.destroyComponents();
			var component = Ext.widget('panelProfile',{
				title: L('panel.template.panelProfile.title')
			});
			me.getNavigationview().push(component);
		}
		me.hideMenu();
	},

	showPanelAbout: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: menu', 'bt: about');
		var currentXtype = me.getXtypeByView(me.getNavigationview().getActiveItem());
        if (currentXtype != 'panelAbout') {
			me.destroyComponents();
			var component = Ext.widget('panelAbout',{
				title: L('panel.template.panelAbout.title')
			});
			me.getNavigationview().push(component);
		}
		me.hideMenu();
	},

	showListFriend: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: menu', 'bt: friend');
		var currentXtype = me.getXtypeByView(me.getNavigationview().getActiveItem());
        if (currentXtype != 'listFriend') {
			me.destroyComponents();
			var component = Ext.widget('listFriend',{
				title: L('list.template.listFriend.title')
			});
			me.getNavigationview().push(component);
		}
		me.hideMenu();
	},

	createFood: function() {
		var me = this;
		var name = me.getPanelCreateFood().down('#name').getValue();
		var unit = me.getPanelCreateFood().down('#unit').getValue();
		var amount = me.getPanelCreateFood().down('#amount').getValue();
		var points = me.getPanelCreateFood().down('#points').getValue();
		if(name === '' || amount === '' || points === null || unit === null || unit == 0) {
			me.showModalCheckFilledFields();
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'createFood',
				async: false,
				params: {
					name: name,
					unit: unit,
					amount: amount,
					points: points/3.6,
					user: localStorage.getItem('dietofpoints_code_user'),
					date: me.formatDateToPortugueseFormat(),
					period: me.period
				},
				success: function (conn, response, options, eOpts) {

					var result = Ext.decode(conn.responseText);

					if(result.success === true) {
						me.showDiaryAfterCreateFood();
					}
					else {
						me.showModalErrorCreateFood();
					}
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
	},

	messageBox_networkError: function() {
		Ext.Msg.show({
			title: L('ERROR_TITLE'),
			cls: 'messageBox',
			message: L('messageBox_networkError'),
			buttons: Ext.MessageBox.OK,
			fn: Ext.emptyFn
		});
	},

	hideNavButtons: function() {
		var me = this;
		me.getNavigationview().down('#menu_navigationview').setHidden(true);
		me.getNavigationview().down('#close_navigationview').setHidden(true);
		me.getNavigationview().down('#edit_navigationview').setHidden(true);
		me.getNavigationview().down('#trash_navigationview').setHidden(true);
		me.getNavigationview().down('#options_navigationview').setHidden(true);
		me.getNavigationview().down('#add_navigationview').setHidden(true);
		me.getNavigationview().down('#saveCreateFood_navigationview').setHidden(true);
		me.getNavigationview().down('#saveUpdatePortion_navigationview').setHidden(true);
		me.getNavigationview().down('#saveUpdateCalories_navigationview').setHidden(true);
		me.getNavigationview().down('#registerWeight_navigationview').setHidden(true);
		me.getNavigationview().down('#saveActivityLevel_navigationview').setHidden(true);
		me.getNavigationview().down('#editTabpanelMyFoodAndMeals_navigationview').setHidden(true);
		me.getNavigationview().down('#addTabpanelMyFoodAndMeals_navigationview').setHidden(true);
		me.getNavigationview().down('#closeTabpanelMyFoodAndMeals_navigationview').setHidden(true);
		me.getNavigationview().down('#updateUserFood_navigationview').setHidden(true);
		me.getNavigationview().down('#saveCreateFoodFromTabpanelMyFoodAndMeals_navigationview').setHidden(true);
		me.getNavigationview().down('#trashTabpanelMyFoodAndMeals_navigationview').setHidden(true);
		me.getNavigationview().down('#addMeal_navigationview').setHidden(true);
		me.getNavigationview().down('#saveIdiom_navigationview').setHidden(true);
		me.getNavigationview().down('#saveActivityLevelRegister_navigationview').setHidden(true);
		me.getNavigationview().down('#savePanelYou_navigationview').setHidden(true);
		me.getNavigationview().down('#savePanelAccessData_navigationview').setHidden(true);
		me.getNavigationview().down('#addFriend_navigationview').setHidden(true);
		me.getNavigationview().down('#sendFriendRequest_navigationview').setHidden(true);
	},

	showPanelDiary: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: menu', 'bt: diary');
		var currentXtype = me.getXtypeByView(me.getNavigationview().getActiveItem());
        if (currentXtype != 'diary') {
			me.destroyComponents();
			var component = Ext.widget('diary',{
				title: L('panel.template.diary')
			});
			me.getNavigationview().push(component);
		}
		me.hideMenu();
	},

	hideMenu: function() {
		Ext.Viewport.hideMenu('left');
	},

	showMenu: function(){
		Ext.Viewport.showMenu('left');
	},

	showPanelCreateFood: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackView('panelCreateFood');
		me.hideNavButtons();
		me.getNavigationview().down('#saveCreateFood_navigationview').setHidden(false);

		var component = Ext.widget('panelCreateFood',{
			title: L('panel.template.panelCreateFood.title')
		});
		me.getNavigationview().push(component);	
	},

	add_food: function() {
		var me = this;
		//Get xtype of active component in TabpanelAddFood component
		var activeComponent = me.getXtypeByView(me.getTabpanelAddFood().getActiveItem());
		var elements = document.getElementsByClassName('checkbox');
		var size = 0;

		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {
				size++;
			}
		}

		if(size == 0) {
			me.showModalErrorDeleteItems();
		}
		else {
			switch(activeComponent) {
				case 'panelAllFood':
		    		me.addFood_panelAllFood();
		    	break;
		    	case 'panelMyFood':
		        	me.addFood_panelMyFood();
		        break;
		    	case 'panelMyMeals':
		        	me.addFood_panelMyMeals();
			}
		}
	},

	addFood_panelAllFood: function() {
		var me = this;
		var elements = document.getElementsByClassName('checkbox');
		var data = [];

		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {
				data.push(elements[cont].value);
			}
		}

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'addFood_panelAllFood',
			async: false,
			params: {
				code_food: JSON.stringify(data),
				period: me.period,
				user: localStorage.getItem('dietofpoints_code_user'),
				date: me.formatDateToPortugueseFormat(),
				idiom: L('language.idiom')
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});

		me.showDiaryAfterAddFood();
	},

	addFood_panelMyFood: function() {
		var me = this;
		var elements = document.getElementsByClassName('checkbox');
		var data = [];

		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {
				data.push(elements[cont].value);
			}
		}

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'addFood_panelMyFood',
			async: false,
			params: {
				code_userfood: JSON.stringify(data),
				period: me.period,
				user: localStorage.getItem('dietofpoints_code_user'),
				date: me.formatDateToPortugueseFormat()
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});

		me.showDiaryAfterAddFood();
	},

	addFood_panelMyMeals: function() {
		var me = this;
		var elements = document.getElementsByClassName('checkbox');
		var data = [];

		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {
				data.push(elements[cont].value);
			}
		}

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'addFood_panelMyMeals',
			async: false,
			params: {
				code_meal: JSON.stringify(data),
				period: me.period,
				user: localStorage.getItem('dietofpoints_code_user'),
				date: me.formatDateToPortugueseFormat()
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});

		me.showDiaryAfterAddFood();
	},

	showDiaryAfterAddFood: function() {
		var me = this;
		me.setPeriod();
		me.getNavigationview().getNavigationBar().setTitle(L('panel.template.diary'));
		me.getNavigationview().pop(1);
	},

	showDiaryAfterCreateFood: function() {
		var me = this;
		me.setPeriod();
		me.getNavigationview().getNavigationBar().setTitle(L('panel.template.diary'));
		me.getNavigationview().pop(2);
	},

	unckeckCheckboxes: function() {
		var elements = document.getElementsByClassName('checkbox');
		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].checked = 0;
		}
	},

	cleanStores: function() {
		var me = this;
		me.cleanStoreListAllFood();
		me.cleanStoreListMyFood();
		me.cleanStoreListMyMeals();
	},

	cleanStoreListAllFood: function() {
		var store = Ext.getStore('ListAllFood');
		store.clearFilter();
		store.removeAll();
	},

	searchFood_panelAllFood: function() {
		var me = this;

		if(me.getPanelAllFood().down('#searchfield').getValue() != '') {
			var store = Ext.getStore('ListAllFood');
			store.setParams(
				{food: me.getPanelAllFood().down('#searchfield').getValue(), idiom: L('language.idiom')}
			);
			store.getProxy().setUrl(Dietofpoints.app.data_server + 'listAllFood');
			store.removeAll();
			store.load();
		}
		else {
			me.cleanStoreListAllFood();
		}
	},

	cleanStoreListMyFood: function() {
		var store = Ext.getStore('ListMyFood');
		store.clearFilter();
		store.removeAll();
	},

	searchFood_panelMyFood: function() {
		var me = this;

		if(me.getPanelMyFood().down('#searchfield').getValue() != '') {
			var store = Ext.getStore('ListMyFood');
			store.setParams(
				{food: me.getPanelMyFood().down('#searchfield').getValue(), user: localStorage.getItem('dietofpoints_code_user')}
			);
			store.getProxy().setUrl(Dietofpoints.app.data_server + 'listMyFood');
			store.removeAll();
			store.load();
		}
		else {
			me.cleanStoreListMyFood();
		}
	},

	cleanStoreListMyMeals: function() {
		var store = Ext.getStore('ListMyMeals');
		store.clearFilter();
		store.removeAll();
	},

	searchFood_panelMyMeals: function() {
		var me = this;

		if(me.getPanelMyMeals().down('#searchfield').getValue() != '') {
			var store = Ext.getStore('ListMyMeals');
			store.setParams(
				{meal: me.getPanelMyMeals().down('#searchfield').getValue(), user: localStorage.getItem('dietofpoints_code_user')}
			);
			store.getProxy().setUrl(Dietofpoints.app.data_server + 'listMyMeals');
			store.removeAll();
			store.load();
		}
		else {
			me.cleanStoreListMyMeals();
		}
	},

	destroyModalErrorWithoutNameCreateMeal: function() {
		var me = this;
		me.getModalErrorWithoutNameCreateMeal().destroy();
	},

	destroyModalErrorCreateMeal: function() {
		var me = this;
		me.getModalErrorCreateMeal().destroy();
	},

	destroyModalCheckFilledFields: function() {
		var me = this;
		me.getModalCheckFilledFields().destroy();
	},

	destroyModalSuccessCreateMeal: function() {
		var me = this;
		me.getModalSuccessCreateMeal().destroy();
	},

	destroyModalErrorDeleteItems: function() {
		var me = this;
		me.getModalErrorDeleteItems().destroy();
	},

	destroyModalConfirmDeleteItems: function() {
		var me = this;
		me.getModalConfirmDeleteItems().destroy();
	},

	destroyModalConfirmDeleteItemsTabpanelMyFoodAndMeals: function() {
		var me = this;
		me.getModalConfirmDeleteItemsTabpanelMyFoodAndMeals().destroy();
	},

	destroyModalOptionsSelectItem: function() {
		var me = this;
		me.getModalOptionsSelectItem().destroy();
	},

	showModalSuccessCreateMeal: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalSuccessCreateMeal');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalAddWeight: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalAddWeight');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalConfirmDeleteItems: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalConfirmDeleteItems');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalConfirmDeleteItemsTabpanelMyFoodAndMeals: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalConfirmDeleteItemsTabpanelMyFoodAndMeals');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorDeleteItems: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorDeleteItems');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalCheckFilledFields: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalCheckFilledFields');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorCreateFood: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorCreateFood');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorCreateMeal: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorCreateMeal');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalOptionsSelectItem: function() {
		var me = this;
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalOptionsSelectItem');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorWithoutNameCreateMeal: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorWithoutNameCreateMeal');
		Ext.Viewport.add(panel);
		panel.show();
	},

	selectAllItems: function() {

		var me = this;
		var elements = document.getElementsByClassName('checkbox');
		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].checked = 1;
		}

		me.destroyModalOptionsSelectItem();
	},

	deleteItemsTabpanelMyFoodAndMeals: function() {
		var me = this;
		me.destroyModalConfirmDeleteItemsTabpanelMyFoodAndMeals();
		var elements = document.getElementsByClassName('checkbox');
		var data = [];

		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {
				data.push(elements[cont].value);
			}
		}

		var currentXtype = me.getXtypeByView(me.getTabpanelMyFoodAndMeals().getActiveItem());
		if(currentXtype === 'listMyFood_tabpanelMyFoodAndMeals') {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'deleteItemsListMyFood',
				async: false,
				params: {
					code_userfood: JSON.stringify(data)
				},
				success: function (conn, response, options, eOpts) {
					me.hideNavButtons();
					me.getNavigationview().down('#menu_navigationview').setHidden(false); 
					me.getNavigationview().down('#addTabpanelMyFoodAndMeals_navigationview').setHidden(false);
					me.starts_listMyFood_tabpanelMyFoodAndMeals();
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'deleteItemsListMyMeals',
				async: false,
				params: {
					code_meal: JSON.stringify(data)
				},
				success: function (conn, response, options, eOpts) {
					me.hideNavButtons();
					me.getNavigationview().down('#menu_navigationview').setHidden(false); 
					me.starts_listMyMeals_tabpanelMyFoodAndMeals();
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
	},

	deleteItems: function() {

		var me = this;
		me.destroyModalConfirmDeleteItems();
		var elements = document.getElementsByClassName('checkbox');
		var data = [];

		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {
				data.push(elements[cont].value);
			}
		}

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'deleteItems',
			async: false,
			params: {
				code_feedhistory: JSON.stringify(data)
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});

		me.showDiaryAfterDeleteItems();
	},

	confirmDeleteItems: function() {

		var me = this;

		if(!me.findCheckedItems()) {
			me.showModalErrorDeleteItems();
		}
		else {
			me.showModalConfirmDeleteItems();
		}
	},

	confirmDeleteItemsTabpanelMyFoodAndMeals: function() {

		var me = this;

		if(!me.findCheckedItems()) {
			me.showModalErrorDeleteItems();
		}
		else {
			me.showModalConfirmDeleteItemsTabpanelMyFoodAndMeals();
		}
	},

	showDiaryAfterDeleteItems: function() {
		var me = this;
		me.showItemsDiary();
		me.setItems_diary();
		me.getNavigationview().getNavigationBar().setTitle(L('panel.template.diary'));
	},

	//Hide items in Diary panel
	hideItemsDiary: function() {
		var me = this;
		me.getPanelDatepicker().setHidden(true);
		me.getPaneldatadiary().setHidden(true);
		me.getPanelOptionsBreakfast().setHidden(true);
		me.getPanelOptionsLunch().setHidden(true);
		me.getPanelOptionsDinner().setHidden(true);
		me.getPanelOptionsSnacks().setHidden(true);
		me.getPanelTitleWater().setHidden(true);
	},

	//Show items in Diary panel
	showItemsDiary: function() {
		var me = this;
		me.getPanelDatepicker().setHidden(false);
		me.getPaneldatadiary().setHidden(false);
		me.getPanelOptionsBreakfast().setHidden(false);
		me.getPanelOptionsLunch().setHidden(false);
		me.getPanelOptionsDinner().setHidden(false);
		me.getPanelOptionsSnacks().setHidden(false);
		me.getPanelTitleWater().setHidden(false);
	},

	editItems: function() {
		var me = this;
		me.hideItemsDiary();
		var elements = document.getElementsByClassName('checkbox');
		//Clean all checkbox
		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].checked = 0;
		}
		me.showCheckbox();
		me.getNavigationview().down('#menu_navigationview').setHidden(true);
		me.getNavigationview().down('#edit_navigationview').setHidden(true);
		me.getNavigationview().down('#close_navigationview').setHidden(false);
		me.getNavigationview().down('#trash_navigationview').setHidden(false);
		me.getNavigationview().down('#options_navigationview').setHidden(false);
		me.getNavigationview().getNavigationBar().setTitle(L('panel.template.select_item')); 
	},

	closeEditItems: function() {
		var me = this;
		me.showItemsDiary();
		me.hideCheckbox();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		me.getNavigationview().down('#edit_navigationview').setHidden(false);
		me.getNavigationview().down('#close_navigationview').setHidden(true);
		me.getNavigationview().down('#trash_navigationview').setHidden(true);
		me.getNavigationview().down('#options_navigationview').setHidden(true);
		me.getNavigationview().getNavigationBar().setTitle(L('panel.template.diary'));
	},

	//Display checkbox in list
	showCheckbox: function() {

		var elements = '';

		elements = document.getElementsByClassName('titleWithoutCheckbox');

		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].style.display = 'none';
		}

	    elements = document.getElementsByClassName('titleWithCheckbox');

	    for(cont = 0; cont < elements.length; cont++) {
			elements[cont].style.display = 'inline';
		}

		elements = document.getElementsByClassName('textWithoutCheckbox');

		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].style.display = 'none';
		}

	    elements = document.getElementsByClassName('textWithCheckbox');

	    for(cont = 0; cont < elements.length; cont++) {
			elements[cont].style.display = 'inline';
		}
	},

	//Hide checkbox in list
	hideCheckbox: function() {

		var elements = '';

		elements = document.getElementsByClassName('titleWithCheckbox');

		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].style.display = 'none';
		}

	    elements = document.getElementsByClassName('titleWithoutCheckbox');

	    for(cont = 0; cont < elements.length; cont++) {
			elements[cont].style.display = 'inline';
		}

		elements = document.getElementsByClassName('textWithCheckbox');

		for(cont = 0; cont < elements.length; cont++) {
			elements[cont].style.display = 'none';
		}

	    elements = document.getElementsByClassName('textWithoutCheckbox');

	    for(cont = 0; cont < elements.length; cont++) {
			elements[cont].style.display = 'inline';
		}
	},

	saveMealMultiplePeriods: function() {

		var me = this;

		var name_meal = me.getPanelSaveMealMultiplePeriods().down('#meal').getValue();

		if(name_meal != '') {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'checkMeal',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					name_meal: name_meal
				},
				success: function (conn, response, options, eOpts) {
					
					var result = Ext.decode(conn.responseText);

					if(result.success === false) {
						me.showModalErrorCreateMeal();
					}
					else {
						me.createMealMultiplePeriods(result.code_meal);
					}
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorWithoutNameCreateMeal();
		}
	},

	saveMeal: function() {

		var me = this;

		var name_meal = me.getPanelSaveMeal().down('#meal').getValue();

		if(name_meal != '') {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'checkMeal',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					name_meal: name_meal
				},
				success: function (conn, response, options, eOpts) {
					
					var result = Ext.decode(conn.responseText);

					if(result.success === false) {
						me.showModalErrorCreateMeal();
					}
					else {
						me.createMeal(result.code_meal);
					}
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorWithoutNameCreateMeal();
		}
	},

	createMeal: function(meal) {

		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: panelSaveMeal', 'bt: save');
		var store = Ext.getStore('ListSaveMeal');

		store.each(function (item, index, length) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'createMeal',
				async: false,
				params: {
					meal: meal,
					name_food: item.get('name_food'),
					amount_food: item.get('amount_food'),
					points_food: item.get('points_food'),
					unit_food: item.get('unit_food')
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		});

		me.showDiaryAfterCreateMeal();
	},

	createMealMultiplePeriods: function(meal) {

		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: panelSaveMeal', 'bt: save');
		var store = Ext.getStore('ListSaveMealMultiplePeriods');

		store.each(function (item, index, length) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'createMeal',
				async: false,
				params: {
					meal: meal,
					name_food: item.get('name_food'),
					amount_food: item.get('amount_food'),
					points_food: item.get('points_food'),
					unit_food: item.get('unit_food')
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		});

		me.showDiaryAfterCreateMeal();
	},

	showDiaryAfterCreateMeal: function() {
		var me = this;
		me.getNavigationview().pop(1);
		me.backButton_navigationview();
		me.showModalSuccessCreateMeal();
	},

	//Checks current ative component in given component
	getXtypeByView: function(view) {
        var xtypesArr = view.getXTypes().split('/');
        return xtypesArr[xtypesArr.length - 1];
    },

	backButton_navigationview: function() {

		var me = this;

		var backStack = me.getNavigationview().getNavigationBar().backButtonStack;
		var currentXtype = me.getXtypeByView(me.getNavigationview().getActiveItem());

		if(currentXtype === 'diary') {
			me.hideNavButtons();
			me.getNavigationview().down('#menu_navigationview').setHidden(false);
		}

		if (currentXtype === 'diary' && me.checkListStores()) {
			me.getNavigationview().down('#edit_navigationview').setHidden(false);
		}

		if(currentXtype === 'tabpanelAddFood') {
			me.hideNavButtons();
			me.getNavigationview().down('#add_navigationview').setHidden(false);
		}

		if(currentXtype === 'panelProfile') {
			me.hideNavButtons();
			me.getNavigationview().down('#menu_navigationview').setHidden(false);
		}

		if(currentXtype === 'tabpanelMyFoodAndMeals') {
			me.hideNavButtons();
			var currentXtype = me.getXtypeByView(me.getTabpanelMyFoodAndMeals().getActiveItem());
			if(currentXtype === 'listMyFood_tabpanelMyFoodAndMeals') {
				me.getNavigationview().down('#menu_navigationview').setHidden(false); 
				me.getNavigationview().down('#addTabpanelMyFoodAndMeals_navigationview').setHidden(false);
				me.starts_listMyFood_tabpanelMyFoodAndMeals();
			}
			else {
				me.getNavigationview().down('#menu_navigationview').setHidden(false); 
				me.starts_listMyMeals_tabpanelMyFoodAndMeals();
			}
		}

		if(currentXtype === 'main') {
			me.hideNavButtons();
			me.getNavigationview().getNavigationBar().setHidden(true);
		}

		if(currentXtype === 'panelIdiom') {
			me.hideNavButtons();
			me.getNavigationview().down('#saveIdiom_navigationview').setHidden(false);
		}

		if(currentXtype === 'panelActivityLevel_user') {
			me.hideNavButtons();
			me.getNavigationview().down('#saveActivityLevelRegister_navigationview').setHidden(false);
		}

		if(currentXtype === 'panelYou') {
			me.hideNavButtons();
			me.getNavigationview().down('#savePanelYou_navigationview').setHidden(false);
		}

		if(currentXtype === 'listFriend') {
			me.hideNavButtons();
			me.getNavigationview().down('#menu_navigationview').setHidden(false);
			me.getNavigationview().down('#addFriend_navigationview').setHidden(false);
		}
	},

	showPanelSaveMeal: function() {

		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: ModalWithMultipleOptions', 'bt: saveMeal');

		me.getModalWithMultipleOptions().destroy();

		me.getNavigationview().down('#menu_navigationview').setHidden(true);
		me.getNavigationview().down('#edit_navigationview').setHidden(true);
		me.getNavigationview().down('#options_navigationview').setHidden(true);
		me.getNavigationview().down('#trash_navigationview').setHidden(true);

		var component = Ext.widget('panelSaveMeal',{
			title: L('panel.template.panelSaveMeal.title')
		});

		me.getNavigationview().push(component);		
	},

	showPanelSaveMealMultiplePeriods: function() {

		var me = this;
		me.getModalOptionsSelectItem().destroy();

		if(!me.findCheckedItems()) {
			me.showModalErrorDeleteItems();
		}
		else {
			me.showItemsDiary();
			me.hideCheckbox();
			me.getNavigationview().down('#close_navigationview').setHidden(true);
			me.getNavigationview().down('#menu_navigationview').setHidden(true);
			me.getNavigationview().down('#edit_navigationview').setHidden(true);
			me.getNavigationview().down('#options_navigationview').setHidden(true);
			me.getNavigationview().down('#trash_navigationview').setHidden(true);

			var component = Ext.widget('panelSaveMealMultiplePeriods',{
				title: L('panel.template.panelSaveMeal.title')
			});

			var elements = document.getElementsByClassName('checkbox');
			var data = [];

			for(cont = 0; cont < elements.length; cont++) {
				if(elements[cont].checked == 1) {
					data.push(elements[cont].value);
				}
			}			

			me.starts_panelSaveMealMultiplePeriods(JSON.stringify(data));

			me.getNavigationview().push(component);	
		}
	},

	starts_panelSaveMeal: function() {

		var me = this;
		var period = '';

		switch(me.period) {
			case 1:
	    		period = 1;
	    	break;
	    	case 2:
	        	period = 2;
	        break;
	    	case 3:
	        	period = 3;
	        break;
	        case 4:
	        	period = 4;
		}

		var store = Ext.getStore('ListSaveMeal');
		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user'), date: me.formatDateToPortugueseFormat(), period: period}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listSaveMeal');
		store.removeAll();
		store.load();
	},

	starts_panelSaveMealMultiplePeriods: function(data) {
		var store = Ext.getStore('ListSaveMealMultiplePeriods');
		store.setParams(
			{code_feedhistory: data}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listSaveMealMultiplePeriods');
		store.removeAll();
		store.load();
	},

	quicklyAdd: function() {

		var me = this;

		var calories = me.getModalQuicklyAdd().down('#calories').getValue();
		me.destroyModalQuicklyAdd();

		if(calories != null) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'quicklyAdd',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					date: me.formatDateToPortugueseFormat(),
					period: me.period,
					points: calories/3.6
				},
				success: function (conn, response, options, eOpts) {
					me.setPeriod();
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorQuicklyAdd();
		}
	},

	//Function to update just the necessary data in diary panel
	setPeriod: function() {
		var me = this;
		var period = me.period;
		var store = '';
		var server = '';
		me.set_panelDataDiary();
		switch(period) {
			case 1:
	    		me.set_panelTitlePeriodBreakfast();
	    		store = Ext.getStore('ListBreakfast');
	    		server = 'listBreakfast';
	    	break;
	    	case 2:
	        	me.set_panelTitlePeriodLunch();
	    		store = Ext.getStore('ListLunch');
	    		server = 'listLunch';
	        break;
	    	case 3:
	        	me.set_panelTitlePeriodDinner();
	    		store = Ext.getStore('ListDinner');
	    		server = 'listDinner';
	        break;
	        case 4:
	        	me.set_panelTitlePeriodSnacks();
	    		store = Ext.getStore('ListSnacks');
	    		server = 'listSnacks';
	    }

		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user'), date: me.formatDateToPortugueseFormat()}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + server);
		store.removeAll();
		store.load(function() {
			if(me.checkListStores()) {
				me.getNavigationview().down('#edit_navigationview').setHidden(false);
			}
			else {
				me.getNavigationview().down('#edit_navigationview').setHidden(true);
			}
		    me.setSizeListsDiary();
		}, this);

	    me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		if(me.checkListStores()) {
			me.getNavigationview().down('#edit_navigationview').setHidden(false);
		}
		me.updateMenuButtonColor('#diary');
	},

	addWeight: function() {

		var me = this;

		var weight = me.getModalAddWeight().down('#weight').getValue();
		me.destroyModalAddWeight();

		if(weight != null) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'addWeight',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					date: me.formatDateToPortugueseFormat(new Date()),
					weight: weight
				},
				success: function (conn, response, options, eOpts) {
					me.recalculateDiaryPoints();
					me.set_panelTopListWeight();
					me.set_panelMenu();
					me.starts_listWeight();
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorAddWeight();
		}
	},

	quicklyAddCalledFromTabpanelAddFood: function() {

		var me = this;

		var calories = me.getModalQuicklyAddCalledFromTabpanelAddFood().down('#calories').getValue();
		me.destroyModalQuicklyAddCalledFromTabpanelAddFood();

		if(calories != null) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'quicklyAdd',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					date: me.formatDateToPortugueseFormat(),
					period: me.period,
					points: calories/3.6
				},
				success: function (conn, response, options, eOpts) {
					me.showDiaryAfterAddFood();
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorQuicklyAddCalledFromTabpanelAddFood();
		}
	},

	//Find checked items in lists
	findCheckedItems: function() {
		var elements = document.getElementsByClassName('checkbox');
		var checked = 0;
		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {
				checked++;
			}
		}
		return checked > 0;
	},

	//Find checked radio
	findRadioChecked: function() {
		var elements = document.getElementsByClassName('radio');
		var checked = 0;
		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {
				checked++;
			}
		}
		return checked > 0;
	},

	showModalCopyMealMultiplePeriods: function() {

		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackView('ModalCopyMealMultiplePeriods');

		me.getModalOptionsSelectItem().destroy();

		if(!me.findCheckedItems()) {
			me.showModalErrorDeleteItems();
		}

		else {
			var actualDate = new Date();
			var option1 = new Date(actualDate.getTime() + (2 * 24 * 60 * 60 * 1000));
			var option5 = new Date(actualDate.getTime() - (2 * 24 * 60 * 60 * 1000));

			var modal = Ext.create('Dietofpoints.view.ModalCopyMealMultiplePeriods');
			modal.down('#option1').setText(me.formatDate(option1));
			modal.down('#option2').setText(L('modal.template.modalCopyMeal.tomorrow'));
			modal.down('#option3').setText(L('modal.template.modalCopyMeal.today'));
			modal.down('#option4').setText(L('modal.template.modalCopyMeal.yesterday'));
			modal.down('#option5').setText(me.formatDate(option5));

			Ext.Viewport.add(modal);
			modal.show();
		}
	},

	showModalCopyMeal: function() {

		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: ModalWithMultipleOptions', 'bt: copyMeal');

		me.getModalWithMultipleOptions().destroy();

		var actualDate = new Date();
		var option1 = new Date(actualDate.getTime() + (2 * 24 * 60 * 60 * 1000));
		var option5 = new Date(actualDate.getTime() - (2 * 24 * 60 * 60 * 1000));

		var modal = Ext.create('Dietofpoints.view.ModalCopyMeal');
		modal.down('#option1').setText(me.formatDate(option1));
		modal.down('#option2').setText(L('modal.template.modalCopyMeal.tomorrow'));
		modal.down('#option3').setText(L('modal.template.modalCopyMeal.today'));
		modal.down('#option4').setText(L('modal.template.modalCopyMeal.yesterday'));
		modal.down('#option5').setText(me.formatDate(option5));

		Ext.Viewport.add(modal);
		modal.show();
	},

	copyMealMultiplePeriods: function(option) {

		var me = this;
		me.getModalCopyMealMultiplePeriods().destroy();
		var dateWithoutFormat = '';
		var dateFormatted = '';
		var actualDate = new Date();

		switch(option) {
			case 1:
	    		dateFormatted = me.formatDateToPortugueseFormat(new Date(actualDate.getTime() + (2 * 24 * 60 * 60 * 1000)));
	    		dateWithoutFormat = new Date(actualDate.getTime() + (2 * 24 * 60 * 60 * 1000));
	    	break;
	    	case 2:
	        	dateFormatted = me.formatDateToPortugueseFormat(new Date(actualDate.getTime() + (1 * 24 * 60 * 60 * 1000)));
	        	dateWithoutFormat = new Date(actualDate.getTime() + (1 * 24 * 60 * 60 * 1000));
	        break;
	    	case 3:
	        	dateFormatted = me.formatDateToPortugueseFormat(actualDate);
	        	dateWithoutFormat = actualDate;
	        break;
	        case 4:
	        	dateFormatted = me.formatDateToPortugueseFormat(new Date(actualDate.getTime() - (1 * 24 * 60 * 60 * 1000)));
	        	dateWithoutFormat = new Date(actualDate.getTime() - (1 * 24 * 60 * 60 * 1000));
	        break;
	        case 5:
	        	dateFormatted = me.formatDateToPortugueseFormat(new Date(actualDate.getTime() - (2 * 24 * 60 * 60 * 1000)));
	        	dateWithoutFormat = new Date(actualDate.getTime() - (2 * 24 * 60 * 60 * 1000));
		}

		var elements = document.getElementsByClassName('checkbox');

		for(cont = 0; cont < elements.length; cont++) {
			if(elements[cont].checked == 1) {

				Ext.Ajax.request({
					url: Dietofpoints.app.data_server + 'copyMealMultiplePeriods',
					async: false,
					params: {
						user: localStorage.getItem('dietofpoints_code_user'),
						date: dateFormatted,
						code_feedhistory: elements[cont].value
					},
					failure: function (conn, response, options, eOpts) {
						me.messageBox_networkError();
					}
				});
			}
		}

		me.showItemsDiary();
		if(dateWithoutFormat.getDate() != me.getDatepicker_diary().getValue().getDate() || dateWithoutFormat.getMonth() != me.getDatepicker_diary().getValue().getMonth() || dateWithoutFormat.getFullYear() != me.getDatepicker_diary().getValue().getFullYear()) {
			me.starts_diary(dateWithoutFormat);
		}
		else {
			me.setItems_diary();
		}
		if(me.checkListStores()) {
			me.getNavigationview().down('#edit_navigationview').setHidden(false);
		}
		else {
			me.getNavigationview().down('#edit_navigationview').setHidden(true);
		}
		me.getNavigationview().down('#close_navigationview').setHidden(true);
		me.getNavigationview().down('#trash_navigationview').setHidden(true);
		me.getNavigationview().down('#options_navigationview').setHidden(true);
		me.getNavigationview().getNavigationBar().setTitle(L('panel.template.diary'));
	},

	copyMeal: function(option) {

		var me = this;
		me.getModalCopyMeal().destroy();
		var store = '';
		var dateWithoutFormat = '';
		var dateFormatted = '';
		var actualDate = new Date();
		
		switch(me.period) {
			case 1:
	    		store = Ext.getStore('ListBreakfast');
	    	break;
	    	case 2:
	        	store = Ext.getStore('ListLunch');
	        break;
	    	case 3:
	        	store = Ext.getStore('ListDinner');
	        break;
	        case 4:
	        	store = Ext.getStore('ListSnacks');
		}

		switch(option) {
			case 1:
	    		dateFormatted = me.formatDateToPortugueseFormat(new Date(actualDate.getTime() + (2 * 24 * 60 * 60 * 1000)));
	    		dateWithoutFormat = new Date(actualDate.getTime() + (2 * 24 * 60 * 60 * 1000));
	    	break;
	    	case 2:
	        	dateFormatted = me.formatDateToPortugueseFormat(new Date(actualDate.getTime() + (1 * 24 * 60 * 60 * 1000)));
	        	dateWithoutFormat = new Date(actualDate.getTime() + (1 * 24 * 60 * 60 * 1000));
	        break;
	    	case 3:
	        	dateFormatted = me.formatDateToPortugueseFormat(actualDate);
	        	dateWithoutFormat = actualDate;
	        break;
	        case 4:
	        	dateFormatted = me.formatDateToPortugueseFormat(new Date(actualDate.getTime() - (1 * 24 * 60 * 60 * 1000)));
	        	dateWithoutFormat = new Date(actualDate.getTime() - (1 * 24 * 60 * 60 * 1000));
	        break;
	        case 5:
	        	dateFormatted = me.formatDateToPortugueseFormat(new Date(actualDate.getTime() - (2 * 24 * 60 * 60 * 1000)));
	        	dateWithoutFormat = new Date(actualDate.getTime() - (2 * 24 * 60 * 60 * 1000));
		}

		store.each(function (item, index, length) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'copyMeal',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					date: dateFormatted,
					period: me.period,
					name_food: item.get('name_food'),
					amount_food: item.get('amount_food'),
					points_food: item.get('points_food'),
					unit_food: item.get('unit_food'),
					repeat_portion: item.get('repeat_portion')
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		});

		if(dateWithoutFormat.getDate() != me.getDatepicker_diary().getValue().getDate() || dateWithoutFormat.getMonth() != me.getDatepicker_diary().getValue().getMonth() || dateWithoutFormat.getFullYear() != me.getDatepicker_diary().getValue().getFullYear()) {
			me.starts_diary(dateWithoutFormat);
		}
		else {
			me.setPeriod();
		}
	},

	formatDate: function(date) {
		
		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();
		var result = '';
		
		if(day < 10) {
			day = '0' + day;
		}
		
		if(month < 10) {
			month = '0' + month;
		}

		if(L('dateFormat') === 'd/m/Y') {
			result = day + '/' + month + '/' + year;
		}
		else {
			result = month + '/' + day + '/' + year;
		}

		return result;
    },

    formatDateToPortugueseFormat: function(data) {
    	var me = this;
    	var date = '';

    	//If received a date in param of function
		if(data instanceof Date && !isNaN(data.valueOf())) {
			date = data;
		}
		else {
			date = me.getDatepicker_diary().getValue();
		}

    	var day = date.getDate();
    	var month = date.getMonth() + 1;
    	var year = date.getFullYear();

    	if(day < 10) {
			day = '0' + day;
		}
		
		if(month < 10) {
			month = '0' + month;
		}

    	return day + '/' + month + '/' + year;
    },

	destroyModalQuicklyAdd: function() {

		var me = this;
		me.getModalQuicklyAdd().destroy();
	},

	destroyModalAddWeight: function() {

		var me = this;
		me.getModalAddWeight().destroy();
	},

	destroyModalErrorCreateFood: function() {

		var me = this;
		me.getModalErrorCreateFood().destroy();
	},

	destroyModalQuicklyAddCalledFromTabpanelAddFood: function() {

		var me = this;
		me.getModalQuicklyAddCalledFromTabpanelAddFood().destroy();
	},

	//Increase date
	increaseDate: function() {

		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: increaseDate');

		var currentDate = me.getDatepicker_diary().getValue();
		var newDate = new Date(currentDate.getTime() + (1 * 24 * 60 * 60 * 1000));
		me.starts_diary(newDate);
	},

	//Decrease date
	decreaseDate: function() {

		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: decreaseDate');

		var currentDate = me.getDatepicker_diary().getValue();
		var newDate = new Date(currentDate.getTime() - (1 * 24 * 60 * 60 * 1000));
		me.starts_diary(newDate);
	},

	newDateSelected: function(datepicker, value, eOpts) {
		var me = this;
		me.setItems_diary();
	},

	//Capture total points consumed by a certain date 
	get_food: function() {

		var me = this;
		var result = '';

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'amountFood',
			async: false,
			params: {
				user: localStorage.getItem('dietofpoints_code_user'),
				date: me.formatDateToPortugueseFormat()
			},
			success: function (conn, response, options, eOpts) {
				result = Ext.decode(conn.responseText);
			},
			//Caso não tenha conseguido fazer a requisição
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});

		return result;
	},

	//Calculate points consumed in a given period of the day
	get_amountPerPeriod: function(period) {

		var me = this;
		var result = '';

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'amountPerPeriod',
			async: false,
			params: {
				user: localStorage.getItem('dietofpoints_code_user'),
				date: me.formatDateToPortugueseFormat(),
				period: period
			},
			success: function (conn, response, options, eOpts) {
				result = Ext.decode(conn.responseText);
			},
			//Caso não tenha conseguido fazer a requisição
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});

		return result;
	},

	showPanelAddFood: function(period) {

		var me = this;
		me.period = period;
		var result = '';

		switch(me.period) {
			case 1:
	    		result = L('panel.template.panelTitlePeriodBreakfast');
	    	break;
	    	case 2:
	        	result = L('panel.template.panelTitlePeriodLunch');
	        break;
	    	case 3:
	        	result = L('panel.template.panelTitlePeriodDinner');
	        break;
	        case 4:
	        	result = L('panel.template.panelTitlePeriodSnacks');
		}

		me.getNavigationview().down('#menu_navigationview').setHidden(true);
		me.getNavigationview().down('#edit_navigationview').setHidden(true);
		me.getNavigationview().down('#close_navigationview').setHidden(true);
		me.getNavigationview().down('#trash_navigationview').setHidden(true);
		me.getNavigationview().down('#options_navigationview').setHidden(true);
		me.getNavigationview().down('#add_navigationview').setHidden(false);

		var component = Ext.widget('tabpanelAddFood',{
			title: result
		});
		me.getNavigationview().push(component);
	},

	showModalOption_panelOptions_diary: function(period) {

		var me = this;
		me.period = period;

		if(me.checkSpecificStore() > 0) {
			me.showModalWithMultipleOptions();
		}
		else {
			me.showModalWithSingleOption();
		}
	},

	checkSpecificStore: function() {

		var me = this;
		var result = '';

		switch(me.period) {
			case 1:
	    		result = Ext.getStore('ListBreakfast').getCount();
	    	break;
	    	case 2:
	        	result = Ext.getStore('ListLunch').getCount();
	        break;
	    	case 3:
	        	result = Ext.getStore('ListDinner').getCount();
	        break;
	        case 4:
	        	result = Ext.getStore('ListSnacks').getCount();
		}

		return result;
	},

	showPanelUpdateUserFood: function(code) {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: tabpanelMyFoodAndMeals', 'bt: updateUserFood');

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'showPanelUpdateUserFood',
			async: false,
			params: {
				code_userfood: code
			},
			success: function (conn, response, options, eOpts) {
				var result = Ext.decode(conn.responseText);
				var component = Ext.widget('panelUpdateUserFood',{
					title: L('panel.template.panelUpdateUserFood.title')
				});
				component.down('#name').setValue(result.name);
				component.down('#amount').setValue(result.amount);
				component.down('#portions').setValue(result.portions);
				component.down('#code').setValue(result.code);
				me.hideNavButtons();
				me.getNavigationview().down('#updateUserFood_navigationview').setHidden(false);
				me.getNavigationview().push(component);
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});
	},

	showPanelUpdatePortion: function(code) {
		
		var me = this;

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'showPanelUpdatePortion',
			async: false,
			params: {
				code_feedhistory: code
			},
			success: function (conn, response, options, eOpts) {
				var result = Ext.decode(conn.responseText);
				if(result.name === 'Quickly add') {
					Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: updateCalories');
					var component = Ext.widget('panelUpdateCalories',{
						title: L('panel.template.panelUpdateCalories.title')
					});
					component.down('#name').setValue(result.name);
					component.down('#code').setValue(code);
					me.hideNavButtons();
					me.getNavigationview().down('#saveUpdateCalories_navigationview').setHidden(false);
					me.getNavigationview().push(component);
				}
				else {
					Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: updatePortion');
					var component = Ext.widget('panelUpdatePortion',{
						title: L('panel.template.panelUpdatePortion.title')
					});
					component.down('#name').setValue(result.name);
					component.down('#amount').setValue(result.portion);
					component.down('#code').setValue(code);
					me.hideNavButtons();
					me.getNavigationview().down('#saveUpdatePortion_navigationview').setHidden(false);
					me.getNavigationview().push(component);
				}
			},
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});		
	},

	addWater: function() {

		var me = this;

		var cups = me.getModalWater().down('#cups').getValue();
		me.destroyModalWater();

		if(cups != null) {

			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'addWater',
				async: false,
				params: {
					user: localStorage.getItem('dietofpoints_code_user'),
					date: me.formatDateToPortugueseFormat(),
					cups: cups
				},
				success: function (conn, response, options, eOpts) {
					me.set_panelTitleWater();
				},
				//Caso não tenha conseguido fazer a requisição
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
		else {
			me.showModalErrorWater();
		}
	},

	showModalWater: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: diary', 'bt: showModalWater');
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalWater');
		Ext.Viewport.add(panel);
		panel.show();	
	},

	updatePortion: function() {
		var me = this;
		var portions = me.getPanelUpdatePortion().down('#portions').getValue();
		var code = me.getPanelUpdatePortion().down('#code').getValue();
		if(portions === null || portions == 0) {
			me.showModalCheckFilledFields();
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'updatePortion',
				async: false,
				params: {
					code_feedhistory: code,
					portions: portions
				},
				success: function (conn, response, options, eOpts) {
					me.showDiaryAfterAddFood();
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
	},

	updateCalories: function() {
		var me = this;
		var calories = me.getPanelUpdateCalories().down('#calories').getValue();
		var code = me.getPanelUpdateCalories().down('#code').getValue();
		if(calories === null || calories == 0) {
			me.showModalCheckFilledFields();
		}
		else {
			Ext.Ajax.request({
				url: Dietofpoints.app.data_server + 'updateCalories',
				async: false,
				params: {
					code_feedhistory: code,
					calories: calories
				},
				success: function (conn, response, options, eOpts) {
					me.showDiaryAfterAddFood();
				},
				failure: function (conn, response, options, eOpts) {
					me.messageBox_networkError();
				}
			});
		}
	},

	closeAnotherModals: function() {
		var me = this;
		if(me.getModalWithSingleOption()) {
			me.getModalWithSingleOption().destroy();
		}
		if(me.getModalWithMultipleOptions()) {
			me.getModalWithMultipleOptions().destroy();
		}
		if(me.getModalErrorQuicklyAdd()) {
			me.getModalErrorQuicklyAdd().destroy();
		}
		if(me.getModalErrorQuicklyAddCalledFromTabpanelAddFood()) {
			me.getModalErrorQuicklyAddCalledFromTabpanelAddFood().destroy();
		}
		if(me.getModalErrorAddWeight()) {
			me.getModalErrorAddWeight().destroy();
		}
		if(me.getModalErrorActualWeight()) {
			me.getModalErrorActualWeight().destroy();
		}
		if(me.getModalErrorGoalWeight()) {
			me.getModalErrorGoalWeight().destroy();
		}
		if(me.getModalErrorHeight()) {
			me.getModalErrorHeight().destroy();
		}
		if(me.getModalErrorAge()) {
			me.getModalErrorAge().destroy();
		}
		if(me.getModalErrorWater()) {
			me.getModalErrorWater().destroy();
		}
		if(me.getModalWithSingleOption()) {
			me.getModalWithSingleOption().destroy();
		}
		if(me.getModalSuccessCreateMeal()) {
			me.getModalSuccessCreateMeal().destroy();
		}
		if(me.getModalConfirmDeleteItems()) {
			me.getModalConfirmDeleteItems().destroy();
		}
		if(me.getModalCheckFilledFields()) {
			me.getModalCheckFilledFields().destroy();
		}
		if(me.getModalOptionsSelectItem()) {
			me.getModalOptionsSelectItem().destroy();
		}
		if(me.getModalConfirmDeleteItemsTabpanelMyFoodAndMeals()) {
			me.getModalConfirmDeleteItemsTabpanelMyFoodAndMeals().destroy();
		}
		if(me.getModalAddMeal()) {
			me.getModalAddMeal().destroy();
		}
		if(me.getModalErrorLogin()) {
			me.getModalErrorLogin().destroy();
		}
		if(me.getModalErrorEmailRegister()) {
			me.getModalErrorEmailRegister().destroy();
		}
		if(me.getModalConfirmLogout()) {
			me.getModalConfirmLogout().destroy();
		}
		if(me.getModalChangeLanguage()) {
			me.getModalChangeLanguage().destroy();
		}
		if(me.getModalBadRate()) {
			me.getModalBadRate().destroy();
		}
		if(me.getModalErrorChooseRate()) {
			me.getModalErrorChooseRate().destroy();
		}
		if(me.getModalDeleteFriend()) {
			me.getModalDeleteFriend().destroy();
		}
		if(me.getModalErrorSendFriendRequest()) {
			me.getModalErrorSendFriendRequest().destroy();
		}
		if(me.getModalSuccessSendFriendRequest()) {
			me.getModalSuccessSendFriendRequest().destroy();
		}
		if(me.getModalAlreadyFriends()) {
			me.getModalAlreadyFriends().destroy();
		}
		if(me.getModalOpenFriendRequestSent()) {
			me.getModalOpenFriendRequestSent().destroy();
		}
		if(me.getModalOpenFriendRequestReceived()) {
			me.getModalOpenFriendRequestReceived().destroy();
		}
	},

	showModalQuicklyAdd: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackView('ModalQuicklyAdd');
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalQuicklyAdd');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorQuicklyAdd: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorQuicklyAdd');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorWater: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorWater');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorAddWeight: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorAddWeight');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalErrorQuicklyAddCalledFromTabpanelAddFood: function() {
		var panel = Ext.create('Dietofpoints.view.ModalErrorQuicklyAddCalledFromTabpanelAddFood');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalQuicklyAddCalledFromTabpanelAddFood: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackView('ModalQuicklyAddCalledFromTabpanelAddFood');
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalQuicklyAddCalledFromTabpanelAddFood');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalWithSingleOption: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackView('ModalWithSingleOption');
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalWithSingleOption');
		Ext.Viewport.add(panel);
		panel.show();
	},

	showModalWithMultipleOptions: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackView('ModalWithMultipleOptions');
		me.closeAnotherModals();
		var panel = Ext.create('Dietofpoints.view.ModalWithMultipleOptions');
		Ext.Viewport.add(panel);
		panel.show();
	},

	//Function to show diary panel
	starts_diary: function(date) {
		var me = this;
		//Set date in Datepicker
		if(date instanceof Date && !isNaN(date.valueOf())) {
			me.getDatepicker_diary().setValue(date);
		}
		else {
			me.getDatepicker_diary().setValue(new Date());
		}
	},

	confirmLogout: function() {
		var me = this;
		me.hideMenu();
		me.showModalConfirmLogout();
	},

	logout: function() {
		var me = this;
		Dietofpoints.app.getController('GoogleAnalytics').trackEvent('view: modalConfirmLogout', 'bt: logout');
		me.destroyModalConfirmLogout();
		me.getMenu().destroy();
		me.hideNavButtons();
		localStorage.removeItem('dietofpoints_code_user');
		localStorage.removeItem('dietofpoints_goal_user');
		localStorage.removeItem('dietofpoints_name_user');
		me.getNavigationview().getNavigationBar().setHidden(true);
		me.destroyComponents();
		var component = Ext.widget('main');
		me.getNavigationview().push(component);
	},

	setItems_diary: function() {
		var me = this;
		//Set data in panel
		me.set_panelDataDiary();
		//Set data in panels
		me.set_panelTitlePeriodBreakfast();
		me.set_panelTitlePeriodLunch();
		me.set_panelTitlePeriodDinner();
		me.set_panelTitlePeriodSnacks();
		me.set_panelTitleWater();
		//Set data in panels
		me.set_panelOptionsBreakfast();
		me.set_panelOptionsLunch();
		me.set_panelOptionsDinner();
		me.set_panelOptionsSnacks();
		//Load store of lists
		me.load_listBreakfast();
		me.load_listLunch();
		me.load_listDinner();
		me.load_listSnacks();
		//Set buttons in Navigationbar
		me.hideNavButtons();
		me.getNavigationview().down('#menu_navigationview').setHidden(false);
		if(me.checkListStores()) {
			me.getNavigationview().down('#edit_navigationview').setHidden(false);
		}
		me.updateMenuButtonColor('#diary');
	},

	starts_listWeight: function() {
		var me = this;
		var store = Ext.getStore('ListWeight');
		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user')}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listWeight');
		store.removeAll();
		store.load();
	},

	checkListStores: function() {
		if(Ext.getStore('ListBreakfast').getCount() > 0 || Ext.getStore('ListLunch').getCount() > 0 || Ext.getStore('ListDinner').getCount() > 0 || Ext.getStore('ListSnacks').getCount() > 0) {
			return true;
		}
		else {
			return false;
		}
	},

	setSizeListsDiary: function() {
		var me = this;
		me.getListBreakfast().setHeight(me.getListBreakfast().getStore().getCount() * Dietofpoints.app.sizeOfList + 'px');
		me.getListLunch().setHeight(me.getListLunch().getStore().getCount() * Dietofpoints.app.sizeOfList + 'px');
		me.getListDinner().setHeight(me.getListDinner().getStore().getCount() * Dietofpoints.app.sizeOfList + 'px');
		me.getListSnacks().setHeight(me.getListSnacks().getStore().getCount() * Dietofpoints.app.sizeOfList + 'px');
	},

	load_listBreakfast: function() {

		var me = this;
		var store = Ext.getStore('ListBreakfast');

		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user'), date: me.formatDateToPortugueseFormat()}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listBreakfast');
		store.removeAll();
		store.load();
	},

	load_listFriend: function() {

		var me = this;
		var store = Ext.getStore('ListFriend');

		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user')}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listFriend');
		store.removeAll();
		store.load();
	},

	load_listLunch: function() {

		var me = this;
		var store = Ext.getStore('ListLunch');

		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user'), date: me.formatDateToPortugueseFormat()}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listLunch');
		store.removeAll();
		store.load();
	},

	load_listDinner: function() {

		var me = this;
		var store = Ext.getStore('ListDinner');

		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user'), date: me.formatDateToPortugueseFormat()}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listDinner');
		store.removeAll();
		store.load();
	},

	load_listSnacks: function() {

		var me = this;
		var store = Ext.getStore('ListSnacks');
		store.setParams(
			{user: localStorage.getItem('dietofpoints_code_user'), date: me.formatDateToPortugueseFormat()}
		);
		store.getProxy().setUrl(Dietofpoints.app.data_server + 'listSnacks');
		store.removeAll();
		store.load(function() {
			if(me.checkListStores()) {
				me.getNavigationview().down('#edit_navigationview').setHidden(false);
			}
			else {
				me.getNavigationview().down('#edit_navigationview').setHidden(true);
			}
		    me.setSizeListsDiary();
		}, this);
	},

	set_panelOptionsBreakfast: function() { 
		var me = this;
        me.getPanelOptionsBreakfast().setHeight(Dietofpoints.app.sizeOfList);
	},

	set_panelOptionsLunch: function() {
		var me = this;
        me.getPanelOptionsLunch().setHeight(Dietofpoints.app.sizeOfList);
	},

	set_panelOptionsDinner: function() {
		var me = this;
        me.getPanelOptionsDinner().setHeight(Dietofpoints.app.sizeOfList);
	},

	set_panelOptionsSnacks: function() {
		var me = this;
        me.getPanelOptionsSnacks().setHeight(Dietofpoints.app.sizeOfList);
	},

	set_panelTitlePeriodBreakfast: function() {

		var me = this;

		var amount = me.get_amountPerPeriod(1);

        me.getPanelTitlePeriodBreakfast().setData(
            {
                amount: amount
            }
        );
	},

	set_panelTitlePeriodLunch: function() {

		var me = this;

		var amount = me.get_amountPerPeriod(2);

        me.getPanelTitlePeriodLunch().setData(
            {
                amount: amount
            }
        );
	},

	set_panelTitlePeriodDinner: function() {

		var me = this;

		var amount = me.get_amountPerPeriod(3);

        me.getPanelTitlePeriodDinner().setData(
            {
                amount: amount
            }
        );
	},

	set_panelTitlePeriodSnacks: function() {

		var me = this;

		var amount = me.get_amountPerPeriod(4);

        me.getPanelTitlePeriodSnacks().setData(
            {
                amount: amount
            }
        );
	},

	set_panelTitleWater: function() {

		var me = this;

		var amount = me.get_numberOfCupsOfWater();

        me.getPanelTitleWater().setData(
            {
                amount: amount
            }
        );
	},

	set_panelDataDiary: function() {

		var me = this;

        var goal = localStorage.getItem('dietofpoints_goal_user');
        var food = me.get_food();
        var pointsbalance = goal - food;

        me.getPaneldatadiary().setData(
            {
                goal: goal, 
                food: food, 
                pointsbalance: pointsbalance
            }
        );
	},

	get_numberOfCupsOfWater: function(period) {

		var me = this;
		var result = '';

		Ext.Ajax.request({
			url: Dietofpoints.app.data_server + 'get_numberOfCupsOfWater',
			async: false,
			params: {
				user: localStorage.getItem('dietofpoints_code_user'),
				date: me.formatDateToPortugueseFormat()
			},
			success: function (conn, response, options, eOpts) {
				result = Ext.decode(conn.responseText);
			},
			//Caso não tenha conseguido fazer a requisição
			failure: function (conn, response, options, eOpts) {
				me.messageBox_networkError();
			}
		});

		return result;
	},

	//Starts component Navigationview
	starts_navigationview: function() {
		var me = this;
		if(!me.checkIdiom()) {
			me.getNavigationview().down('#saveIdiom_navigationview').setHidden(false);
			me.getNavigationview().getNavigationBar().setHidden(false);
			var component = Ext.widget('panelIdiom',{
				title: L('panel.template.panelIdiom.title')
			});
			me.getNavigationview().push(component);			
		}
		else {
			//Checks for user data in localStorage
	        if(localStorage.getItem('dietofpoints_code_user')) {
				me.startsMenu();
				me.set_panelMenu();
	        	me.updateMenuButtonColor('#diary');
				me.destroyComponents();
				var component = Ext.widget('diary',{
					title: L('panel.template.diary')
				});
				me.getNavigationview().push(component);
	        }
			else {
				me.getNavigationview().getNavigationBar().setHidden(true);
				me.destroyComponents();
				var component = Ext.widget('main');
				me.getNavigationview().push(component);
			}
		}
	},

	//Remove all components of Navigationview, including back buttons
    destroyComponents: function() {
    	var me = this;
        var backButtonStack;
        //Array of back buttons
        backButtonStack = me.getNavigationview().getNavigationBar().backButtonStack;
        //While exist inner items in navigation view you remove the first index of navigation view
        while (me.getNavigationview().getInnerItems().length > 0) {
            me.getNavigationview().removeInnerAt(0);
        }
        //Removing from backButtonStack array in index 0 the total length of backButtonStack array
        Ext.Array.erase(backButtonStack, 0, backButtonStack.length);
    }
});