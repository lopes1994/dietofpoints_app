Ext.define("Dietofpoints.view.ModalWithMultipleOptions", {

	extend: 'Ext.Panel',
	
	xtype: 'modalWithMultipleOptions',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'button',
                cls: 'buttonModal',
                text: L('modal.template.modalWithMultipleOptions.option1'),
                itemId: 'option1'
            },
            {
                xtype: 'button',
                cls: 'buttonModal',
                text: L('modal.template.modalWithMultipleOptions.option2'),
                itemId: 'option2'
            },
            {
                xtype: 'button',
                cls: 'buttonModal',
                text: L('modal.template.modalWithMultipleOptions.option3'),
                itemId: 'option3'
            }
        ]
    }
});