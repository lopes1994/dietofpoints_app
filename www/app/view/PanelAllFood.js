Ext.define("Dietofpoints.view.PanelAllFood", {

	extend: 'Ext.Panel',
	
	xtype: 'panelAllFood',

	config: {

        scrollable: false,
        title: L('panel.template.panelAllFood.title'),

        layout: {
            type: 'vbox'
        },

        items: [
            {
                xtype: 'panel',
                layout: 'hbox',
                docked: 'top',
                items: [
                    {
                        xtype: 'searchfield',
                        itemId: 'searchfield',
                        cls: 'searchfield',
                        placeHolder: L('panel.template.panelAllFood.searchfield'),
                        flex: 1,
                        listeners: [
                            {
                                fn: function(element, eOpts) {
                                    element.down('input').dom.focus();
                                },
                                event: 'painted'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: L('panel.template.panelAllFood.search'),
                        cls: 'buttonSearch',
                        itemId: 'search'
                    }
                ]
            },
            {
                xtype: 'listAllFood'
            }
        ]
	}
});