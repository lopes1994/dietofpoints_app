Ext.define("Dietofpoints.store.ListFriendRequest", {

	extend: 'Ext.data.Store',

	config: {
		model: 'Dietofpoints.model.ListFriendRequest'
	}
});