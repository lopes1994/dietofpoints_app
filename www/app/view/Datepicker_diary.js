Ext.define("Dietofpoints.view.Datepicker_diary", {

	extend: 'Ext.field.DatePicker',
	
	xtype: 'datepicker_diary',

	config: {
	   
        dateFormat: L('dateFormat'),
        picker: {
            useTitles: true,
            cls: 'datepicker',
            listeners: {
                initialize: function (elem) {
                    if (Ext.os.is.Android) {
                        elem.setShowAnimation(null);
                        elem.setHideAnimation(null);
                    }
                }
            }
        }
	}
});