Ext.define("Dietofpoints.view.PanelTitlePeriodLunch", {

	extend: 'Ext.Panel',

	xtype: 'panelTitlePeriodLunch',

	config: {

		cls: 'panelTitlePeriod',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="titlePeriod_diary">' + L('panel.template.panelTitlePeriodLunch') + '</div>';
                    str += '<div class="amountPeriod_diary">' + values.amount + '</div>';
                    return str;
                }
            }
        )
	}
});
