Ext.define("Dietofpoints.view.PanelOptionsBreakfast", {

	extend: 'Ext.Panel',
	
	xtype: 'panelOptionsBreakfast',

	config: {
	
		cls: 'panelOptions_diary',
        height: null,

        items: [
            {
                xtype: 'button',
                itemId: 'addFood_period1',
                text: L('panel.template.panelOptionsDiary.addFood'),
                cls: 'addItem_panelOptions_diary',
                iconCls: 'fa fa-plus fa-fw'
            },
            {
                xtype: 'button',
                itemId: 'moreOptions_period1',
                text: L('panel.template.panelOptionsDiary.more'),
                cls: 'moreOptions_panelOptions_diary',
                iconCls: 'fa fa-ellipsis-h fa-fw'
            }
        ]
	}
});