Ext.define("Dietofpoints.view.ModalErrorEmailRegister", {

	extend: 'Ext.Panel',
	
	xtype: 'modalErrorEmailRegister',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalErrorCreateFood.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalErrorEmailRegister.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalErrorDeleteItems.close'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'close'
                    }
                ]
            }
        ]
    }
});