Ext.define("Dietofpoints.view.PanelHeight", {

	extend: 'Ext.Panel',
	
	xtype: 'panelHeight',

	config: {

        cls: 'panelProfile',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="panelProfile_textLeft">' + L('panel.template.panelHeight.title') + '</div>';
                    str += '<div class="panelProfile_textRight">' + values.text + ' cm' + '</div>';
                    return str;
                }
            }
        )
	},

    initialize: function() {

        var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showModalHeight,
            delegate: ['.panelProfile_textRight'],
            scope: me
        });
    },

    showModalHeight: function() {
        Dietofpoints.app.getController('Diary').showModalHeight();
    }
});