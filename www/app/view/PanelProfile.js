Ext.define("Dietofpoints.view.PanelProfile", {

	extend: 'Ext.Panel',
	
	xtype: 'panelProfile',

	config: {

        layout: 'vbox',
        scrollable: 'vertical',

        items: [
            {
                xtype: 'panelInitialWeight'
            },
            {
                xtype: 'panelActualWeight'
            },
            {
                xtype: 'panelGoalWeight'
            },
            {
                xtype: 'panelActivityLevel'
            },
            {
                xtype: 'panelHeight'
            },
            {
                xtype: 'panelAge'
            }
        ]
	}
});