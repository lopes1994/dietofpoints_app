Ext.define("Dietofpoints.view.PanelAccessData", {

	extend: 'Ext.Panel',
	
	xtype: 'panelAccessData',

	config: {

        layout: 'vbox',
        scrollable: 'vertical',

        items: [
            {
                xtype: 'textfield', 
                itemId: 'email',
                placeHolder: L('panel.template.panelAccessData.email'),
                maxLength: 60,
                cls: 'customInput',
                listeners: [
                    {
                        fn: function(element, eOpts) {
                            element.down('input').dom.focus();
                        },
                        event: 'painted'
                    }
                ]
            },
            {
                xtype: 'passwordfield',
                itemId: 'password',
                placeHolder: L('panel.template.panelAccessData.password'),
                maxLength: 32,
                cls: 'customInput'
            }
        ]
	}
});