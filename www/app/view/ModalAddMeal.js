Ext.define("Dietofpoints.view.ModalAddMeal", {

	extend: 'Ext.Panel',
	
	xtype: 'modalAddMeal',

	config: {
	
        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="title_modalCopyMeal"> ' + values.title + '</div>';
                    str += '<div id="option1" class="option_modalCopyMeal"> ' + values.breakfast + '</div>';
                    str += '<div id="option2" class="option_modalCopyMeal"> ' + values.lunch + '</div>';
                    str += '<div id="option3" class="option_modalCopyMeal"> ' + values.dinner + '</div>';
                    str += '<div id="option4" class="option_modalCopyMeal"> ' + values.snacks + '</div>';
                    return str;
                }
            }
        )
	},

    initialize: function() {

        var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.addMeal1,
            delegate: ['#option1'],
            scope: me
        });

        listElement.on({
            tap: me.addMeal2,
            delegate: ['#option2'],
            scope: me
        });

        listElement.on({
            tap: me.addMeal3,
            delegate: ['#option3'],
            scope: me
        });

        listElement.on({
            tap: me.addMeal4,
            delegate: ['#option4'],
            scope: me
        });
    },

    addMeal1: function() {
        Dietofpoints.app.getController('Diary').addMeal(1);
    },

    addMeal2: function() {
        Dietofpoints.app.getController('Diary').addMeal(2);
    },

    addMeal3: function() {
        Dietofpoints.app.getController('Diary').addMeal(3);
    },

    addMeal4: function() {
        Dietofpoints.app.getController('Diary').addMeal(4);
    }
});