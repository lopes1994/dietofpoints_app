Ext.define("Dietofpoints.view.PanelTitlePeriodDinner", {

	extend: 'Ext.Panel',
	
	xtype: 'panelTitlePeriodDinner',

	config: {
	
		cls: 'panelTitlePeriod',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="titlePeriod_diary">' + L('panel.template.panelTitlePeriodDinner') + '</div>';
                    str += '<div class="amountPeriod_diary">' + values.amount + '</div>';
                    return str;
                }
            }
        )
	}
});