Ext.define("Dietofpoints.store.ListSnacks", {

	extend: 'Ext.data.Store',

	config: {
		model: 'Dietofpoints.model.ListSnacks'
	}
});