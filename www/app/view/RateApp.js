Ext.define('Dietofpoints.view.RateApp', {
    extend: 'Ext.form.Panel',
    xtype: 'rateApp',
    config: {
        width: 280,
        height: 280,
        centered: true,
        modal: true,
        hideOnMaskTap: false,
        hidden: true,
        showAnimation: Ext.os.is.Android ? false : {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: Ext.os.is.Android ? false : {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        scrollable: false,
        layout: 'vbox'
    },
    initialize: function () {
        var me = this;
        if(+localStorage.getItem('startsCount') > 5 && !localStorage.getItem('doneRate')) {
            me.launchRate();
        }
        me.add([{
            xtype: 'panel',
            html: L('rate.title'),
            cls: 'titulo_rateApp'
        }, {
            xtype: 'panel',
            layout: 'hbox',
            cls: 'rate-panel',
            flex: 1,
            items: [{
                xtype: 'spacer',
                flex: 1
            },{
                xtype: 'panel',
                cls: 'rate-1',
                listeners: {
                    initialize: me.initRatePanel,
                    scope: me
                }
            }, {
                xtype: 'panel',
                cls: 'rate-2',
                listeners: {
                    initialize: me.initRatePanel,
                    scope: me
                }
            }, {
                xtype: 'panel',
                cls: 'rate-3',
                listeners: {
                    initialize: me.initRatePanel,
                    scope: me
                }
            }, {
                xtype: 'panel',
                cls: 'rate-4',
                listeners: {
                    initialize: me.initRatePanel,
                    scope: me
                }
            }, {
                xtype: 'panel',
                cls: 'rate-5',
                listeners: {
                    initialize: me.initRatePanel,
                    scope: me
                }
            },{
                xtype: 'spacer',
                flex: 1
            }]
        }, {
            xtype: 'button',
            html: L('rate.buttons.3'),
            cls: 'buttonRate',
            listeners: {
                tap: me.checkRate,
                scope: me
            }
        }, {
            xtype: 'button',
            html: L('rate.buttons.1'),
            cls: 'buttonRate',
            listeners: {
                tap: me.doNotDisturb,
                scope: me
            }
        }, {
            xtype: 'button',
            html: L('rate.buttons.2'),
            cls: 'buttonRate',
            listeners: {
                tap: me.later,
                scope: me
            }
        }]);

        me.callParent(arguments);
    },
    timer: null,
    launchRate: function () {
        var me = this;
        me.clearInterval();
        me.timer = setInterval(Ext.bind(me.showRate, me), 1000*5);
    },
    showRate: function () {
        var me = this;
        if(me.checkNoModal()) {
            me.clearInterval();
            Dietofpoints.app.getController('GoogleAnalytics').trackEvent('reateApp', 'show');
            me.show();
        } else {
            me.launchRate();
        }
    },
    clearInterval: function(){
        var me = this;
        if(me.timer) {
            clearInterval(me.timer);
        }
    },
    checkNoModal: function () {
        var me = this,
            modalWindows = Ext.ComponentQuery.query('[modal][hidden=false]'),
            menu = Dietofpoints.app.getController('Diary').getMenu();

        if (!menu.isHidden()) {
            return false;
        } else if (modalWindows.length !== 0) {
            return false;
        } else {
            return true;
        }
    },
    initRatePanel: function (pnl) {
        var me = this;
        pnl.element.on('tap', Ext.bind(me.redrawRate, me, [pnl]));
    },
    redrawRate: function (pnl) {
        var me = this,
            rate = pnl.getCls()[0].split('rate-')[1];
        for(var i=1; i <= 5;i++) {
            pnl.up().getInnerAt(i).removeCls('active');
        }
        for(var i=1; i <= rate;i++) {
            pnl.up().getInnerAt(i).addCls('active');
        }
        me.rate = rate;
    },
    checkRate: function () {
        var me = this;
        if(me.rate) {
            if(me.rate >=4) {
                Dietofpoints.app.getController('GoogleAnalytics').trackEvent('reateApp', 'good', me.rate);
                if(Ext.os.is.ios) {
                    window.open('itms-apps://itunes.apple.com/us/app/freeshare/id1078009403?ls=1&mt=8', '_system');
                } else {
                    window.open(Dietofpoints.app.market, '_system');
                }
            } else {
                Dietofpoints.app.getController('GoogleAnalytics').trackEvent('reateApp', 'bad', me.rate);
                localStorage.setItem('doNotDisturb', new Date().getTime());
                Ext.defer(function () {
                    Dietofpoints.app.getController('Diary').showModalBadRate();
                }, 350);
            }
            localStorage.setItem('doneRate', true);
            me.hide();
        } else {
            Dietofpoints.app.getController('Diary').showModalErrorChooseRate();
        }
    },
    doNotDisturb: function () {
        var me = this;
        localStorage.setItem('doneRate', true);
        Dietofpoints.app.getController('GoogleAnalytics').trackEvent('reateApp', 'doNotDisturb');
        localStorage.setItem('doNotDisturb', new Date().getTime());
        Ext.defer(function () {
            me.hide();
        }, 300);
    },
    later: function () {
        var me = this;
        Dietofpoints.app.getController('GoogleAnalytics').trackEvent('reateApp', 'later');
        Ext.defer(function () {
            me.hide();
        }, 300);
    }
});