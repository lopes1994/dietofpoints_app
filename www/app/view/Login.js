Ext.define('Dietofpoints.view.Login', {

    extend: 'Ext.form.Panel',
	
    xtype: 'login',
	
    config: {

        scrollable: 'vertical',

        layout:{
            type  : 'vbox'
        },
		
        items: [
            {
                xtype: 'textfield',
                placeHolder: L('panel.template.login.email'),
                itemId: 'email',
                required: true,
                autoComplete: true,
                cls: 'customInput',
                listeners: [
                    {
                        fn: function(element, eOpts) {
                            element.down('input').dom.focus();
                        },
                        event: 'painted'
                    }
                ]
            },
            {
                xtype: 'passwordfield',
                placeHolder: L('panel.template.login.password'),
                itemId: 'password',
                required: true,
                cls: 'customInput'
            },
            {
                xtype: 'button',
                text: L('panel.template.login.login'),
				cls: 'bigButton',
                itemId: 'login'
            }
        ]
    }
});