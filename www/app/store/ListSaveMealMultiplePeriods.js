Ext.define("Dietofpoints.store.ListSaveMealMultiplePeriods", {

	extend: 'Ext.data.Store',

	config: {
		model: 'Dietofpoints.model.ListSaveMealMultiplePeriods'
	}
});