Ext.define("Dietofpoints.view.PanelTitleWater", {

	extend: 'Ext.Panel',
	
	xtype: 'panelTitleWater',

	config: {
	
		cls: 'panelTitlePeriod',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="titlePeriod_diary">' + L('panel.template.panelTitleWater.title') + '</div>';
                    str += '<div class="amountPeriod_diary"><div class="cups">' + values.amount + ' ' + L('panel.template.panelTitleWater.cups') + '</div></div>';
                    return str;
                }
            }
        )
	},

    initialize: function() {

        var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showModalWater,
            delegate: ['.cups'],
            scope: me
        });
    },

    showModalWater: function(event, element) {
        Dietofpoints.app.getController('Diary').showModalWater();
    }
});