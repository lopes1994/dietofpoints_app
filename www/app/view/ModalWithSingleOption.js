Ext.define("Dietofpoints.view.ModalWithSingleOption", {

	extend: 'Ext.Panel',
	
	xtype: 'modalWithSingleOption',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'button',
                text: L('modal.template.modalWithSingleOption.option1'),
                cls: 'buttonModal',
                itemId: 'option1'
            }
        ]
    }
});