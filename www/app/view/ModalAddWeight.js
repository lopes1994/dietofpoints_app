Ext.define("Dietofpoints.view.ModalAddWeight", {

	extend: 'Ext.Panel',
	
	xtype: 'modalAddWeight',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalAddWeight.title')
            },
            {
                xtype: 'panel',
                style: 'padding: 0px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'numberfield',
                        cls: 'numberfield_modalQuicklyAdd',
                        flex: 1,
                        itemId: 'weight',
                        maxLength: 5,
                        listeners: [
                            {
                                fn: function(element, eOpts) {
                                    element.down('input').dom.focus();
                                },
                                event: 'painted'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        cls: 'texto_modalQuicklyAdd',
                        flex: 1,
                        html: L('modal.template.modalAddWeight.text')
                    }
                ]
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalQuicklyAdd.cancel'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'cancel'
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalQuicklyAdd.add'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'add'
                    }
                ]
            }
        ]
    }
});