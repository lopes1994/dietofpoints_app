Ext.define("Dietofpoints.view.ModalConfirmLogout", {

	extend: 'Ext.Panel',
	
	xtype: 'modalConfirmLogout',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalConfirmLogout.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalConfirmLogout.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalConfirmLogout.cancel'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'cancel'
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalConfirmLogout.yes'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'yes'
                    }
                ]
            }
        ]
    }
});