Ext.define("Dietofpoints.view.PanelInitialWeight", {

	extend: 'Ext.Panel',
	
	xtype: 'panelInitialWeight',

	config: {

        cls: 'panelProfile',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="panelProfile_textLeft">' + L('panel.template.panelInitialWeight.title') + '</div>';
                    str += '<div class="panelProfile_textRight">' + values.text + ' kg' + '</div>';
                    return str;
                }
            }
        )
	}
});