Ext.define("Dietofpoints.view.PanelUpdatePortion", {

	extend: 'Ext.Panel',
	
	xtype: 'panelUpdatePortion',

	config: {

        layout: 'vbox',
        scrollable: 'vertical',

        items: [
            {
                xtype: 'textfield',
                itemId: 'name',
                readOnly: true,
                cls: 'customInput'
            },
            {
                xtype: 'textfield',
                itemId: 'amount',
                readOnly: true,
                cls: 'customInput'
            },
            {
                xtype: 'numberfield',
                placeHolder: L('panel.template.panelUpdatePortion.portions'),
                itemId: 'portions',
                cls: 'customInput',
                maxLength: 2,
                listeners: [
                    {
                        fn: function(element, eOpts) {
                            element.down('input').dom.focus();
                        },
                        event: 'painted'
                    }
                ]
            },
            {
                xtype: 'numberfield',
                hidden: true,
                itemId: 'code'
            }
        ]
	}
});