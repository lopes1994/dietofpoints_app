Ext.define("Dietofpoints.view.ListMyMeals", {

	extend: 'Ext.List',
	
	xtype: 'listMyMeals',

	config: {

		scrollable: 'vertical',
		store: 'ListMyMeals',
		flex: 1,
		emptyText: L('emptyText'),

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            str += '<div class="titleWithoutCheckbox"><label><input class="checkbox" type="checkbox" value="' + values.code_meal + '"><div class="name_food_withCheckbox">' + values.name_meal + '</div></label></div>';
					return str;
				}
			}
		)
	}
});