Ext.define("Dietofpoints.view.PanelMyFood", {

	extend: 'Ext.Panel',
	
	xtype: 'panelMyFood',

	config: {

        scrollable: false,
        title: L('panel.template.panelMyFood'),

        layout: {
            type: 'vbox'
        },

        items: [
            {
                xtype: 'panel',
                layout: 'hbox',
                docked: 'top',
                items: [
                    {
                        xtype: 'searchfield',
                        itemId: 'searchfield',
                        cls: 'searchfield',
                        placeHolder: L('panel.template.panelAllFood.searchfield'),
                        flex: 1,
                        listeners: [
                            {
                                fn: function(element, eOpts) {
                                    element.down('input').dom.focus();
                                },
                                event: 'painted'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: L('panel.template.panelAllFood.search'),
                        cls: 'buttonSearch',
                        itemId: 'search'
                    }
                ]
            },
            {
                xtype: 'listMyFood'
            }
        ]
	}
});