Ext.define("Dietofpoints.view.PanelMyMeals", {

	extend: 'Ext.Panel',
	
	xtype: 'panelMyMeals',

	config: {

        scrollable: false,
        title: L('panel.template.panelMyMeals.title'),

        layout: {
            type: 'vbox'
        },

        items: [
            {
                xtype: 'panel',
                layout: 'hbox',
                docked: 'top',
                items: [
                    {
                        xtype: 'searchfield',
                        itemId: 'searchfield',
                        cls: 'searchfield',
                        placeHolder: L('panel.template.panelMyMeals.searchfield'),
                        flex: 1,
                        listeners: [
                            {
                                fn: function(element, eOpts) {
                                    element.down('input').dom.focus();
                                },
                                event: 'painted'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: L('panel.template.panelAllFood.search'),
                        cls: 'buttonSearch',
                        itemId: 'search'
                    }
                ]
            },
            {
                xtype: 'listMyMeals'
            }
        ]
	}
});