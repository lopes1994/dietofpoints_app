Ext.define("Dietofpoints.view.PanelTitlePeriodBreakfast", {

	extend: 'Ext.Panel',
	
	xtype: 'panelTitlePeriodBreakfast',

	config: {
	
		cls: 'panelTitlePeriodWithoutMarginTop',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="titlePeriod_diary">' + L('panel.template.panelTitlePeriodBreakfast') + '</div>';
                    str += '<div class="amountPeriod_diary">' + values.amount + '</div>';
                    return str;
                }
            }
        )
	}
});