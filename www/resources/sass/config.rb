# Get the directory that this configuration file exists in
Encoding.default_external = "UTF-8"
dir = File.dirname(__FILE__)

# Load the sencha-touch framework automatically.
load File.join(dir, '..', '..', 'touch', 'resources', 'themes')

# Compass configurations
sass_path = dir
css_path = File.join(dir, "..", "css")

# Require any additional compass plugins here.
images_dir = File.join(dir, "..", "images")
fonts_path = File.join(dir, "..", "fonts")
#output_style = :expanded # TODO remove for production
output_style = :compressed # TODO uncommit for production
environment = :production
#environment = :development # TODO remove for production
