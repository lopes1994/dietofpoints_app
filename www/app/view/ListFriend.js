Ext.define("Dietofpoints.view.ListFriend", {

	extend: 'Ext.List',
	
	xtype: 'listFriend',

	config: {

		scrollable: 'vertical',
		store: 'ListFriend',
		flex: 1,
		emptyText: L('emptyText'),

		itemTpl: new Ext.XTemplate(
			'{[this.setTpl(values)]}',
		    {
		        setTpl: function(values) {
		            var str = '';
		            var lostWeight = 0;
		            str += '<div class="name_listFriend">' + values.name + '</div>';
		            if(values.text_balance === 'positive') {
		            	lostWeight = 0;
		            }
		            else {
		            	lostWeight = 1;
		            }
		            if(lostWeight == 0) {
                        str += '<div class="result_listFriend">' + L('list.template.listFriend.won') + ' ' + values.weight_balance + ' kg ';
                        str += '<i class="icon_listFriendRequest_delete fa fa-times fa-fw"></i></div>';
                    }
                    else {
                    	str += '<div class="result_listFriend">' + L('list.template.listFriend.lost') + ' ' + values.weight_balance + ' kg ';
                    	str += '<i class="icon_listFriendRequest_delete fa fa-times fa-fw"></i></div>';
                    }
                    str += '<div class="code">' + values.code + '</div>';
					return str;
				}
			}
		)
	},

	initialize: function() {

		var me = this;
        
        listElement = me.element;

        listElement.on({
            tap: me.showModalDeleteFriend,
            delegate: ['.fa-times'],
            scope: me
        });
	},

	showModalDeleteFriend: function(event, element) {
		var me = this;
        var record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord();
		Dietofpoints.app.getController('Diary').showModalDeleteFriend(record.get('code'));
	}
});