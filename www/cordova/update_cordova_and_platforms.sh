cordova platform add android@5.0.0

export plugins_add="
cordova-plugin-crosswalk-webview
cordova-plugin-inappbrowser
cordova-plugin-splashscreen
cordova-plugin-social-message
cordova-plugin-whitelist
de.appplant.cordova.plugin.local-notification
cordova-plugin-datepicker
cordova-plugin-google-analytics
"
for plugin in $plugins_add; do cordova plugin add $plugin;
done

cordova plugins list

