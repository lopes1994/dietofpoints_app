Ext.define("Dietofpoints.view.ModalAlreadyFriends", {

	extend: 'Ext.Panel',
	
	xtype: 'modalAlreadyFriends',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalAlreadyFriends.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalAlreadyFriends.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalErrorQuicklyAdd.close'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'close'
                    }
                ]
            }
        ]
    }
});