Ext.define("Dietofpoints.view.ModalErrorSendFriendRequest", {

    extend: 'Ext.Panel',
    
    xtype: 'modalErrorSendFriendRequest',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalErrorSendFriendRequest.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalErrorSendFriendRequest.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalErrorSendFriendRequest.no'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'no'
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalErrorSendFriendRequest.yes'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'yes'
                    }
                ]
            }
        ]
    }
});