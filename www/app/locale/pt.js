/**
 *  Resource file for Portuguese language.
 */
Ext.define('Dietofpoints.locale.pt', {
    singleton: true,
    requires: [
        'Ext.MessageBox',
        'Ext.picker.Picker',
        'Ext.picker.Date'
    ],
    translation: {
        language: {
            idiom: 'pt'
        },
        buttons: {
            save: 'Salvar',
            next: 'Próximo',
            add: 'Adicionar'
        },
        socialMessage: {
            text: 'Faça o controle da sua alimentação diária no aplicativo DietofPoints. É muito fácil, baixe agora gratuitamente:'
        },
        notifications: {
            title: 'Registre suas refeições diárias',
            text: 'É hora de registrar suas refeições. Vamos lá?'
        },
        rate: {
            title: 'Avalie o app',
            buttons: {
                '1': 'Não perguntar novamente',
                '2': 'Depois',
                '3': 'Avaliar'
            }
        },
        dateFormat: 'd/m/Y',
        emptyText: 'Nenhum resultado encontrado',
        ERROR_TITLE: 'Ocorreu um erro',
        messageBox_networkError: 'Erro de rede. Tente novamente!',
        menu: {
            diary: 'Diário',
            profile: 'Perfil',
            myFoodAndMeals: 'Meus alimentos e refeições',
            reminders: 'Lembretes',
            logout: 'Sair',
            help: 'Ajuda',
            myWeight: 'Meu peso',
            about: 'Sobre',
            share: 'Compartilhar',
            friend: 'Amigos',
            friendRequest: 'Solicitações de amizade'
        },
        list: {
            template: {
                listWeight: {
                    last_weight: 'Peso inicial'
                },
                listMyFood_tabpanelMyFoodAndMeals: {
                    title: 'Meus alimentos'
                },
                listMyMeals_tabpanelMyFoodAndMeals: {
                    title: 'Minhas refeições'
                },
                listFriend: {
                    title: 'Amigos',
                    won: 'Ganhou',
                    lost: 'Perdeu'
                },
                listFriendRequest: {
                    title: 'Solicitações de amizade'
                }
            }
        },
        panel: {
            template: {
                panelTitlePeriodBreakfast: 'Café da manhã',
                panelTitlePeriodLunch: 'Almoço',
                panelTitlePeriodDinner: 'Jantar',
                panelTitlePeriodSnacks: 'Lanches',
                panelTitleWater: {
                    title: 'Água',
                    cups: 'copos de 250 ml'
                },
                diary: 'Diário',
                select_item: 'Selecione um item',
                panelInitialWeight: {
                    title: 'Peso inicial'
                },
                panelActualWeight: {
                    title: 'Peso atual'
                },
                panelGoalWeight: {
                    title: 'Meta de peso'
                },
                panelActivityLevel: {
                    title: 'Nível de atividade'
                },
                panelHeight: {
                    title: 'Altura'
                },
                panelAge: {
                    title: 'Idade'
                },
                panelProfile: {
                    title: 'Perfil'
                },
                panelListWeight: {
                    title: 'Histórico de peso'
                },
                panelTopListWeight: {
                    won: 'Você ganhou',
                    lost: 'Você perdeu',
                    in: 'em',
                    days: 'dias',
                    goal_lost: 'Você precisa perder',
                    goal_win: 'Você precisa ganhar',
                    reach: 'para alcançar a meta de',
                    congratulations: 'Parabéns, você conseguiu alcançar a meta de'
                },
                panelMenu: {
                    consecutive_days: 'dias consecutivos',
                    text_balance_positive: 'Ganhou',
                    text_balance_negative: 'Perdeu'
                },
                panelAllFood: {
                    title: 'Todos alimentos',
                    search: 'Pesquisar',
                    searchfield: ' Digite o nome do alimento',
                    createFood: 'Criar um alimento',
                    quickAdd: 'Calories "Quick add"'
                },
                panelMyFood: 'Meus alimentos',
                panelMyMeals: {
                    title: 'Minhas refeições',
                    searchfield: 'Digite o nome da refeição'
                },
                panelOptionsDiary: {
                    addFood: 'Adicionar alimento',
                    more: 'Mais'
                },
                paneldatadiary: {
                    goal: 'Meta',
                    food: 'Alimento',
                    pointsbalance: 'Saldo de pontos'
                },
                login: {
                    email: 'E-mail',
                    password: 'Senha',
                    login: 'Entrar'
                },
                main: {
                    register: 'Cadastre-se',
                    login: 'Entrar'
                },
                panelSaveMeal: {
                    title: 'Salvar refeição',
                    textfield: 'Digite o nome da refeição',
                    button: 'Salvar',
                    panel: 'Itens contidos nesta refeição'
                },
                panelSaveMealMultiplePeriods: {
                    textfield: 'Digite o nome da refeição',
                    button: 'Salvar',
                    panel: 'Itens contidos nesta refeição'
                },
                panelCreateFood: {
                    title: 'Criar alimento',
                    name: 'Nome',
                    amount: 'Unidade (col.(sopa), ml, g)',
                    unit: 'Número de porções (1, 2, 350, 400)',
                    points: 'Calorias'
                },
                panelUpdatePortion: {
                    title: 'Alterar porção',
                    portions: 'Número de porções'
                },
                panelUpdateCalories: {
                    title: 'Alterar calorias',
                    calories: 'Calorias'
                },
                panelActivityLevel_user: {
                    title: 'Nível de atividade'
                },
                tabpanelMyFoodAndMeals: {
                    title: 'Meus alimentos e refeições'
                },
                panelUpdateUserFood: {
                    title: 'Alterar alimento',
                    name: 'Nome',
                    amount: 'Unidade (col.(sopa), ml, g)',
                    portions: 'Número de porções (1, 2, 350, 400)',
                    points: 'Calorias'
                },
                panelAddMeal: {
                    title: 'Adicionar refeição'
                },
                panelIdiom: {
                    title: 'Escolha um idioma',
                    english: 'English',
                    spanish: 'Español',
                    portuguese: 'Português'
                },
                panelYou: {
                    title: 'Você',
                    nameGender: 'Gênero',
                    age: 'Idade',
                    height: 'Altura (cm)',
                    weight: 'Peso atual (kg)',
                    goalWeight: 'Meta de peso',
                    name: 'Nome'
                },
                panelGender_you: {
                    male: 'Masculino',
                    female: 'Feminino'
                },
                panelAccessData: {
                    title: 'Dados de acesso',
                    email: 'E-mail',
                    password: 'Senha'
                },
                login: {
                    title: 'Entrar',
                    email: 'E-mail',
                    password: 'Senha',
                    login: 'Entrar'
                },
                panelAbout: {
                    title: 'Sobre',
                    text: 'Na Dieta dos pontos é necessário que você siga a quantidade de pontos que pode ingerir por dia. Não é permitido comer alimentos que tenham gordura trans. Qualquer outro alimento é permitido, mas você precisa ter bom senso ao montar o seu cardápio, escolhendo os alimentos mais saudáveis para conseguir manter uma alimentação equilibrada. Evite a carência de nutrientes para evitar problemas de saúde futuros.'
                },
                panelAddFriend: {
                    title: 'Adicionar amigo',
                    email: 'E-mail do amigo'
                }
            }
        },
        modal: {
            template: {
                modalConfirmDeleteItems: {
                    title: 'Excluir',
                    text: 'Você tem certeza que deseja excluir este item?',
                    cancel: 'Cancelar',
                    delete: 'Excluir'
                },
                modalCopyMeal: {
                    title: 'Copiar para qual data?',
                    today: 'Hoje',
                    yesterday: 'Ontem',
                    tomorrow: 'Amanhã'
                },
                modalErrorCreateMeal: {
                    title: 'Atenção',
                    text: 'Você já criou uma refeição com este nome. Por favor, escolha outro nome.',
                    close: 'Fechar'
                },
                modalErrorDeleteItems: {
                    title: 'Atenção',
                    text: 'Não há itens selecionados.',
                    close: 'Fechar'
                },
                modalErrorQuicklyAdd: {
                    title: 'Atenção',
                    text: 'Digite o número de calorias que pretende registrar.',
                    close: 'Fechar'
                },
                modalErrorAddWeight: {
                    text: 'Digite o número do peso que pretende registrar.'
                },
                modalErrorWithoutNameCreateMeal: {
                    title: 'Atenção',
                    text: 'Você deve digitar um nome para esta refeição.',
                    close: 'Fechar'
                },
                modalOptionsSelectItem: {
                    option1: 'Copiar para data',
                    option2: 'Salvar refeição',
                    option3: 'Selecionar tudo'
                },
                modalQuicklyAdd: {
                    title: 'Calorias para adicionar',
                    text: 'Calorias',
                    cancel: 'Cancelar',
                    add: 'Adicionar'
                },
                modalAddWeight: {
                    title: 'Peso para adicionar',
                    text: 'kg'
                },
                modalSuccessCreateMeal: {
                    title: 'Atenção',
                    text: 'Refeição criada com sucesso.',
                    close: 'Fechar'
                },
                modalWithMultipleOptions: {
                    option1: 'Salvar refeição',
                    option2: 'Copiar refeição',
                    option3: 'Quick add'
                },
                modalWithSingleOption: {
                    option1: 'Quick add'
                },
                modalCheckFilledFields: {
                    title: 'Atenção',
                    text: 'Todos os campos devem ser preenchidos.'
                },
                modalErrorCreateFood: {
                    title: 'Atenção',
                    text: 'Você já criou um alimento com este nome. Por favor, escolha outro nome.'
                },
                modalHeight: {
                    title: 'Altura',
                    text: 'cm'
                },
                modalAge: {
                    title: 'Idade'
                },
                modalErrorHeight: {
                    text: 'Digite sua altura.'
                },
                modalErrorAge: {
                    text: 'Digite sua idade.'
                },
                modalWater: {
                    title: 'Copos para adicionar',
                    text: 'copos de 250 ml'
                },
                modalErrorWater: {
                    text: 'Digite o número de copos de 250 ml de água que pretende registrar.'
                },
                modalAddMeal: {
                    title: 'Refeições',
                    breakfast: 'Café da manhã',
                    lunch: 'Almoço',
                    dinner: 'Jantar',
                    snacks: 'Lanches'
                },
                modalErrorLogin: {
                    text: 'E-mail ou senha está incorreto.'
                },
                modalErrorEmailRegister: {
                    text: 'O endereço de e-mail que você digitou já está sendo usado.'
                },
                modalConfirmLogout: {
                    title: 'Sair',
                    text: 'Tem certeza que deseja sair?',
                    cancel: 'Cancelar',
                    yes: 'Sim'
                },
                modalChangeLanguage: {
                    title: 'Atenção',
                    text: 'O aplicativo será reiniciado. Vai demorar alguns segundos.',
                    ok: 'Ok'
                },
                modalBadRate: {
                    title: 'Avaliação ruim',
                    text: 'Poderia nos dizer o que precisa ser melhorado?',
                    no: 'Não',
                    yes: 'Sim'
                },
                modalErrorChooseRate: {
                    title: 'Atenção',
                    text: 'Você precisa selecionar uma avaliação.'
                },
                modalDeleteFriend: {
                    title: 'Atenção',
                    text: 'Você tem certeza que deseja excluir esse amigo?',
                    delete: 'Excluir'
                },
                modalErrorSendFriendRequest: {
                    title: 'Atenção',
                    text: 'Não encontramos nenhum usuário com o e-mail informado. Gostaria de enviar uma mensagem para seu amigo agora?',
                    no: 'Não',
                    yes: 'Sim',
                    message: 'Olá! Gostaria de adicioná-lo como amigo no DietofPoints. Qual o seu e-mail? Se ainda não baixou o aplicativo, baixe aqui:'
                },
                modalSuccessSendFriendRequest: {
                    text: 'Sua solicitação de amizade foi enviada com sucesso.'
                },
                modalAlreadyFriends: {
                    title: 'Atenção',
                    text: 'Vocês já são amigos.'
                },
                modalOpenFriendRequestSent: {
                    text: 'Já existe uma solicitação de amizade em aberto entre você e este usuário.'
                },
                modalOpenFriendRequestReceived: {
                    title: 'Atenção',
                    text: 'Já existe uma solicitação de amizade em aberto entre você e este usuário. Gostaria de aceitar agora?',
                    yes: 'Sim',
                    no: 'Não'
                }
            }
        }
    },

    constructor: function() {
        if (this.$className.substr(-2) === lang) {
            this.applyLocale();
        }
    },
    applyLocale: function() {
        console.log('pt');
        //
        // Default Sencha wordings
        //

        Ext.Date.monthNames = [
            'Janeiro',
            'Fevereiro',
            'Março',
            'Abril',
            'Maio',
            'Junho',
            'Julho',
            'Agosto',
            'Setembro',
            'Outubro',
            'Novembro',
            'Dezembro'
        ];

        Ext.Date.monthNumbers = {
            'Jan.': 0,
            'Feb.': 1,
            'Mar.': 2,
            'Apr.': 3,
            'May':  4,
            'Jun.': 5,
            'Jul.': 6,
            'Aug.': 7,
            'Sep.': 8,
            'Oct.': 9,
            'Nov.': 10,
            'Dec.': 11
        };

        Ext.Date.shortMonthNames = [
            "Jan.",
            "Fev.",
            "Mar.",
            "Abr.",
            "Mai",
            "Jun.",
            "Jul.",
            "Ago.",
            "Set.",
            "Out.",
            "Nov.",
            "Dez."
        ];
        Ext.Date.getShortDayName = function(day) {
            return Ext.Date.dayNames[day].substring(0, 3);
        };

        Ext.Date.getMonthNumber = function(name) {
            return Ext.Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
        };
        Ext.Date.getShortMonthName = function(month) {
            return Ext.Date.shortMonthNames[month];
        };

        Ext.Date.parseCodes.S.s = '(?:?)';

        Ext.Date.getSuffix = function(date) {
            return '?';
        };

        if (Ext.picker && Ext.picker.Picker) {
            obj = Ext.picker.Picker.prototype.config;
            obj.doneButton = {
                text: 'Ok'
            };
            obj.cancelButton = {
                text: 'Cancelar'
            };
        }

        if (Ext.picker && Ext.picker.Date) {
            obj = Ext.picker.Date.prototype.config;
            obj.doneButton = {
                text: 'OK'
            };
            obj.cancelButton = {
                text: 'Cancelar'
            };
            Ext.apply(obj, {
                'dayText': 'Dia',
                'monthText': 'Mês',
                'yearText': 'Ano',
                'hourText': 'Hora',
                'minuteText': 'Minuto',
                'slotOrder': ['day', 'month', 'year'],
                'yearFrom': new Date().getFullYear(),
                'yearTo': new Date().getFullYear()+1
            });
        }

        if (Ext.IndexBar) {
            obj = Ext.IndexBar.prototype.config;
            Ext.apply(obj, {
                letters: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
            });
        }

        if (Ext.LoadMask) {
            obj = Ext.LoadMask.prototype.config;
            Ext.apply(obj, {
                message: 'Carregando...',
                indicator: true
            });
        }

        if (Ext.dataview && Ext.dataview.DataView) {
            obj = Ext.dataview.DataView.prototype.config;
            Ext.apply(obj, {
                loadingText: ''
            });
        }

        if (Ext.plugin.ListPaging) {
            obj = Ext.plugin.ListPaging.prototype.config;
            Ext.apply(obj, {
                autoPaging: true,
                loadMoreText: 'Mais registros',
                noMoreRecordsText: 'Não há mais registros'
            });
        }

        if (Ext.Button) {
            obj = Ext.Button.prototype.config;
            obj.pressedCls = 'pressedButton'
        }

        if (Ext.MessageBox) {

            Ext.apply(Ext.MessageBox, {
                OK: {cls: 'button_messageBox'}
            });

            Ext.MessageBox.OK.text = 'Ok';
            Ext.MessageBox.CANCEL.text = 'Cancelar';
            Ext.MessageBox.YES.text = 'Sim';
            Ext.MessageBox.NO.text = 'Não';

            Ext.MessageBox.OKCANCEL[0].text = 'Cancelar';
            Ext.MessageBox.OKCANCEL[1].text = 'Ok';

            Ext.MessageBox.YESNOCANCEL[0].text = 'Cancelar';
            Ext.MessageBox.YESNOCANCEL[1].text = 'Não';
            Ext.MessageBox.YESNOCANCEL[2].text = 'Sim';

            Ext.MessageBox.YESNO[0].text = 'Não';
            Ext.MessageBox.YESNO[1].text = 'Sim';
        }
    }
});