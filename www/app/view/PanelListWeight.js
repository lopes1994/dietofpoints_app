Ext.define("Dietofpoints.view.PanelListWeight", {

	extend: 'Ext.Panel',
	
	xtype: 'panelListWeight',

	config: {

        scrollable: false,

        layout: {
            type: 'vbox'
        },

        items: [
            {
                xtype: 'panelTopListWeight',
                docked: 'top'
            },
            {
                xtype: 'listWeight'
            }
        ]
	}
});