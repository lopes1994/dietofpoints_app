Ext.define("Dietofpoints.view.PanelUpdateCalories", {

	extend: 'Ext.Panel',
	
	xtype: 'panelUpdateCalories',

	config: {

        layout: 'vbox',
        scrollable: 'vertical',

        items: [
            {
                xtype: 'textfield',
                itemId: 'name',
                readOnly: true,
                cls: 'customInput'
            },
            {
                xtype: 'numberfield',
                placeHolder: L('panel.template.panelUpdateCalories.calories'),
                itemId: 'calories',
                cls: 'customInput',
                maxLength: 3,
                listeners: [
                    {
                        fn: function(element, eOpts) {
                            element.down('input').dom.focus();
                        },
                        event: 'painted'
                    }
                ]
            },
            {
                xtype: 'numberfield',
                hidden: true,
                itemId: 'code'
            }
        ]
	}
});