/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.application({
    name: 'Dietofpoints',

    requires: [
        'Dietofpoints.locale.pt',
        'Dietofpoints.locale.en',
        'Dietofpoints.locale.es',
        'Ext.ActionSheet',
        'Ext.MessageBox',
        'Ext.form.FieldSet',
        'Ext.field.Select',
        'Ext.field.DatePicker',
        'Ext.field.Text',
        'Ext.form.Password',
        'Ext.Label',
        'Ext.Img',
        'Ext.util.DelayedTask',
        'Ext.Menu',
        'Ext.field.Search',
        'Ext.field.Email',
        'Ext.field.Number',
        'Ext.field.TextArea',
        'Ext.plugin.ListPaging',
        'Ext.field.Toggle'
    ],

    views: [
        'Navigationview',
        'Main',
        'Diary',
        'ListBreakfast',
        'ListLunch',
        'ListDinner',
        'ListSnacks',
        'PanelOptionsBreakfast',
        'PanelOptionsLunch',
        'PanelOptionsDinner',
        'PanelOptionsSnacks',
        'Login',
        'PanelDataDiary',
        'Datepicker_diary',
        'PanelTitlePeriodBreakfast',
        'PanelTitlePeriodLunch',
        'PanelTitlePeriodDinner',
        'PanelTitlePeriodSnacks',
        'PanelDatepicker',
        'ModalQuicklyAdd',
        'ModalErrorQuicklyAdd',
        'ModalCopyMeal',
        'ModalWithSingleOption',
        'ModalWithMultipleOptions',
        'PanelSaveMeal',
        'ListSaveMeal',
        'ModalSuccessCreateMeal',
        'ModalErrorCreateMeal',
        'ModalErrorWithoutNameCreateMeal',
        'ModalOptionsSelectItem',
        'ModalErrorDeleteItems',
        'ModalConfirmDeleteItems',
        'ModalCopyMealMultiplePeriods',
        'PanelSaveMealMultiplePeriods',
        'ListSaveMealMultiplePeriods',
        'TabpanelAddFood',
        'PanelAllFood',
        'PanelMyFood',
        'PanelMyMeals',
        'ListAllFood',
        'ListMyFood',
        'ListMyMeals',
        'ModalQuicklyAddCalledFromTabpanelAddFood',
        'PanelCreateFood',
        'ModalErrorCreateFood',
        'ModalCheckFilledFields',
        'ModalErrorQuicklyAddCalledFromTabpanelAddFood',
        'PanelUpdatePortion',
        'PanelUpdateCalories',
        'PanelMenu',
        'Menu',
        'ListWeight',
        'ModalAddWeight',
        'ModalErrorAddWeight',
        'PanelListWeight',
        'PanelTopListWeight',
        'PanelInitialWeight',
        'PanelActualWeight',
        'PanelGoalWeight',
        'PanelActivityLevel',
        'PanelHeight',
        'PanelAge',
        'PanelProfile',
        'ModalActualWeight',
        'ModalGoalWeight',
        'ModalHeight',
        'ModalAge',
        'ModalErrorActualWeight',
        'ModalErrorGoalWeight',
        'ModalErrorHeight',
        'ModalErrorAge',
        'PanelActivityLevel_user',
        'PanelTitleWater',
        'ModalWater',
        'ModalErrorWater',
        'TabpanelMyFoodAndMeals',
        'ListMyFood_tabpanelMyFoodAndMeals',
        'ListMyMeals_tabpanelMyFoodAndMeals',
        'PanelUpdateUserFood',
        'ModalConfirmDeleteItemsTabpanelMyFoodAndMeals',
        'PanelAddMeal',
        'ListAddMeal',
        'ModalAddMeal',
        'PanelIdiom',
        'PanelYou',
        'PanelGender_you',
        'PanelAccessData',
        'ModalErrorLogin',
        'ModalErrorEmailRegister',
        'ModalConfirmLogout',
        'ModalChangeLanguage',
        'PanelAbout',
        'ListFriend',
        'ModalBadRate',
        'ModalErrorChooseRate',
        'RateApp',
        'ModalDeleteFriend',
        'PanelAddFriend',
        'ModalErrorSendFriendRequest',
        'ModalSuccessSendFriendRequest',
        'ModalAlreadyFriends',
        'ModalOpenFriendRequestSent',
        'ListFriendRequest',
        'ModalOpenFriendRequestReceived',
        'PanelBottomTabPanelAddFood'
    ],

    stores: [
        'ListBreakfast',
        'ListLunch',
        'ListDinner',
        'ListSnacks',
        'ListSaveMeal',
        'ListSaveMealMultiplePeriods',
        'ListAllFood',
        'ListMyFood',
        'ListMyMeals',
        'ListWeight',
        'ListMyFood_tabpanelMyFoodAndMeals',
        'ListMyMeals_tabpanelMyFoodAndMeals',
        'ListAddMeal',
        'ListFriend',
        'ListFriendRequest'
    ],

    models : [
        'ListBreakfast',
        'ListLunch',
        'ListDinner',
        'ListSnacks',
        'ListSaveMeal',
        'ListSaveMealMultiplePeriods',
        'ListAllFood',
        'ListMyFood',
        'ListMyMeals',
        'ListWeight',
        'ListMyFood_tabpanelMyFoodAndMeals',
        'ListMyMeals_tabpanelMyFoodAndMeals',
        'ListAddMeal',
        'ListFriend',
        'ListFriendRequest'
    ],

    controllers: [
        'Diary',
        'GoogleAnalytics'
    ],

    active_group: 'production',

    data_server: null,

    images: null,

    sizeOfList: 55,

    helpdesk: 'https://dietofpoints.on.spiceworks.com/portal',

    market: 'https://play.google.com/store/apps/details?id=com.dietofpoints.DietofPoints',

    launch: function() {
        this.setStartsCount();
        this.globalSettings();
        this.globalVariables();
        if(window.cordova) {
            this.createLocalNotifications();
        }
        //Google Analytics
        Dietofpoints.app.getController('GoogleAnalytics').startTrackerWithId('UA-76397137-7');
        // Destroy the #spiner element
        Ext.fly('spiner').destroy();
        if(window.cordova) {
            Dietofpoints.app.debug = false;
        } else {
            Dietofpoints.app.debug = true;
        }
        //Automatic update stylesheet
        if (Dietofpoints.app.debug) {
            window.addEventListener('keydown', function(event) {
                if (event.ctrlKey && event.keyCode === 81) { //ctrl+Q
                    console.log(event.keyCode);
                    Ext.DomQuery.select('link')[0].href = 'resources/css/app.css?' + Math.ceil(Math.random() * 100000000);
                }
            }, false);
        }

        if (!Dietofpoints.app.debug) {
            Ext.defer(function() {
                navigator.splashscreen.hide();
            }, 1000);
        }

        if(window.cordova && AdMob) {
            AdMob.createBanner({
                adId: 'ca-app-pub-8759751152566475/8248950341',
                adSize: 'CUSTOM',
                width: window.innerWidth,
                height: Ext.os.deviceType == 'Tablet' ? 100 : 50,
                position: AdMob.AD_POSITION.BOTTOM_CENTER,
                autoShow: true
            });
        }

        //Create view Rate
        Ext.Viewport.add(Ext.create('Dietofpoints.view.RateApp'));
        //Initialize the main view
        Ext.Viewport.add(Ext.create('Dietofpoints.view.Navigationview'));
    },

    addMenu: function() {
        Ext.Viewport.setMenu(Ext.create('Dietofpoints.view.Menu'), {
            side: 'left',
            reveal: false
        });
    },

    globalVariables: function() {

        var me = this;

        if(me.active_group === 'development') {
            me.data_server = 'http://localhost/dietofpoints/dietofpoints_server/dietofpoints_server/';
        }

        else {
            me.data_server = 'http://www.belagendavip.com.br/dietofpoints_server/';
        }
    },

    setStartsCount: function() {
        var number = localStorage.getItem('startsCount');
        if(number) {
            localStorage.setItem('startsCount', parseInt(number) + 1);
        }
        else {
            localStorage.setItem('startsCount', 1);
        }
    },

    createLocalNotifications: function() {
        //Cancel all notifications
        cordova.plugins.notification.local.cancelAll();
        //Create new notifications
        var date = new Date();
        date.setMinutes(date.getMinutes() + 180);

        //https://www.npmjs.com/package/cordova-plugin-local-notification-iphm-proto
        cordova.plugins.notification.local.schedule({
            id: 1,
            title: L('notifications.title'),
            text: L('notifications.text'),
            firstAt: date,
            every: 180, //minutes
            icon: './resources/img/icon.png'
        });
    },

    //Função para setar as configuraÃ§Ãµes comuns a alguns componentes
    globalSettings: function() {

        //Se aparelho do usuário for android
        if(Ext.os.is.Android) {
            //Desabilitando animação quando for mostrar uma caixa de mensagem
            Ext.Msg.defaultAllowedConfig.showAnimation = false;
            //Desabilitando animação quando for esconder uma caixa de mensagem
            Ext.Msg.defaultAllowedConfig.hideAnimation = false;
        }

        //CONFIGURAÇÃO DA CHAMADA DO LOADMASK
        //Fires before a network request is made to retrieve a data object.
        Ext.Ajax.on('beforerequest', function (con, opt) {
            //To show the mask
            Ext.Viewport.setMasked({
                xtype: 'loadmask'
            });
        }, this);

        //Fires if the request was successfully completed and hide the mask.
        Ext.Ajax.on('requestcomplete', function (con, res, opt) {
            //To hide the mask
            Ext.Viewport.setMasked(false);
        }, this);

        //Fires if an error HTTP status was returned from the server  and hide the mask.
        Ext.Ajax.on('requestexception', function (con, response, opt) {
            //To hide the mask
            Ext.Viewport.setMasked(false);
        }, this);
    }
});
