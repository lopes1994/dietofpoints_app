Ext.define("Dietofpoints.view.ModalAge", {

	extend: 'Ext.Panel',
	
	xtype: 'modalAge',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalAge.title')
            },
            {
                xtype: 'panel',
                style: 'padding: 0px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'numberfield',
                        cls: 'numberfield_modalQuicklyAdd',
                        itemId: 'age',
                        flex: 1,
                        maxLength: 3,
                        listeners: [
                            {
                                fn: function(element, eOpts) {
                                    element.down('input').dom.focus();
                                },
                                event: 'painted'
                            }
                        ]
                    },
                    {
                        xtype: 'spacer',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalQuicklyAdd.cancel'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'cancel'
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalQuicklyAdd.add'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'add'
                    }
                ]
            }
        ]
    }
});