Ext.define("Dietofpoints.view.ModalConfirmDeleteItemsTabpanelMyFoodAndMeals", {

	extend: 'Ext.Panel',
	
	xtype: 'modalConfirmDeleteItemsTabpanelMyFoodAndMeals',

    config: {

        hideOnMaskTap: true,
        layout: 'vbox',
        width: '90%',
        height: 'auto',
        centered: true,
        modal: true,
        items: [
            {
                xtype: 'panel',
                cls: 'titulo_modalQuicklyAdd',
                html: L('modal.template.modalConfirmDeleteItems.title')
            },
            {
                xtype: 'panel',
                cls: 'texto_modalErrorQuicklyAdd',
                html: L('modal.template.modalConfirmDeleteItems.text')
            },
            {
                xtype: 'panel',
                style: 'padding: 12px 15px;',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalConfirmDeleteItems.cancel'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'cancel'
                    },
                    {
                        xtype: 'button',
                        text: L('modal.template.modalConfirmDeleteItems.delete'),
                        cls: 'button_modalQuicklyAdd',
                        itemId: 'delete'
                    }
                ]
            }
        ]
    }
});