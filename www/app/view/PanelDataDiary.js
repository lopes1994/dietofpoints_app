Ext.define("Dietofpoints.view.PanelDataDiary", {

	extend: 'Ext.Panel',
	
	xtype: 'paneldatadiary',

	config: {

        cls: 'panelDataDiary',
        docked: 'top',

        tpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '';
                    str += '<div class="goal">' + values.goal + '</div>';
                    str += '<div class="operation">-</div>';
                    str += '<div class="food">' + values.food + '</div>';
                    str += '<div class="operation">=</div>';
                    if(values.pointsbalance >= 0) {
                        str += '<div class="pointsbalance_positive">' + values.pointsbalance + '</div>';
                    }
                    else {
                        str += '<div class="pointsbalance_negative">' + values.pointsbalance + '</div>';
                    }
                    str += '<div class="text_goal">' + L('panel.template.paneldatadiary.goal') + '</div>';
                    str += '<div class="text_food">' + L('panel.template.paneldatadiary.food') + '</div>';
                    str += '<div class="text_pointsbalance">' + L('panel.template.paneldatadiary.pointsbalance') + '</div>';
                    return str;
                }
            }
        )
	}
});